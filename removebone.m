handles = guihandles(findobj('Tag','amb_interface')); TPlan=getappdata(handles.amb_interface,'TPlan');
M=TPlan.plan.Mat_HU;
M(M~=20)=0;
M=logical(M);
CC = bwconncomp(M);
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = find(numPixels<=5000);

M=double(M);

for i=1:size(idx,2)
    M(CC.PixelIdxList{idx(i)}) = 2;
end

TPlan.plan.Mat_HU(M==2)=17;
setappdata(handles.amb_interface,'TPlan',TPlan);
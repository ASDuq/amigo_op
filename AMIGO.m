function varargout = AMIGO(varargin)
% See also: GUIDE, GUIDATA, GUIHANDLES
% Edit the above text to modify the response to help AMIGO
% Last Modified by GUIDE v2.5 27-Mar-2020 21:02:43
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AMIGO_OpeningFcn, ...
                   'gui_OutputFcn',  @AMIGO_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AMIGO is made visible.
function AMIGO_OpeningFcn(hObject, ~, handles, varargin)
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);
addpath(genpath([pwd filesep 'Extra']),genpath([pwd filesep 'fcn']),genpath([pwd filesep 'icons']),genpath([pwd filesep 'soft_data']));
%diary AMIGO_log.amb

% UIWAIT makes AMIGO wait for user response (see UIRESUME)
% uiwait(handles.amb_interface);

% --- Outputs from this function are returned to the command line.
function varargout = AMIGO_OutputFcn(~, ~, handles) 
varargout{1} = handles.output;
create_cmap_menu;
set (gcf, 'WindowScrollWheelFcn', @mouse_wheel);
javatoolbar;         %  icons
w = warning ('off','all');
% java progress bar
%
try % try to delete previous wait bar
    h=findjobj(javax.swing.JProgressBar);  [hPb, hContainer] = javacomponent(h);       delete(hPb); delete(hContainer); delete(h); 
catch
    % not an error
end
%
% find icons ...
ic_fold=which('amb_view_findicon.png'); l=strfind(ic_fold,filesep);
setappdata(gcf,'icons_fol',ic_fold(1:l(end)));
if ispc==1
  set_ini_icons(handles);
else
  set_ini_icons_linux(handles);  
end
%
warning('off','MATLAB:str2func:invalidFunctionName');
%
warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
jframe=get(gcf,'javaframe');
jIcon=javax.swing.ImageIcon([ic_fold(1:l(end)) 'amb_icon.png']);
jframe.setFigureIcon(jIcon);
%
jFrame = get(handle(gcf),'JavaFrame');    % Minimize the GUI while setting the visualization options 
jFrame.setMinimized(true); drawnow;       drawnow; 
%
% ---------------------------------------------------------------------------------------------------
jFrame.setMaximized(true);  drawnow;       % Default mouse ... Maximaze
%
jProx = jFrame.fHG2Client.getWindow;
jProx.setMinimumSize(java.awt.Dimension(1200, 700));
%
jPb = javax.swing.JProgressBar;                         set(jPb,'StringPainted',1,'Value',0,'Indeterminate',0,'Background',(java.awt.Color(1,1,1)));  
jPb.setBackground(java.awt.Color(1,1,1));               position=[200 10 20 20]; 
[hPb, hContainer] = javacomponent(jPb,position,gcf);    
set(hContainer,'Units','normalized','position',[0.86 0.004 0.12 0.027]); 
setappdata(gcf,'waitbarjava',hPb); set(hPb,'Visible',0); drawnow;
%
jframe = get(gcf, 'JavaFrame');
jframe.fHG2Client.setClientDockable(false);
%
% link axes
linkaxes([handles.amb_vis_01,handles.amb_dos_01],'xy');
linkaxes([handles.amb_vis_02,handles.amb_dos_02],'xy');
linkaxes([handles.amb_vis_03,handles.amb_dos_03],'xy');
%
%
% try
%   lh=getappdata(handles.amb_interface,'listeners_'); 
%   delete(lh.ax1_1); delete(lh.ax1_2); delete(lh.ax2_1);
%   delete(lh.ax2_2); delete(lh.ax3_1); delete(lh.ax3_2);
% end
%
% Manually controls the zoom (linkaxes does not work since ax01 and ax 03
% are rotated)
%
if isempty(getappdata(handles.amb_interface,'listeners_'))==0; return; end
%
lh.ax1_1=addlistener(handles.amb_vis_01, 'XLim', 'PostSet', @adjust_position_axes); % has to do it twice to avoid update problems
lh.ax1_2=addlistener(handles.amb_vis_01, 'YLim', 'PostSet', @adjust_position_axes);
lh.ax2_1=addlistener(handles.amb_vis_02, 'XLim', 'PostSet', @adjust_position_axes);
lh.ax2_2=addlistener(handles.amb_vis_02, 'YLim', 'PostSet', @adjust_position_axes);
lh.ax3_1=addlistener(handles.amb_vis_03, 'XLim', 'PostSet', @adjust_position_axes);
lh.ax3_2=addlistener(handles.amb_vis_03, 'YLim', 'PostSet', @adjust_position_axes);
%
setappdata(handles.amb_interface,'listeners_',lh);
%
ax                    = getappdata(handles.amb_interface,'ax');
ax.gui                = handles.amb_interface;
ax.dose_menu          = handles.dose_menu;
ax.cont_pop_menu      = handles.cont_pop_menu;
ax.material_pop_menu  = handles.material_pop_menu;
ax.section_mathu      = handles.section_mathu;
setappdata(handles.amb_interface,'ax',ax);
%
% Cover
%
image(handles.cover_axes,imread([ic_fold(1:l(end)) 'cover.png']));
set(handles.cover_panel,'Visible','on','Position',[ 0 0 1 1]);
%
set_menu_list(handles);
% -----------------------------------------------------------------------
% -----------------------------------------------------------------------
%%
%

function edit_materials_Callback(~, ~, handles) %#ok<*DEFNU>
if strcmp(handles.material_panel.Visible,'on')==0
    set(handles.material_panel,'Visible','on','Position',[0.18 0.0 0.82 1]);
else
    set(handles.material_panel,'Visible','off');
end

%
function ct_cal_menu(handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
load('amb_ct_calibration.amb','-mat'); %#ok<*LOAD>
% create material list
TPlan.plan.ct_cal=CT_calibration;
%
handles.ct_cal_pop_menu.String =fieldnames(TPlan.plan.ct_cal);
handles.ct_cal_edit_menu.String=fieldnames(TPlan.plan.ct_cal);
setappdata(handles.amb_interface,'TPlan',TPlan);

%
function material_menu(handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
%
if isfield(TPlan.image,'load')==1
    %
    handles.edit_all_materials.String  =  TPlan.image.load.ed_all_mat;
    handles.material_pop_menu.UserData =  TPlan.image.load.mat_limits;
    handles.material_pop_menu.String   =  TPlan.image.load.mat_menu;
    handles.materials_range.String     =  num2str(TPlan.image.load.mat_limits(1,2:3));
    %
    TPlan.image=rmfield(TPlan.image,'load');
    setappdata(handles.amb_interface,'TPlan',TPlan);
    return;
end
%
load('amb_material.amb','-mat');
%
TPlan.plan.materials=mat;
% create material list
cont=0;
str={}; allmat={};
for i=1:length(fieldnames(mat))
   allmat{end+1}=strtrim(mat.(sprintf('mat%.0f',i)).name);
   if strcmp(mat.(sprintf('mat%.0f',i)).name,'empty')==0 
    cont=cont+1;   
    TPlan.plan.MatNames{cont}   = strtrim(mat.(sprintf('mat%.0f',i)).name);
    TPlan.plan.MatNamesID(cont) = i;
    %
    % create HTML String
    n=['+' num2str(0,'%5.5d') '  +' num2str(0,'%5.5d') ' ' TPlan.plan.MatNames{cont}]; %#ok<*AGROW>
    n=strrep(n,' ',' &nbsp '); 
    %
    str{end+1}=['<HTML><FONT color=#000000 size=5>' n '</FONT></HTML>'];
    %
   end
end
handles.edit_all_materials.String  =allmat;
handles.material_pop_menu.String   =str;
handles.material_pop_menu.UserData =zeros(length(fieldnames(mat)),6);
setappdata(handles.amb_interface,'TPlan',TPlan);

%
function plot_image_after_import(handles,TPlan)
ax=getappdata(handles.amb_interface,'ax');
%
handles.view_DCM.Checked      ='on';
handles.view_mat_map.Checked  ='off';
handles.view_dens_map.Checked ='off';
%
sli(1:3)=[round(TPlan.image.Nvoxels(3)/2) round(TPlan.image.Nvoxels(2)/2) round(TPlan.image.Nvoxels(1)/2)];
setappdata(handles.amb_interface,'slices',sli); drawnow;
%
set(handles.section_ImView,'State','off'); % force it to update the axes
set(handles.section_ImView,'State','on');
colormap gray;
%
cla(handles.amb_vis_01);
imagesc(handles.amb_vis_01,TPlan.image.Image(:,:,sli(1)));           h=get(handles.amb_vis_01,'Children'); ax.fig01=h(end); hold(handles.amb_vis_01,'on'); drawnow;
cla(handles.amb_vis_02);
imagesc(handles.amb_vis_02,squeeze(TPlan.image.Image(:,sli(2),:)));  h=get(handles.amb_vis_02,'Children'); ax.fig02=h(end); hold(handles.amb_vis_02,'on'); drawnow;
handles.amb_vis_02.XLim=[1 TPlan.image.Nvoxels(3)];
handles.amb_dos_02.XLim=[1 TPlan.image.Nvoxels(3)];
cla(handles.amb_vis_03);
imagesc(handles.amb_vis_03,squeeze(TPlan.image.Image(sli(3),:,:)));  h=get(handles.amb_vis_03,'Children'); ax.fig03=h(end); hold(handles.amb_vis_03,'on'); drawnow;
handles.amb_vis_03.XLim=[1 TPlan.image.Nvoxels(3)];
handles.amb_dos_03.XLim=[1 TPlan.image.Nvoxels(3)];
%
setappdata(handles.amb_interface,'ax',ax);


function plot_empty_contour_layer(handles,TPlan)
ax=getappdata(handles.amb_interface,'ax');
% Plot empty layer for contrast view
I=zeros(TPlan.image.Nvoxels(1),TPlan.image.Nvoxels(2),3); 
c=image(handles.amb_vis_01,I); set(c,'Tag','contour_xy');
set(c, 'AlphaData', rgb2gray(I)); 
%
h=get(handles.amb_vis_01,'Children'); ax.fig01_contour=h(end-1);
%
I=zeros(TPlan.image.Nvoxels(3),TPlan.image.Nvoxels(1),3);
c=image(handles.amb_vis_02,I); set(c,'Tag','contour_yz');
set(c, 'AlphaData', rgb2gray(I));
h=get(handles.amb_vis_02,'Children'); ax.fig02_contour=h(end-1);
%
I=zeros(TPlan.image.Nvoxels(3),TPlan.image.Nvoxels(2),3);
c=image(handles.amb_vis_03,I); set(c,'Tag','contour_xz');
set(c, 'AlphaData', rgb2gray(I));
h=get(handles.amb_vis_03,'Children'); ax.fig03_contour=h(end-1);
setappdata(handles.amb_interface,'ax',ax);

function plot_empty_dose_layer(handles,TPlan)
ax=getappdata(handles.amb_interface,'ax');
% Plot empty layer for dose view
set([handles.amb_dos_01,handles.amb_dos_02,handles.amb_dos_03],'Color','none');
%
I=zeros(TPlan.image.Nvoxels(1),TPlan.image.Nvoxels(2),3); 
c=imagesc(handles.amb_dos_01,I); set(c,'Tag','contour_xy');
set(c, 'AlphaData', rgb2gray(I)); 
ax.dos01=get(handles.amb_dos_01,'Children'); 
%
I=zeros(TPlan.image.Nvoxels(3),TPlan.image.Nvoxels(1),3);
c=imagesc(handles.amb_dos_02,I); set(c,'Tag','contour_yz');
set(c, 'AlphaData', rgb2gray(I));
ax.dos02=get(handles.amb_dos_02,'Children');
%
I=zeros(TPlan.image.Nvoxels(3),TPlan.image.Nvoxels(2),3);
c=imagesc(handles.amb_dos_03,I); set(c,'Tag','contour_xz');
set(c, 'AlphaData', rgb2gray(I));
ax.dos03=get(handles.amb_dos_03,'Children'); 
%
setappdata(handles.amb_interface,'ax',ax);


function dose_menu(handles,TPlan)
% Dose menu
handles.dose_panel.Visible='on';
str={};
str{1}='none';
if isempty(TPlan.dose)==0
  str(2:size(fieldnames(TPlan.dose),1)+1)=fieldnames(TPlan.dose);
end
%
handles.dose_menu.String=str;
handles.dose_menu.Value  =1;
%
setappdata(handles.amb_interface,'show_dose','none');

function [] = set_contour_list(TPlan,handles)
% contour list
if isfield(TPlan.struct,'contours')==1
    str={};
    c_names=fieldnames(TPlan.struct.contours);
    for i=1:length(c_names)
       str{end+1}=['<html><font color="black" size="6">' TPlan.struct.contours.(sprintf('%s',c_names{i})).Name '</font></html>'];
    end
    set(handles.cont_pop_menu,'String',str','Value',1,'Visible','on');
    set(handles.cont_pop_menu,'Userdata',zeros(length(c_names),4));
else
    set(handles.cont_pop_menu,'String','No contours','Value',1,'Visible','on');
end
handles.amb_cont_panel.Visible='on';
set_cont_view_off(handles);



% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
%%
%
%                  Listeners and axes
%
% axes listener
function [ ] = adjust_position_axes(~,evnt)
ax=str2num(evnt.AffectedObject.Tag(end-1:end));
lh=getappdata(gcf,'listeners_');
%
axs1=findobj('Tag','amb_vis_01');
axs2=findobj('Tag','amb_vis_02');
axs3=findobj('Tag','amb_vis_03');
%
if     ax==1
        %
        y=get(axs1,'Ylim');
        delete(lh.ax2_2);                                   set(axs2,'YLim',y);  
        lh.ax2_2 = addlistener(axs2, 'YLim', 'PostSet', @adjust_position_axes);
        %
        x=get(axs1,'Xlim'); 
        delete(lh.ax3_2);                                   set(axs3,'YLim',x);
        lh.ax3_2 = addlistener(axs3, 'YLim', 'PostSet', @adjust_position_axes);
        %
        drawnow;
elseif ax==2
        x=get(axs2,'Xlim');  y=get(axs2,'Ylim');
        delete(lh.ax1_2);    set(axs1,'YLim',y);
        lh.ax1_2 =  addlistener(axs1, 'YLim', 'PostSet', @adjust_position_axes);
        %
        delete(lh.ax3_1);    set(axs3,'XLim',x);
        lh.ax3_1 =  addlistener(axs3, 'XLim', 'PostSet', @adjust_position_axes);
        drawnow;  
        %
elseif ax==3
        x=get(axs3,'Xlim');  y=get(axs3,'Ylim');
        delete(lh.ax1_1);    set(axs1,'XLim',y);
        lh.ax1_1 =  addlistener(axs1, 'XLim', 'PostSet', @adjust_position_axes);
        %
        delete(lh.ax2_1);    set(axs2,'XLim',x);
        lh.ax2_1 =  addlistener(axs2, 'XLim', 'PostSet', @adjust_position_axes);
        drawnow;  
end
drawnow;
set_slice_guides; 
setappdata(gcf,'listeners_',lh);







% -----------------------------------------------------------------------
% -----------------------------------------------------------------------
%%
%
%                   Section 
%


function section_ImView_OnCallback(~,~, handles)
%
handles.section_imedit.State    ='off';
handles.section_mathu.State     ='off';
handles.section_plan.State      ='off';
handles.section_dose.State      ='off';
handles.section_ext_beam.State  ='off';
%
if isempty(getappdata(handles.amb_interface,'TPlan'))==1
    warndlg('Import a treatment !');
    return;
end
%
TPlan = getappdata(handles.amb_interface,'TPlan');
if isempty(TPlan.DECT)==0
    handles.DECT_panel.Visible = 'on';
end
%imview_adjust_pos(handles);



function section_imedit_OnCallback(~,~, handles)
handles.section_ImView.State    ='off';
handles.section_mathu.State     ='off';
handles.section_plan.State      ='off';
handles.section_dose.State      ='off';
handles.section_ext_beam.State  ='off';
handles.imedit_panel.Visible='on';
handles.imedit_panel.Position=[0.0 0.04 0.175 0.52];

function section_imedit_OffCallback(~, ~, handles)
handles.imedit_panel.Visible='off';

function section_mathu_OnCallback(~,~, handles)
handles.section_ImView.State    ='off';
handles.section_imedit.State    ='off';
handles.section_plan.State      ='off';
handles.section_dose.State      ='off';
handles.section_ext_beam.State  ='off';
%
handles.mat_hu_panel.Visible='on';
handles.mat_hu_panel.Position=[0.0 0.04 0.175 0.52];
%
if getappdata(handles.amb_interface,'plot_contour')==1
   setappdata(handles.amb_interface,'plot_contour',3);
end
materials_range_Callback(1, 1, handles);
TPlan= getappdata(handles.amb_interface,'TPlan');
dose_menu(handles,TPlan);
dose_menu_Callback(1,1,handles);
handles.dose_panel.Visible='off';



function section_mathu_OffCallback(~,~, handles)
handles.mat_hu_panel.Visible='off';
handles.material_panel.Visible='off';
handles.ct_cal_panel.Visible  ='off';
%
handles.edit_materials.Value=0;
handles.edit_ct_cal.Value=0;
%
show_mat_off_Callback(1, 1, handles);
%
if getappdata(handles.amb_interface,'plot_contour')==3
   setappdata(handles.amb_interface,'plot_contour',1);
   cont_pop_menu_Callback(1, 1, handles);
end
%
handles.dose_panel.Visible='on';

function section_plan_OnCallback(~,~, handles)
handles.section_ImView.State    ='off';
handles.section_imedit.State    ='off';
handles.section_mathu.State     ='off';
handles.section_dose.State      ='off';
handles.section_ext_beam.State  ='off';
%
handles.sec_plan_panel.Visible='on';
handles.sec_plan_panel.Position=[0.0 0.04 0.175 0.52];
%
handles.menu_plan.Enable='on';

cat_menu_Callback(1, 1, handles);


function section_plan_OffCallback(~, ~, handles)
handles.sec_plan_panel.Visible='off';
%
%
% if isempty(findobj('Tag','phant_xyz'))==0; 
%     delete(findobj('Tag','phant_xyz'));
% end
% %
% if isempty(findobj('Tag','score_xyz'))==0; 
%     delete(findobj('Tag','score_xyz')); drawnow;
% end
%
if strcmp(handles.view_dwell.Checked,'on')==1
    view_dwell_Callback(1, 1, handles); drawnow;
end
%
if strcmp(handles.view_catheters.Checked,'on')==1
    view_catheters_Callback(1, 1, handles);
end
%
handles.menu_plan.Enable='off';


function section_dose_OnCallback(~,~, handles)
handles.section_ImView.State    ='off';
handles.section_imedit.State    ='off';
handles.section_mathu.State     ='off';
handles.section_plan.State      ='off';
handles.section_ext_beam.State  ='off';
%
% warndlg('Section under development with very limited functionalities');
%
handles.dose_eval_panel.Visible  = 'on';
handles.dose_eval_panel.Position = [0.001 0.001 0.999 0.999];

function section_dose_OffCallback(~, ~, handles)
handles.dose_eval_panel.Visible='off';


% --------------------------------------------------------------------
function section_ext_beam_OnCallback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
%
if isempty(TPlan)==1
    handles.section_ext_beam.State ='off';
    return;
end
%
%
%
handles.section_ImView.State    = 'off';
handles.section_imedit.State    = 'off';
handles.section_mathu.State     = 'off';
handles.section_plan.State      = 'off';
handles.section_dose.State      = 'off';

handles.EB_panel_01.Visible     = 'on';
handles.rot3d_eb.Visible        = 'on';
handles.EB_panel_01.Position    = [0.0 0.0 1 1];
%
h = findobj('Tag','IrIS_rot_panel');
if isempty(h)==0
    h.Visible = 'on';
end
% get plan data
TPlan=getappdata(handles.amb_interface,'TPlan');
%
% EB? 
if isfield(TPlan.plan,'BEAM_info')== 1
    handles.EB_listbox_01.String    = TPlan.plan.BEAM_info;
    %
    % adjust listbox position in case user open a new plan
    if size(handles.EB_listbox_01.String,1)<handles.EB_listbox_01.Value
        handles.EB_listbox_01.Value=1;
    end
    %
end
%
if isfield(TPlan.plan,'pat_model')==0 || isfield(TPlan.plan.pat_model,'patch')==0 || (isfield(TPlan.plan.pat_model,'patch')==1 && (isempty(TPlan.plan.pat_model.patch)==1 || isvalid(TPlan.plan.pat_model.patch)==0))
  % try to find a contour named Body
  found=0;
  if isempty(TPlan.struct)==0 
      for i=1:length(fieldnames(TPlan.struct.contours))
          % plans from raystation might have external instead of body
          if  strcmpi(TPlan.struct.contours.(sprintf('Item_%d',i)).Name,'Body')==1
              found=[1,i];
              break;
          end
      end
  end
  %
  % in case body is not available use External (RayStation)
  %
  if found(1) == 0 && isempty(TPlan.struct)==0 
      for i=1:length(fieldnames(TPlan.struct.contours))
          % plans from raystation might have external instead of body
          if  strcmpi(TPlan.struct.contours.(sprintf('Item_%d',i)).Name,'External')==1
              found=[1,i];
              break;
          end
      end
  end
  %
  if found(1)==1
        % Construct a questdlg with three options
        choice = questdlg('Would you like to use the Body contour or a general model?', ...
            '3D view', ...
            'Body','Model','Model');
        % Handle response
        switch choice
            case 'Body'
              h=warndlg('Please wait ... It can take some time to creat a 3D model');
              [TPlan.plan.pat_model.face,v] = isosurface(TPlan.struct.contours.(sprintf('Item_%d',found(2))).Mask,0.5);
              v=bsxfun(@times,v, TPlan.image.Resolution); 
              v=bsxfun(@plus,v,TPlan.image.ImagePositionPatient);
              v=v/10;
              v(:,2)=-v(:,2);
              TPlan.plan.pat_model.vertex=v(:,[1,3,2]);
              cla(handles.EB_vis_01);         
              TPlan.plan.pat_model.patch=patch(handles.EB_vis_01,'Faces',TPlan.plan.pat_model.face,'Vertices',TPlan.plan.pat_model.vertex,'FaceColor',[1 1 1],'FaceAlpha',1,'EdgeAlpha',0.2);
              % might need to reduce the resolution to improve speed
              if size(TPlan.plan.pat_model.vertex,1)>20000
                 [TPlan.plan.pat_model.face,TPlan.plan.pat_model.vertex]=reducepatch(TPlan.plan.pat_model.patch,0.01);
                 delete(TPlan.plan.pat_model.patch);
                 TPlan.plan.pat_model.patch=patch(handles.EB_vis_01,'Faces',TPlan.plan.pat_model.face,'Vertices',TPlan.plan.pat_model.vertex,'FaceColor',[1 1 1],'FaceAlpha',1,'EdgeAlpha',0.2);
              end
              close(h);
              TPlan.plan.isocenter=[0;0;0];
            case 'Model'
              % import patient model
                load('model_pat.mat');
                TPlan.plan.pat_model.face=face;
                TPlan.plan.pat_model.vertex=vertex;  
                cla(handles.EB_vis_01);
                TPlan.plan.pat_model.patch=patch(handles.EB_vis_01,'Faces',TPlan.plan.pat_model.face,'Vertices',TPlan.plan.pat_model.vertex,'FaceColor',[1 1 1],'FaceAlpha',1,'EdgeAlpha',0.2);
        end
  elseif strcmp(TPlan.plan.modality,'Protons')==0
                % import patient model
                load('model_pat.mat');
                TPlan.plan.pat_model.face=face;
                TPlan.plan.pat_model.vertex=vertex;  
                cla(handles.EB_vis_01);
                TPlan.plan.pat_model.patch=patch(handles.EB_vis_01,'Faces',TPlan.plan.pat_model.face,'Vertices',TPlan.plan.pat_model.vertex,'FaceColor',[1 1 1],'FaceAlpha',1,'EdgeAlpha',0.2);
   end
  %
  setappdata(handles.amb_interface,'TPlan',TPlan);
  handles.EB_vis_01.DataAspectRatio=[1,1,1];
  set(handles.EB_vis_01,'XLim',[-120 120],'YLim',[-120 50],'ZLim',[-120 120]); box on;
  hold(handles.EB_vis_01,'on');
  TPlan.plan.pat_model.X_line = plot(handles.EB_vis_01,[-200 200],[   0   0],'r-');
  TPlan.plan.pat_model.Y_line = plot(handles.EB_vis_01,[   0   0],[-200 200],'g-');
  TPlan.plan.pat_model.Z_line = plot3(handles.EB_vis_01,[0   0 ],[0 0],[-200 200],'b-');
  hold(handles.EB_vis_01,'off');
  handles.EB_vis_01.DataAspectRatio=[1 1 1];
  handles.EB_vis_01.DataAspectRatio=[1 1 1];
  %
  % proton geometry
  if isfield(TPlan.plan,'modality') && strcmp(TPlan.plan.modality,'Protons')==1
    %
    proton_geometry;
    TPlan.plan.C_angle = 0;
    setappdata(handles.amb_interface,'TPlan',TPlan);
    %
  elseif isfield(TPlan.plan,'BEAM_info')== 1
    colimator_mlc(TPlan);
  else
    TPlan.plan.isocenter = TPlan.image.ImagePositionPatient';
    setappdata(handles.amb_interface,'TPlan',TPlan);
  end
  grid on;
 
end
%




% --------------------------------------------------------------------
function section_ext_beam_OffCallback(~, ~, handles)
handles.EB_panel_01.Visible = 'off';
handles.rot3d_eb.Visible    = 'off';
%
rotate3d(handles.EB_vis_01,'off');
%
handles.rot3d_eb.State      = 'off';
h = findobj('Tag','IrIS_rot_panel');
if isempty(h)==0
    h.Visible = 'off';
end



% -----------------------------------------------------------------------
% -----------------------------------------------------------------------
%%
%
%                   Import data
function import_tretment_plan_Callback(~, ~, handles)
%
tic
%
TPlan = import_treatmentplan_V2;
if isnumeric(TPlan.image)==1; return; end
%
set(handles.cover_panel,'Visible','on'); drawnow;
setappdata(handles.amb_interface,'TPlan',TPlan);
toc
TPlan=getappdata(handles.amb_interface,'TPlan');
if isempty(TPlan.image)==1; return; end
%
w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
%
set(findobj('Tag','text4'),'String','Preparing visualization');
%
set(w_bar.hand,'Visible',1,'Value',80);
% User most select phantom and grid size ....
handles.phantom_user.Checked   ='off';
handles.phantom_all.Checked    ='off';
handles.dosegrid_user.Checked  ='off';
handles.dosegrid_all.Checked   ='off';
%
handles.draw_on.Value=0;
global draw_countour
draw_countour.on=0;
draw_countour.subtract=0;
delete(findobj('Tag','draw_pointer'));
set(handles.amb_interface,'pointer','default');
set(handles.amb_interface,'WindowButtonMotionFcn',[]);
% plot images
TPlan=getappdata(handles.amb_interface,'TPlan');
if isfield(TPlan.image,'Image')==0; errordlg('no image available'); return; end
plot_image_after_import(handles,TPlan);         set(w_bar.hand,'Visible',1,'Value',85);   
plot_empty_contour_layer(handles,TPlan);        set(w_bar.hand,'Visible',1,'Value',87);
plot_empty_dose_layer(handles,TPlan);           set(w_bar.hand,'Visible',1,'Value',89);
set_contour_list(TPlan,handles);                set(w_bar.hand,'Visible',1,'Value',91);
adjust_contrast_axes(handles,TPlan,1);          set(w_bar.hand,'Visible',1,'Value',93);
dose_menu(handles,TPlan);
dose_contrast_histogram(0);
%
material_menu(handles);                         set(w_bar.hand,'Visible',1,'Value',97);
ct_cal_menu(handles);
set_slice_guides;                         drawnow;
%
if isfield(TPlan.plan,'NCat')==1
    handles.cat_menu.String=strsplit(num2str((1:1:TPlan.plan.NCat)));
end
%
cla(handles.amb_dose_eval);
handles.clinical_par_list.String=[];
%
imview_adjust_pos(handles);
set(handles.cover_panel,'Visible','off'); drawnow;
set(w_bar.hand,'Visible',0,'Value',0);



% ----------------------------------------------------------------------
% ----------------------------------------------------------------------
%%
%
%                   Visualization options

% isodoses

function isodose_view_Callback(hObject, ~, handles)
iso = getappdata(handles.amb_interface,'isod_settings');
if hObject.UserData == 1
    hObject.UserData = 0;
    iso.view=0;
    setappdata(handles.amb_interface,'isod_settings',iso);
    iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'iso_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
    str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
    set(handles.isodose_view,'String',str);
    delete(findobj('Tag','Contour01'));
    delete(findobj('Tag','Contour02'));
    delete(findobj('Tag','Contour03'));
    % delete isodoses
    return;
end
%
hObject.UserData = 1;
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'iso_on.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.isodose_view,'String',str);
[s,~] = listdlg('PromptString','Select up to 3 doses:','SelectionMode','multiple','ListString',{handles.dose_menu.String{2:end}}); %#ok<*CCAT1>
%
TPlan=getappdata(handles.amb_interface,'TPlan');
%
if isfield(TPlan.plan,'presc_dose')==1
    dose = TPlan.plan.presc_dose;
else
    dose =2;
end
prompt = {'Reference dose:','Isolines (%):'}; dlg_title = 'Isodoses';  num_lines = 1;
def = {num2str(dose),'100 75 50 25'};                            answer = inputdlg(prompt,dlg_title,num_lines,def);
%
if isempty(getappdata(handles.amb_interface,'isod_settings'))==1
    settings_isodoses(handles);
end
iso = getappdata(handles.amb_interface,'isod_settings');
iso.dvalues=str2num(answer{1})*str2num(answer{2})/100; 
iso.view=1;
hold(handles.amb_dos_01,'on');
hold(handles.amb_dos_02,'on');
hold(handles.amb_dos_03,'on');
%
iso.s=s;
setappdata(handles.amb_interface,'isod_settings',iso);
%
sli=getappdata(handles.amb_interface,'slices');
ax=getappdata(handles.amb_interface,'ax');
update_isodoses(4,TPlan,sli,ax)




function isod_01_Callback(~, ~, handles)
if isempty(getappdata(handles.amb_interface,'isod_settings'))==1
    settings_isodoses(handles);
end
iso=getappdata(handles.amb_interface,'isod_settings');
iso.line01(1:3)=uisetcolor;
%
prompt = {'1 - Continuous  2 - Dashed lines:','Line Width:'};
dlg_title = 'Isodoes 1';
num_lines = 1;
defaultans = {'1','1'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
%
iso.line01(4)=str2double(answer{1});
iso.line01(5)=str2double(answer{2});
setappdata(handles.amb_interface,'isod_settings',iso);
%
if iso.view==0; return; end
TPlan=getappdata(handles.amb_interface,'TPlan');
sli=getappdata(handles.amb_interface,'slices');
ax=getappdata(handles.amb_interface,'ax');
update_isodoses(4,TPlan,sli,ax)

function isod_02_Callback(~, ~, handles)
if isempty(getappdata(handles.amb_interface,'isod_settings'))==1
    settings_isodoses(handles);
end
iso=getappdata(handles.amb_interface,'isod_settings');
iso.line02(1:3)=uisetcolor;
%
prompt = {'1 - Continuous  2 - Dashed lines:','Line Width:'};
dlg_title = 'Isodoes 2';
num_lines = 1;
defaultans = {'1','1'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
%
iso.line02(4)=str2double(answer{1});
iso.line02(5)=str2double(answer{2});
setappdata(handles.amb_interface,'isod_settings',iso);
%
if iso.view==0; return; end
TPlan=getappdata(handles.amb_interface,'TPlan');
sli=getappdata(handles.amb_interface,'slices');
ax=getappdata(handles.amb_interface,'ax');
update_isodoses(4,TPlan,sli,ax)

function isod_03_Callback(~, ~, handles)
if isempty(getappdata(handles.amb_interface,'isod_settings'))==1
    settings_isodoses(handles);
end
iso=getappdata(handles.amb_interface,'isod_settings');
iso.line03(1:3)=uisetcolor;
%
prompt = {'1 - Continuous  2 - Dashed lines:','Line Width:'};
dlg_title = 'Isodoes 3';
num_lines = 1;
defaultans = {'1','1'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
%
iso.line03(4)=str2double(answer{1});
iso.line03(5)=str2double(answer{2});
setappdata(handles.amb_interface,'isod_settings',iso);
%
if iso.view==0; return; end
TPlan=getappdata(handles.amb_interface,'TPlan');
sli=getappdata(handles.amb_interface,'slices');
ax=getappdata(handles.amb_interface,'ax');
update_isodoses(4,TPlan,sli,ax)

function settings_isodoses(handles)
iso.line01=[1 0 0 1 1];
iso.line02=[0 0 0 1 1];
iso.line03=[1 1 1 1 1];
setappdata(handles.amb_interface,'isod_settings',iso);




% Materials
function material_pop_menu_Callback(hObject, ~, handles)
if hObject.UserData(hObject.Value,1) == 0
    iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'View_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
    str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
    set(handles.show_material,'String',str);
    handles.show_material.UserData=0;
else
    iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'View_on.png'];   iconFilename = strrep(iconFilename, '\', '/');
    str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
    set(handles.show_material,'String',str);
    handles.show_material.UserData=1;
end
set(handles.materials_range,'String',num2str(hObject.UserData(hObject.Value,2:3),'%4.4d  ')); 
update_material_map_all(handles);


function materials_range_Callback(~, ~, handles)
r=str2num(handles.materials_range.String);
handles.material_pop_menu.UserData(handles.material_pop_menu.Value,2:3)=r;
if r(1)>=0; n1=num2str(r(1),'+%5.5d'); else n1=num2str(r(1),'%5.5d'); end
if r(2)>=0; n2=num2str(r(2),'+%5.5d'); else n2=num2str(r(2),'%5.5d'); end
handles.material_pop_menu.String{handles.material_pop_menu.Value}(34:39)=n1;
handles.material_pop_menu.String{handles.material_pop_menu.Value}(54:59)=n2;
update_material_map_all(handles);

function show_material_Callback(~, ~, handles)
%
if handles.show_material.UserData==1
      handles.material_pop_menu.UserData(handles.material_pop_menu.Value,1)=0;
      handles.show_material.UserData=0;
      iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'View_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
      str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
      set(handles.show_material,'String',str);
else
      handles.material_pop_menu.UserData(handles.material_pop_menu.Value,1)=1;
      handles.show_material.UserData=1;
      iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'View_on.png'];   iconFilename = strrep(iconFilename, '\', '/');
      str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
      set(handles.show_material,'String',str);   
      %
      rgb_color=round(uisetcolor);
      handles.material_pop_menu.UserData(handles.material_pop_menu.Value,4:6)=rgb_color;
      %
      rgb_color=rgb_color*255;
      hex(:,1) = '#'; hex(:,2:7) = reshape(sprintf('%02X',rgb_color.'),6,[]).'; 
      handles.material_pop_menu.String{handles.material_pop_menu.Value}(19:25)=hex;
end
update_material_map_all(handles);

function mat_transp_Callback(~, ~, handles)
trans = getappdata(handles.amb_interface,'trans_material');
def{1}=num2str(trans);
answer=inputdlg('Transparency 0 - 1','Contour',1,def);
trans=str2double(answer{1});
if isnumeric(trans)==1
    setappdata(handles.amb_interface,'trans_material',trans);
    update_material_map_all(handles);
end

function show_mat_off_Callback(~, ~, handles)
for i=1:size(handles.material_pop_menu.UserData,1)
     handles.material_pop_menu.String{i}(19:25)='#000000';
end
handles.material_pop_menu.UserData(1:end,1)=0;
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'View_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.show_material,'String',str,'UserData',0);
update_material_map_all(handles);

function update_material_map_all(handles)
TPlan            = getappdata(handles.amb_interface,'TPlan');
current_slices   = getappdata(handles.amb_interface,'slices');
trans            = getappdata(handles.amb_interface,'trans_material');
ax               = getappdata(handles.amb_interface,'ax');
%
if isempty(trans)==1
    trans=0.5;
    setappdata(handles.amb_interface,'trans_material',trans);
end    
  update_mat_map(4,TPlan,current_slices,ax,trans); 


function amb_interface_ButtonDownFcn(~, ~, handles)
%
setappdata(handles.amb_interface,'Track',1);

%


function amb_interface_WindowButtonUpFcn(~, ~, handles)
global draw_countour
if isempty(getappdata(handles.amb_interface,'draw_on'))==1 || getappdata(handles.amb_interface,'draw_on')==0
   set (gcf, 'WindowButtonMotionFcn', []);      drawnow;
   set(handles.amb_interface,'pointer','default');
end
if isfield(draw_countour,'brush')==1 && draw_countour.brush==1
    h=findobj('Tag','draw_pointer');
    set(handles.amb_interface,'WindowButtonMotionFcn', {@brush_amb,h([2,1]),handles.amb_dos_01,handles.amb_interface});
end
draw_countour.on=0;
setappdata(handles.amb_interface,'Track',0); drawnow;
%draw_countour.subtract=0;


function amb_vis_01_ButtonDownFcn(~, ~, ~)


%
%% toolbar

function [] =  create_cmap_menu ()
if isempty(findobj('Tag','cmap_menu'))==0; return; end
%
hToolbar  = findall(gcf,'tag','uitoolbar1');     hcmap       = uisplittool('parent',hToolbar);
icon      = fullfile(which('bucket.gif'));       [cdata,map] = imread(icon);              
cdataUndo = ind2rgb(cdata,map);
% Convert white pixels into a transparent background
set(hcmap,'cdata',cdataUndo,'Tag','cmap_menu','ClickedCallback','uiundo(gcbf,''execUndo'')'); drawnow;
%
jUndo = get(hcmap,'JavaContainer'); jMenu = get(jUndo,'MenuComponent'); % or: =jUndo.getMenuComponent
% Color map options
jOption1 = jMenu.add('Gray');                                 jOption2 = jMenu.add('Jet');        jOption3 = jMenu.add('Hot');
jOption4 = jMenu.add('Bone');                                 jOption5 = jMenu.add('Summer');     jOption6 = jMenu.add('Autumn');
set(jOption1, 'actionPerformedCallback', 'colormap(findobj(''TAG'',''amb_vis_01''),''gray'');   colormap(findobj(''TAG'',''amb_vis_02''),''gray'');   colormap(findobj(''TAG'',''amb_vis_03''),''gray'');   colormap(findobj(''TAG'',''amb_vis_contrast2''),''gray'')');   
set(jOption2, 'actionPerformedCallback', 'colormap(findobj(''TAG'',''amb_vis_01''),''jet'');    colormap(findobj(''TAG'',''amb_vis_02''),''jet'');    colormap(findobj(''TAG'',''amb_vis_03''),''jet'');    colormap(findobj(''TAG'',''amb_vis_contrast2''),''jet'')'); 
set(jOption3, 'actionPerformedCallback', 'colormap(findobj(''TAG'',''amb_vis_01''),''hot'');    colormap(findobj(''TAG'',''amb_vis_02''),''hot'');    colormap(findobj(''TAG'',''amb_vis_03''),''hot'');    colormap(findobj(''TAG'',''amb_vis_contrast2''),''hot'')');      
set(jOption4, 'actionPerformedCallback', 'colormap(findobj(''TAG'',''amb_vis_01''),''Bone'');   colormap(findobj(''TAG'',''amb_vis_02''),''Bone'');   colormap(findobj(''TAG'',''amb_vis_03''),''Bone'');   colormap(findobj(''TAG'',''amb_vis_contrast2''),''Bone'')'); 
set(jOption5, 'actionPerformedCallback', 'colormap(findobj(''TAG'',''amb_vis_01''),''Summer''); colormap(findobj(''TAG'',''amb_vis_02''),''Summer''); colormap(findobj(''TAG'',''amb_vis_03''),''Summer''); colormap(findobj(''TAG'',''amb_vis_contrast2''),''Summer'')');  
set(jOption6, 'actionPerformedCallback', 'colormap(findobj(''TAG'',''amb_vis_01''),''Autumn''); colormap(findobj(''TAG'',''amb_vis_02''),''Autumn''); colormap(findobj(''TAG'',''amb_vis_03''),''Autumn''); colormap(findobj(''TAG'',''amb_vis_contrast2''),''Autumn'')');  
%
% % organize position
% jToolbar = get(get(hToolbar,'JavaContainer'),'ComponentPeer');
% jButtons = jToolbar.getComponents;
% jToolbar.setComponentZOrder(jButtons(end), 8);
%
% Dose colormap
hToolbar = findall(gcf,'tag','uitoolbar1');     hcmap = uisplittool('parent',hToolbar);
icon = fullfile(which('bucket2.gif'));  [cdata,map] = imread(icon);              cdataUndo =   ind2rgb(cdata,map);
% Convert white pixels into a transparent background
set(hcmap,'cdata',cdataUndo,'Tag','cmap_menu2','ClickedCallback','uiundo(gcbf,''execUndo'')'); drawnow;
%
jUndo = get(hcmap,'JavaContainer'); jMenu = get(jUndo,'MenuComponent'); % or: =jUndo.getMenuComponent
% Color map options
jOption1 = jMenu.add('Gray');                                 jOption2 = jMenu.add('Jet');        jOption3 = jMenu.add('Hot');
jOption4 = jMenu.add('Bone');                                 jOption5 = jMenu.add('Summer');     jOption6 = jMenu.add('Autumn');
set(jOption1, 'actionPerformedCallback', 'colormap(findobj(''TAG'',''amb_dos_01''),''gray'');   colormap(findobj(''TAG'',''amb_dos_02''),''gray'');   colormap(findobj(''TAG'',''amb_dos_03''),''gray'');   colormap(findobj(''TAG'',''amb_dos_contrast2''),''gray'')');   
set(jOption2, 'actionPerformedCallback', 'colormap(findobj(''TAG'',''amb_dos_01''),''jet'');    colormap(findobj(''TAG'',''amb_dos_02''),''jet'');    colormap(findobj(''TAG'',''amb_dos_03''),''jet'');    colormap(findobj(''TAG'',''amb_dos_contrast2''),''jet'')'); 
set(jOption3, 'actionPerformedCallback', 'colormap(findobj(''TAG'',''amb_dos_01''),''hot'');    colormap(findobj(''TAG'',''amb_dos_02''),''hot'');    colormap(findobj(''TAG'',''amb_dos_03''),''hot'');    colormap(findobj(''TAG'',''amb_dos_contrast2''),''hot'')');      
set(jOption4, 'actionPerformedCallback', 'colormap(findobj(''TAG'',''amb_dos_01''),''Bone'');   colormap(findobj(''TAG'',''amb_dos_02''),''Bone'');   colormap(findobj(''TAG'',''amb_dos_03''),''Bone'');   colormap(findobj(''TAG'',''amb_dos_contrast2''),''Bone'')'); 
set(jOption5, 'actionPerformedCallback', 'colormap(findobj(''TAG'',''amb_dos_01''),''Summer''); colormap(findobj(''TAG'',''amb_dos_02''),''Summer''); colormap(findobj(''TAG'',''amb_dos_03''),''Summer''); colormap(findobj(''TAG'',''amb_dos_contrast2''),''Summer'')');  
set(jOption6, 'actionPerformedCallback', 'colormap(findobj(''TAG'',''amb_dos_01''),''Autumn''); colormap(findobj(''TAG'',''amb_dos_02''),''Autumn''); colormap(findobj(''TAG'',''amb_dos_03''),''Autumn''); colormap(findobj(''TAG'',''amb_dos_contrast2''),''Autumn'')');  
%
%





function [] =  javatoolbar()
    % Java customization
    hToolbar = findall(gcf,'tag','uitoolbar1');
    jToolbar = get(get(hToolbar,'JavaContainer'),'ComponentPeer');
    %
    jToolbar.setPreferredSize(java.awt.Dimension(90,90));
    jToolbar.revalidate; % refresh/update the displayed toolbar
    %
    color = java.awt.Color.white;
    jToolbar.setBackground(color);
    jToolbar.getParent.getParent.setBackground(color);
    jtbc = jToolbar.getComponents;
    for idx = 1 : length(jtbc)
        jtbc(idx).setOpaque(false);
        jtbc(idx).setBackground(color);
        for childIdx = 1 : length(jtbc(idx).getComponents)
          jtbc(idx).getComponent(childIdx-1).setBackground(color);
        end
    end
    % scale icons
    numButtons = jToolbar.getComponentCount;
    for i=1:numButtons
    %
      try
         newSize = java.awt.Dimension(90,100);
         % Remember that Java indexes start at 0, not 1. . .
         jToolbar.getComponent(i).setMaximumSize(newSize);
         %
         icon = jToolbar.getComponent(i).getIcon;
         iconImg = icon.getImage; % a java.awt.image.BufferedImage object
         newIconImg = iconImg.getScaledInstance(64,64,iconImg.SCALE_SMOOTH);
         icon.setImage(newIconImg);
         drawnow;
      catch
         % expected error     
      end
    end
    jToolbar.revalidate; % refresh/update the displayed toolbar
    h=findobj('Tag','uitoolbar1'); set(h,'Visible','on');
    drawnow;

    
%% Material map 
function create_mat_map_Callback(~, ~, handles)
TPlan             = getappdata(handles.amb_interface,'TPlan');
TPlan.plan.Mat_HU = int8(zeros(TPlan.image.Nvoxels(1),TPlan.image.Nvoxels(2),TPlan.image.Nvoxels(3)));
%
w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
set(w_bar.hand,'Visible',1);
%
s=size(handles.material_pop_menu.UserData,1);
for i=1:s
    if handles.material_pop_menu.UserData(i,2) ~=0 || handles.material_pop_menu.UserData(i,3)~=0
      TPlan.plan.Mat_HU(TPlan.image.Image(:)>=handles.material_pop_menu.UserData(i,2) ...
                        & TPlan.image.Image(:)<=handles.material_pop_menu.UserData(i,3))=TPlan.plan.MatNamesID(i);   
    end
    set(w_bar.hand,'Value',(i/s*100)); drawnow; 
end
%
if isempty(find(TPlan.plan.Mat_HU==0,1))==0
    warndlg('Check the range ... Unassigned materials were defined as water');
    TPlan.plan.Mat_HU(TPlan.plan.Mat_HU==0)=5;
end
%
setappdata(handles.amb_interface,'TPlan',TPlan);
pause(2);
set(w_bar.hand,'Visible',0,'Value',0);
update_images(4,double(getappdata(handles.amb_interface,'slices')));

% Density map
function creat_dens_map_Callback(~, ~, handles)
%
w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
set(w_bar.hand,'Visible',1,'Value',1);
%
TPlan=getappdata(handles.amb_interface,'TPlan');
if isfield(TPlan.plan,'Mat_HU')==0; warndlg('Create a material map first'); return; end
nam=handles.ct_cal_pop_menu.String(handles.ct_cal_pop_menu.Value);
%
TPlan.plan.Density=single(zeros(TPlan.image.Nvoxels(1),TPlan.image.Nvoxels(2),TPlan.image.Nvoxels(3)));
% 
if strcmp(nam,'Library')==1
  for i=1:100
     TPlan.plan.Density(TPlan.plan.Mat_HU==i)=TPlan.plan.materials.(sprintf('mat%.0f',i)).density;
     set(w_bar.hand,'Value',i);
  end
  setappdata(handles.amb_interface,'TPlan',TPlan);
  set(w_bar.hand,'Visible',0,'Value',0);
  return;
end
%
% ct calibration
cal=(TPlan.plan.ct_cal.(sprintf('%s',nam{1})).cal);
cal(cal(:,1)==0,:)=[];
% Clear empty values
% -----------------------------------------------------------------------------------------
%  inclination parameters
set(w_bar.hand,'Value',15);
    for i=1:size(cal,1)-1
        inc(i,1)=(cal(i+1,1)-cal(i,1))/(cal(i+1,2)-cal(i,2));
        inc(i,2)=(cal(i+1,1)-cal(i+1,2)*inc(i,1));
    end
%   
set(w_bar.hand,'Value',25);
    for i=1:size(cal,1)-2
         TPlan.plan.Density(TPlan.image.Image>=cal(i,2) & TPlan.image.Image<cal(i+1,2))=single(TPlan.image.Image(TPlan.image.Image>=cal(i,2) & TPlan.image.Image<cal(i+1,2)))*inc(i,1)+inc(i,2);
    end 
         TPlan.plan.Density(TPlan.image.Image>=cal(size(cal,1)-1,2))=single(TPlan.image.Image(TPlan.image.Image>=cal(size(cal,1)-1,2)))*inc(size(cal,1)-1,1)+inc(size(cal,1)-1,2);         
set(w_bar.hand,'Value',35);  
%Lower Limit 
TPlan.plan.Density(TPlan.plan.Density<cal(1,2))=cal(1,1);
%Upper Limit - %%% The code will set the upper density to 2.9
TPlan.plan.Density(TPlan.plan.Density>2.9)=2.9;
% check non tissue materials
for i=1:100
  if strcmp(TPlan.plan.materials.(sprintf('mat%.0f',i)).tissue,'no')==1 
    TPlan.plan.Density(TPlan.plan.Mat_HU==i)=TPlan.plan.materials.(sprintf('mat%.0f',i)).density;
    set(w_bar.hand,'Value',i+25);
  end
end
setappdata(handles.amb_interface,'TPlan',TPlan);
set(w_bar.hand,'Visible',0,'Value',0);
update_images(4,double(getappdata(handles.amb_interface,'slices')));


function save_mathu_Callback(~, ~, handles)
TPlan = getappdata(handles.amb_interface,'TPlan');
mat_hu=zeros(100,2);
for i=1:size(TPlan.plan.MatNamesID,2)
   mat_hu(TPlan.plan.MatNamesID(i),1:2)=handles.material_pop_menu.UserData(i,2:3);
end
[file,nfolder]=uiputfile('*.matHU');
save([nfolder file],'mat_hu');



function load_mathu_Callback(~, ~, handles)
[file,nfolder]=uigetfile('*.matHU');
load([nfolder file],'-mat');
TPlan = getappdata(handles.amb_interface,'TPlan');
for i=1:size(TPlan.plan.MatNamesID,2)
   handles.material_pop_menu.UserData(i,2:3)=mat_hu(TPlan.plan.MatNamesID(i),1:2);
   r=mat_hu(TPlan.plan.MatNamesID(i),1:2);
   if r(1)>=0; n1=num2str(r(1),'+%5.5d'); else n1=num2str(r(1),'%5.5d'); end
   if r(2)>=0; n2=num2str(r(2),'+%5.5d'); else n2=num2str(r(2),'%5.5d'); end
   handles.material_pop_menu.String{i}(34:39)=n1;
   handles.material_pop_menu.String{i}(54:59)=n2; 
end
set(handles.materials_range,'String',num2str(handles.material_pop_menu.UserData(handles.material_pop_menu.Value,2:3),'%4.4d  ')); 




%% imageview options
%
function view_xy_Callback(hObject, ~, handles)
    set(hObject,             'Checked','on');
    set(handles.view_yz,     'Checked','off');
    set(handles.view_xz,     'Checked','off');
    set(handles.view_3d_axes,'Checked','off');
    imview_adjust_pos(handles);

function view_yz_Callback(hObject, ~, handles)
    set(hObject,             'Checked','on');
    set(handles.view_xy,     'Checked','off');
    set(handles.view_xz,     'Checked','off');
    set(handles.view_3d_axes,'Checked','off');
    imview_adjust_pos(handles);

function view_xz_Callback(hObject, ~, handles)
    set(hObject,             'Checked','on');
    set(handles.view_yz,     'Checked','off');
    set(handles.view_xy,     'Checked','off');
    set(handles.view_3d_axes,'Checked','off');
    imview_adjust_pos(handles);
%
function view_3d_axes_Callback(~, ~, ~)
warndlg('Not available in this version');
%     set(hObject,             'Checked','on');
%     set(handles.view_yz,     'Checked','off');
%     set(handles.view_xy,     'Checked','off');
%     set(handles.view_xz,     'Checked','off');
%     imview_adjust_pos(handles);

    
%
%  Axes position



function  imview_adjust_pos(handles)
%
TPlan=getappdata(handles.amb_interface,'TPlan');
%
if     strcmp(get(handles.view_xy,'Checked'),'on')   ==  1
       set(handles.amb_vis_01,'Visible','on','Position',[0.19 0.04 0.50 0.94],'Xlim',[1 TPlan.image.Nvoxels(1)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       set(handles.amb_dos_01,'Visible','on','Position',[0.19 0.04 0.50 0.94],'Xlim',[1 TPlan.image.Nvoxels(1)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       %
       set(handles.amb_vis_02,'Visible','on','Position',[0.71 0.52 0.28 0.46],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(1)]);
       set(handles.amb_dos_02,'Visible','on','Position',[0.71 0.52 0.28 0.46],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(1)]);
       set(handles.amb_vis_03,'Visible','on','Position',[0.71 0.04 0.28 0.46],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       set(handles.amb_dos_03,'Visible','on','Position',[0.71 0.04 0.28 0.46],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       %
       set(handles.amb_vis_04,'Visible','off'); set(get(handles.amb_vis_04,'Child'),'Visible','off');
elseif strcmp(get(handles.view_yz,'Checked'),'on')   ==  1
       set(handles.amb_vis_02,'Visible','on','Position',[0.19 0.04 0.50 0.94],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(1)]);
       set(handles.amb_dos_02,'Visible','on','Position',[0.19 0.04 0.50 0.94],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(1)]);       
       set(handles.amb_vis_01,'Visible','on','Position',[0.71 0.52 0.28 0.46],'Xlim',[1 TPlan.image.Nvoxels(1)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       set(handles.amb_dos_01,'Visible','on','Position',[0.71 0.52 0.28 0.46],'Xlim',[1 TPlan.image.Nvoxels(1)],'Ylim',[1 TPlan.image.Nvoxels(2)]);       
       set(handles.amb_vis_03,'Visible','on','Position',[0.71 0.04 0.28 0.46],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       set(handles.amb_vis_03,'Visible','on','Position',[0.71 0.04 0.28 0.46],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       set(handles.amb_vis_04,'Visible','off'); set(get(handles.amb_vis_04,'Child'),'Visible','off');
elseif strcmp(get(handles.view_xz,'Checked'),'on')   ==  1
       set(handles.amb_vis_03,'Visible','on','Position',[0.19 0.04 0.50 0.94],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       set(handles.amb_dos_03,'Visible','on','Position',[0.19 0.04 0.50 0.94],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       set(handles.amb_vis_02,'Visible','on','Position',[0.71 0.52 0.28 0.46],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(1)]);
       set(handles.amb_dos_02,'Visible','on','Position',[0.71 0.52 0.28 0.46],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(1)]);
       set(handles.amb_vis_01,'Visible','on','Position',[0.71 0.04 0.28 0.46],'Xlim',[1 TPlan.image.Nvoxels(1)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       set(handles.amb_dos_01,'Visible','on','Position',[0.71 0.04 0.28 0.46],'Xlim',[1 TPlan.image.Nvoxels(1)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       set(handles.amb_vis_04,'Visible','off'); set(get(handles.amb_vis_04,'Child'),'Visible','off');
elseif strcmp(get(handles.view_3d_axes,'Checked'),'on') == 1
       set(handles.amb_vis_01,'Visible','on','Position',[0.19 0.58 0.25 0.40],'Xlim',[1 TPlan.image.Nvoxels(1)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       set(handles.amb_vis_02,'Visible','on','Position',[0.46 0.58 0.25 0.40],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(1)]);
       set(handles.amb_vis_03,'Visible','on','Position',[0.73 0.58 0.25 0.40],'Xlim',[1 TPlan.image.Nvoxels(3)],'Ylim',[1 TPlan.image.Nvoxels(2)]);
       set(handles.amb_vis_04,'Visible','on','Position',[0.55 0.05 0.45 0.45])%,'Xlim',[1 TPlan.image.Nvoxels(1)],'Ylim',[1 TPlan.image.Nvoxels(2)]); 
end
%
%%



% ----------------------------------------------------------------
%%
%              EDIT Materials
function edit_all_materials_Callback(~, ~, handles)
TPlan = getappdata(handles.amb_interface,'TPlan');
idx   = handles.edit_all_materials.Value;
%
data=TPlan.plan.materials.(sprintf('mat%.0f',idx));
%
handles.material_name.String    = data.name;
handles.material_density.String = data.density;
%
if strcmp(data.tissue,'no')==1
    handles.is_tissue.Value=0;
else
    handles.is_tissue.Value=1;
end
% composition
Mat=[];
for i=2:size(data.composition,1)
  n=str2num(data.composition{i}(4:end));  
  if size(n,2)>2
    n=reshape(n,2,[])';
  end    
  Mat(end+1:end+size(n,1),1:2)=n;
end
Mat(:,1)=Mat(:,1)/1000;
Mat(end+1:100,:)=0;
handles.table_materials.Data=Mat;

function Save_materials_Callback(~, ~, handles)
file=which('amb_material.amb');
TPlan = getappdata(handles.amb_interface,'TPlan');
idx   = handles.edit_all_materials.Value;
%
data.name    = handles.material_name.String;
data.density = str2num(handles.material_density.String);
%
if handles.is_tissue.Value==0
    data.tissue='no';
else
    data.tissue='yes';
end
% composition
Mat=handles.table_materials.Data;
Mat(Mat(:,1)==0,:)=[];
Mat(:,1)=Mat(:,1)*1000;
%
data.composition{1,1}=['c composition of ' data.name ' d= ' num2str(data.density)];  
for i=1:size(Mat,1)
    data.composition{i+1,1}=['     ' num2str(Mat(i,1),'%d') '. ' num2str(Mat(i,2),'%12.6f')];
end
%
TPlan.plan.materials.(sprintf('mat%.0f',idx))=data;
%
mat=TPlan.plan.materials; %#ok<*NASGU>
save(file,'mat');
material_menu(handles);
%












function view_catheters_Callback(~, ~, handles)
TPlan=getappdata(gcf,'TPlan');
if isfield(TPlan.plan,'catheter')==0; return; end
if strcmp(get(handles.view_catheters,'Checked'),'on') 
    set(handles.view_catheters,'Checked','off');
    delete(getappdata(gcf,'Catheters'));
    return;
else
    set(handles.view_catheters,'Checked','on');
end
%
cat=fieldnames(TPlan.plan.catheter); 
[s,~] = listdlg('PromptString','Select catheters to show:','SelectionMode','multiple','ListString',cat);
plot_catheters(handles,TPlan,s);

function plot_catheters(handles,TPlan,s)
cont=0;
cat=fieldnames(TPlan.plan.catheter); 
for i=s
   cont=cont+1;
   TPlan.plan.catheter.(sprintf('%s',cat{i})).PointsEd=[];
   TPlan.plan.catheter.(sprintf('%s',cat{i})).PointsEd=TPlan.plan.catheter.(sprintf('%s',cat{i})).PointsVox;
   h(cont)=impoly(handles.amb_dos_01,TPlan.plan.catheter.(sprintf('%s',cat{i})).PointsVox(:,[1,2]),'Closed',0); %#ok<*AGROW>
   set(h(cont),'Tag',[num2str(i) ' 1 ' num2str(cont)]);
   addNewPositionCallback(h(cont),@cat_update); drawnow;
   % setPositionConstraintFcn(h(cont),makeConstrainToRectFcn('impoly', [1 TPlan.image.Nvoxels(1)], [1 TPlan.image.Nvoxels(2)]));
   %
   cont=cont+1;
   h(cont)=impoly(handles.amb_dos_02,TPlan.plan.catheter.(sprintf('%s',cat{i})).PointsVox(:,[3,2]),'Closed',0);
   set(h(cont),'Tag',[num2str(i) ' 2 ' num2str(cont)]);
   addNewPositionCallback(h(cont),@cat_update);  drawnow;
   % setPositionConstraintFcn(h(cont),makeConstrainToRectFcn('impoly', [1 TPlan.image.Nvoxels(3)], [1 TPlan.image.Nvoxels(1)]));
   %
   cont=cont+1;
   h(cont)=impoly(handles.amb_dos_03,TPlan.plan.catheter.(sprintf('%s',cat{i})).PointsVox(:,[3,1]),'Closed',0);
   set(h(cont),'Tag',[num2str(i) ' 3 ' num2str(cont)]);
   addNewPositionCallback(h(cont),@cat_update);  drawnow;
   % setPositionConstraintFcn(h(cont),makeConstrainToRectFcn('impoly', [1 TPlan.image.Nvoxels(3)], [1 TPlan.image.Nvoxels(2)]));
end
setappdata(gcf,'Cat_plot',1);
setappdata(gcf,'Catheters',h);
setappdata(gcf,'TPlan',TPlan);


function view_dwell_Callback(~, ~, handles)
TPlan=getappdata(gcf,'TPlan');
if isfield(TPlan.plan,'catheter')==0; return; end
%
if strcmp(get(handles.view_dwell,'Checked'),'on') 
    set(handles.view_dwell,'Checked','off');
    delete(findobj('Tag','dwell_pos'));
    delete(findobj('Tag','current_dwell_pos'));
    return;
else
    set(handles.view_dwell,'Checked','on');
end
cat=fieldnames(TPlan.plan.catheter); 
[s,~] = listdlg('PromptString','Select catheters to show:','SelectionMode','multiple','ListString',cat);
plotdwell(handles,s,TPlan);
%

%
function plotdwell(handles,s,TPlan)
lx1=diff(get(handles.amb_vis_01,'Xlim'));   ly1=diff(get(handles.amb_vis_01,'Ylim'));
lx2=diff(get(handles.amb_vis_02,'Xlim'));   ly2=diff(get(handles.amb_vis_02,'Ylim'));
lx3=diff(get(handles.amb_vis_03,'Xlim'));   ly3=diff(get(handles.amb_vis_03,'Ylim'));
%
%
TPlan.plan.dwellVox(:,1:3)=round(TPlan.plan.dwellVox(:,1:3));
for i=s
   Dw_list=[];
   Dw_list(:,1:5)=TPlan.plan.dwellVox(TPlan.plan.dwells(:,5)==i,1:5); 
   for j=1:size(Dw_list,1)
     if Dw_list(j,4)>0 % dwell time ==0  
       rectangle(handles.amb_vis_01,'Position',[Dw_list(j,1)-(lx1*0.005) Dw_list(j,2)-(ly1*0.005) lx1*0.01 ly1*0.01],'Tag','dwell_pos','FaceColor','none','EdgeColor','g'); 
       rectangle(handles.amb_vis_02,'Position',[Dw_list(j,3)-(lx2*0.005) Dw_list(j,2)-(ly2*0.005) lx2*0.01 ly2*0.01],'Tag','dwell_pos','FaceColor','none','EdgeColor','g'); 
       rectangle(handles.amb_vis_03,'Position',[Dw_list(j,3)-(lx3*0.005) Dw_list(j,1)-(ly3*0.005) lx3*0.01 ly3*0.01],'Tag','dwell_pos','FaceColor','none','EdgeColor','g'); 
     else
       rectangle(handles.amb_vis_01,'Position',[Dw_list(j,1)-(lx1*0.005) Dw_list(j,2)-(ly1*0.005) lx1*0.01 ly1*0.01],'Tag','dwell_pos','FaceColor','none','EdgeColor',[1 0.5 0]); 
       rectangle(handles.amb_vis_02,'Position',[Dw_list(j,3)-(lx2*0.005) Dw_list(j,2)-(ly2*0.005) lx2*0.01 ly2*0.01],'Tag','dwell_pos','FaceColor','none','EdgeColor',[1 0.5 0]); 
       rectangle(handles.amb_vis_03,'Position',[Dw_list(j,3)-(lx3*0.005) Dw_list(j,1)-(ly3*0.005) lx3*0.01 ly3*0.01],'Tag','dwell_pos','FaceColor','none','EdgeColor',[1 0.5 0]);  
     end
   end
end


function cont_pop_menu_Callback(~, ~, handles)
if handles.cont_pop_menu.UserData(handles.cont_pop_menu.Value)==1
   iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'View_on.png'];   iconFilename = strrep(iconFilename, '\', '/');
   str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
   set(handles.contour_on_off,'String',str,'UserData',1);  drawnow;
else
   iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'View_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
   str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
   set(handles.contour_on_off,'String',str,'UserData',0);  drawnow;
end
update_all_contours;

function cont_transp_Callback(~, ~, handles)
trans = getappdata(handles.amb_interface,'trans_contour');
def{1}=num2str(trans);
answer=inputdlg('Transparency 0 - 1','Contour',1,def);
trans=str2double(answer{1});
if isnumeric(trans)==1
    setappdata(handles.amb_interface,'trans_contour',trans);
    update_all_contours;
end
%
function update_all_contours
% update contours
fig_r            =  findobj('Tag','amb_interface');
TPlan            =  getappdata(fig_r,'TPlan');
current_slices   =  getappdata(fig_r,'slices');
ax               =  getappdata(fig_r,'ax');
trans            =  getappdata(fig_r,'trans_contour');
if isempty(trans)==1
    trans=0.5;
    setappdata(fig_r,'trans_contour',trans);
end
update_contours(4,TPlan,current_slices,ax,trans);




function start_simulation_Callback(~, ~, handles)
if strcmp(handles.EB_source.Checked,'on')==1  % External beam
    simulation_EB_main; 
else                                          % Brachy 
    simulation=simulation_main;
end


function sim_nps_Callback(~, ~, handles)
answer=inputdlg('Number of particles','NPS',1,{handles.sim_nps.Label});
handles.sim_nps.Label=answer{:};

function dens_lib_Callback(~, ~, handles)
handles.dens_lib.Checked ='on';
handles.dens_ct.Checked  ='off';

function dens_ct_Callback(~, ~, handles)
handles.dens_ct.Checked   ='on';
handles.dens_lib.Checked  ='off';

function score_dwm_Callback(obj, ~, ~)
if strcmp(obj.Checked,'on')
    obj.Checked='off';
else
    obj.Checked='on';
end

function score_dmm_Callback(obj, ~, ~)
if strcmp(obj.Checked,'on')
    obj.Checked='off';
else
    obj.Checked='on';
end

function score_energy_Callback(obj, ~, ~)
if strcmp(obj.Checked,'on')
    obj.Checked='off';
else
    obj.Checked='on';
end

function score_dose_Callback(obj, ~, ~)
if strcmp(obj.Checked,'on')
    obj.Checked='off';
else
    obj.Checked='on';
end

function vox_no_Callback(~, ~, ~)


function phantom_user_Callback(~, ~, handles)
phantom_pos=getappdata(handles.amb_interface,'phantom_region');
if isempty(phantom_pos)==1
    errordlg('Go to the planning module and define a phantom first!');
    return;
end
handles.phantom_user.Checked='on';
handles.phantom_all.Checked='off';
TPlan=getappdata(handles.amb_interface,'TPlan');
TPlan.plan.phantom(1,1:2)=[phantom_pos.xy_pos(1) phantom_pos.xy_pos(1)+phantom_pos.xy_pos(3)]; 
TPlan.plan.phantom(2,1:2)=[phantom_pos.xy_pos(2) phantom_pos.xy_pos(2)+phantom_pos.xy_pos(4)];
TPlan.plan.phantom(3,1:2)=[phantom_pos.yz_pos(1) phantom_pos.yz_pos(1)+phantom_pos.yz_pos(3)];
TPlan.plan.phantom=round(TPlan.plan.phantom);
setappdata(handles.amb_interface,'TPlan',TPlan);


function phantom_all_Callback(~, ~, handles)
handles.phantom_user.Checked='off';
handles.phantom_all.Checked ='on';
TPlan=getappdata(handles.amb_interface,'TPlan');
TPlan.plan.phantom(1,1:2)=[1 TPlan.image.Nvoxels(2)]; 
TPlan.plan.phantom(2,1:2)=[1 TPlan.image.Nvoxels(1)];
TPlan.plan.phantom(3,1:2)=[1 TPlan.image.Nvoxels(3)];
setappdata(handles.amb_interface,'TPlan',TPlan);


function vox_on_Callback(~, ~,  handles)
handles.vox_on.Checked  = 'on';
handles.vox_off.Checked = 'off';

function vox_off_Callback(~, ~, handles)
handles.vox_on.Checked  = 'off';
handles.vox_off.Checked = 'on';

function dosegrid_user_Callback(~, ~, handles)
score_pos=getappdata(handles.amb_interface,'score_region');
if isempty(score_pos)==1
    errordlg('Go to the planning module and define a dose grid first!');
    return;
end
%
handles.dosegrid_tps.Checked  = 'off';
handles.dosegrid_all.Checked  = 'off';
handles.dosegrid_user.Checked = 'on';
%
TPlan=getappdata(handles.amb_interface,'TPlan');
TPlan.plan.dosegrid(1,1:2)=[score_pos.xy_pos(1) score_pos.xy_pos(1)+score_pos.xy_pos(3)]; 
TPlan.plan.dosegrid(2,1:2)=[score_pos.xy_pos(2) score_pos.xy_pos(2)+score_pos.xy_pos(4)];
TPlan.plan.dosegrid(3,1:2)=[score_pos.yz_pos(1) score_pos.yz_pos(1)+score_pos.yz_pos(3)];
TPlan.plan.dosegrid=round(TPlan.plan.dosegrid);
setappdata(handles.amb_interface,'TPlan',TPlan);

function dosegrid_all_Callback(~, ~, handles)
handles.dosegrid_tps.Checked  ='off';
handles.dosegrid_all.Checked  ='on';
handles.dosegrid_user.Checked ='off';
TPlan=getappdata(handles.amb_interface,'TPlan');
TPlan.plan.dosegrid(1,1:2)=[1 TPlan.image.Nvoxels(2)]; 
TPlan.plan.dosegrid(2,1:2)=[1 TPlan.image.Nvoxels(1)];
TPlan.plan.dosegrid(3,1:2)=[1 TPlan.image.Nvoxels(3)];
setappdata(handles.amb_interface,'TPlan',TPlan);


function source_gmp_Callback(~, ~, handles)
handles.source_gmp.Checked           ='on';
handles.source_mselectronv2.Checked  ='off';
handles.no_source.Checked            ='off';
handles.EB_source.Checked            ='off';

function source_mselectronv2_Callback(~, ~, handles)
handles.source_gmp.Checked           ='off';
handles.source_mselectronv2.Checked  ='on';
handles.no_source.Checked            ='off';
handles.EB_source.Checked            ='off';


function no_source_Callback(~, ~, handles)
handles.source_gmp.Checked           ='off';
handles.source_mselectronv2.Checked  ='off';
handles.no_source.Checked            ='on';
handles.EB_source.Checked            ='off';

% --------------------------------------------------------------------
function EB_source_Callback(~, ~, handles)
handles.source_gmp.Checked           ='off';
handles.source_mselectronv2.Checked  ='off';
handles.no_source.Checked            ='off';
handles.EB_source.Checked            ='on';

function contour_on_off_Callback(~, ~, handles)
if handles.contour_on_off.UserData==0
   iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'View_on.png'];   iconFilename = strrep(iconFilename, '\', '/');
   str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
   set(handles.contour_on_off,'String',str); drawnow;
%    %
   rgb_color=uisetcolor;
   handles.cont_pop_menu.UserData(handles.cont_pop_menu.Value,2:4)=rgb_color;
   handles.cont_pop_menu.UserData(handles.cont_pop_menu.Value,1)  =1;
   %
   rgb_color=round(rgb_color*255);
   hex(:,1) = '#'; hex(:,2:7) = reshape(sprintf('%02X',rgb_color.'),6,[]).'; 
   handles.cont_pop_menu.String{handles.cont_pop_menu.Value}(19:25)= hex;
   handles.contour_on_off.UserData=1; 
else
   handles.cont_pop_menu.UserData(handles.cont_pop_menu.Value,1)=0;
   iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'View_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
   str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
   set(handles.contour_on_off,'String',str);  drawnow;
   handles.cont_pop_menu.UserData(handles.cont_pop_menu.Value,2:4)=[0 0 0];
   handles.cont_pop_menu.String{handles.cont_pop_menu.Value}(19:25)='#000000';
   handles.contour_on_off.UserData=0; 
end
% update contours
update_all_contours;

function contour_all_off_Callback(~, ~, handles)
set_cont_view_off(handles);
%
handles.cont_pop_menu.UserData(:,:)=0;
setappdata(handles.amb_interface,'plot_contour',0);
%
% update contours
update_all_contours;

function set_cont_view_off(handles)
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'View_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.contour_on_off,'String',str,'UserData',0); 


function dose_menu_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
setappdata(handles.amb_interface,'show_dose',handles.dose_menu.String{handles.dose_menu.Value});
dose_contrast_histogram(1);  
%
update_dose_all(handles,handles.dose_menu.String{handles.dose_menu.Value});


function update_dose_all(handles,dose)
ax      = getappdata(handles.amb_interface,'ax');
if strcmp(dose,'none')
    ax.dos01.AlphaData=0;
    ax.dos02.AlphaData=0;
    ax.dos03.AlphaData=0;
else
    trans=getappdata(handles.amb_interface,'trans_dose');
    if isempty(trans)==1
        trans=0.5;
        setappdata(handles.amb_interface,'trans_dose',trans);
    end
    %
    update_dose(4,getappdata(handles.amb_interface,'TPlan'), ...
                  getappdata(handles.amb_interface,'slices'), ...
                  ax,trans,dose);
end

function set_contrast_limits_Callback(~, ~, ~)
msgbox({'Set the limits and double click on the axes','Click on the contrast color map NOT on the histogram or the guide bar'});



function dose_menu_CreateFcn(hObject, ~, ~)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function restore_dosecontrast_Callback(~, ~, ~)
dose_contrast_histogram(2);


function dose_transp_Callback(~, ~, handles)
trans = getappdata(handles.amb_interface,'trans_contour');
def{1}=num2str(trans);
answer=inputdlg('Transparency 0 - 1','Contour',1,def);
trans=str2double(answer{1});
if isnumeric(trans)==1
    setappdata(handles.amb_interface,'trans_dose',trans);
    update_dose_all(handles,getappdata(handles.amb_interface,'show_dose'));
end




function restore_contrast_Callback(~, ~, handles)
TPlan =getappdata(handles.amb_interface,'TPlan');
%
adjust_contrast_axes(handles,TPlan,2);

% data tip
function amb_datatip_OnCallback(~, ~, handles)
handles.amb_info.Visible  ='on';
handles.amb_info.Position =[0.01    0.002    0.65    0.03];
% set(handles.amb_interface,'WindowButtonMotionFcn', @data_tip);
set(handles.amb_interface,'WindowButtonMotionFcn', @data_tip);


function amb_datatip_OffCallback(~, ~, handles)
handles.amb_info.Visible='off';
set(handles.amb_interface,'pointer','default');
set(handles.amb_interface,'WindowButtonMotionFcn', []);



function material_pop_menu_CreateFcn(hObject, ~, ~)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end








function materials_range_CreateFcn(hObject, ~, ~)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ct_cal_pop_menu_Callback(~, ~, ~)


function ct_cal_pop_menu_CreateFcn(hObject, ~, ~)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton7_Callback(~, ~, ~)


function pushbutton8_Callback(~, ~, ~)








function material_name_Callback(~, ~, ~)


function material_name_CreateFcn(hObject, ~, ~)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function is_tissue_Callback(~, ~, ~)


function material_density_Callback(hObject, ~, ~)
d=str2num(hObject.String);
hObject.String=num2str(d(1));

function material_density_CreateFcn(hObject, ~,~)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function edit_all_materials_CreateFcn(hObject, ~,~)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ct_cal_edit_name_Callback(~, ~, ~)


function ct_cal_edit_name_CreateFcn(hObject, ~,~)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function checkbox2_Callback(~, ~, ~)


function edit5_Callback(~, ~, ~)


function edit5_CreateFcn(hObject, ~, ~)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ct_cal_edit_menu_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
name=handles.ct_cal_edit_menu.String(handles.ct_cal_edit_menu.Value);
if strcmp(name,'Library')==1
    warndlg({'Libray uses a specif density for each material','Use the material settings to check the density'});
    handles.ct_cal_density.Data=[];
    return;
elseif strcmp(name,'deleted...')==1
    warndlg({'This calibration has been delete','Save to apply the changes'});
    handles.ct_cal_density.Data=[];
    return;
end
%
handles.ct_cal_edit_name.String=name;
handles.ct_cal_density.Data=TPlan.plan.ct_cal.(sprintf('%s',name{1})).cal;
handles.ct_cal_density.Data(end+1:100,:)=0;
plot_ct_cal(handles);

function ct_cal_density_CellEditCallback(~, ~, handles)
plot_ct_cal(handles);

function [] = plot_ct_cal(handles)
d=handles.ct_cal_density.Data;
d((d(:,1)==0 & d(:,2)==0) ,:)=[];
plot(handles.ct_cal_axes,d(:,1),d(:,2),'b-o');

function ct_cal_delete_Callback(~, ~, handles)
handles.ct_cal_edit_menu.String{handles.ct_cal_edit_menu.Value}='deleted...';
ct_cal_edit_menu_Callback(1, 1, handles);

function edit_ct_cal_Callback(~, ~, handles)
if strcmp(handles.ct_cal_panel.Visible,'on')==0
    set(handles.ct_cal_panel,'Visible','on','Position',[0.18 0.0 0.82 1]);
    ct_cal_edit_menu_Callback(1, 1, handles);
else
    set(handles.ct_cal_panel,'Visible','off');
end

function ct_cal_edit_menu_CreateFcn(hObject, ~, ~)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ct_cal_save_Callback(~, ~, handles)
new_name=strtrim(handles.ct_cal_edit_name.String);
handles.ct_cal_edit_menu.String(handles.ct_cal_edit_menu.Value)=new_name;
TPlan=getappdata(handles.amb_interface,'TPlan');
TPlan.plan.ct_cal.(sprintf('%s',new_name{1})).cal=handles.ct_cal_density.Data((handles.ct_cal_density.Data(:,1)~=0 & handles.ct_cal_density.Data(:,2)~=0),:);
setappdata(handles.amb_interface,'TPlan',TPlan);
%
% save non deleted files
for i=1:size(handles.ct_cal_edit_menu.String,1)
  name=handles.ct_cal_edit_menu.String(i);
  if strcmp(name,'deleted...')==0
      CT_calibration.(sprintf('%s',name{1}))=TPlan.plan.ct_cal.(sprintf('%s',name{1}));  %#ok<*STRNU>
  end  
end
%
save(which('amb_ct_calibration.amb'),'CT_calibration');
handles.ct_cal_edit_menu.Value=1;
ct_cal_menu(handles);
ct_cal_edit_menu_Callback(1, 1, handles);


function new_ct_cal_Callback(~, ~, handles)
answer=inputdlg('CT calibration name','New Cal',1,{'New'});
TPlan=getappdata(handles.amb_interface,'TPlan');
TPlan.plan.ct_cal.(sprintf('%s',answer{1})).cal(1:1:2)=[0 0];
setappdata(handles.amb_interface,'TPlan',TPlan);
handles.ct_cal_edit_menu.String{end+1}=answer{1};
handles.ct_cal_edit_menu.Value=size(handles.ct_cal_edit_menu.String,1);
ct_cal_edit_menu_Callback(1, 1, handles);


function cat_menu_Callback(~, ~, handles)
TPlan = getappdata(handles.amb_interface,'TPlan');
if isfield(TPlan.plan,'dwells')==1
  dw    = TPlan.plan.dwells((TPlan.plan.dwells(:,5)==handles.cat_menu.Value),4);
  handles.dwell_table.Data=dw;
end


function cat_menu_CreateFcn(hObject, ~, ~)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% calculate new dwell positions
function save_cat_change_Callback(~, ~, handles)
TPlan  =  getappdata(handles.amb_interface,'TPlan');
if isfield(TPlan.plan,'dwells')==0; return; end
cat    =  handles.cat_menu.Value;
%%
% distance from dw pos to the tip
dw     = TPlan.plan.dwells((TPlan.plan.dwells(:,5)==cat),:);
cat_p  = TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).Points;
%
cat_n  = TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).PointsEd;
cat_n  = (cat_n-1);
cat_n  = bsxfun(@times,cat_n,TPlan.image.Resolution);
cat_n  = bsxfun(@plus,cat_n,TPlan.image.ImagePositionPatient);
%
for i=2:size(cat_n)
  p=cat_n(i,1:3)-cat_n(i-1,1:3);  
  cat_n(i,4)=sqrt(sum(p.^2));
  cat_n(i,5)=sum(cat_n(1:i,4));
end
%
for i=1:size(dw,1)
% find segment - old catheter
idx = dsearchn(cat_p,dw(i,1:3));
if idx==size(cat_p,1) || (idx>1 && sqrt(sum((cat_p(idx-1,1:3)-dw(i,1:3)).^2)) < sqrt(sum((cat_p(idx-1,1:3)-cat_p(idx,1:3)).^2)))
   idx=idx-1; % check if it is after or before the closest point
end
%
% distance from the tip
if idx==1
   p=cat_p(idx,1:3)-dw(i,1:3);
   d=sqrt(sum(p.^2));
else
   d=0;
   for j=1:1:idx-1
     p=cat_p(j,1:3)-cat_p(j+1,1:3);  
     d=d+sqrt(sum(p.^2));
   end
     p=cat_p(idx,1:3)-dw(i,1:3);
     d=d+sqrt(sum(p.^2));
end
%
% finf new interval - cat_new
id=find(cat_n(:,5)>d,1)-1; %pos between id and id+1
p=(-cat_n(id,1:3)+cat_n(id+1,1:3));
theta_z  = atand(p(3)/sum(p(1:2).^2)^.5);
theta_xy = atand(p(2)/p(1));
% dist. dw pos. next point
d_n=d-cat_n(id,5);
%
dw_new(i,1)=abs(d_n*cosd(theta_z) *cosd(theta_xy))*p(1)/abs(p(1));
dw_new(i,2)=abs(d_n*cosd(theta_z) *sind(theta_xy))*p(2)/abs(p(2));
dw_new(i,3)=abs(d_n*sind(theta_z))*p(3)/abs(p(3));
%
dw_new(isnan(dw_new))=0;
dw_new(i,1:3)=dw_new(i,1:3)+cat_n(id,1:3);
%
end
%
cat_v  = bsxfun(@minus,cat_n(:,1:3),TPlan.image.ImagePositionPatient);
cat_v  = bsxfun(@rdivide,cat_v,TPlan.image.Resolution);
cat_v  = cat_v +1;
%
TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).Points    = cat_n(:,1:3);
TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).PointsEd  = cat_v;
TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).PointsVox = cat_v;
%
TPlan.plan.dwells((TPlan.plan.dwells(:,5)==cat),1:3)=dw_new;
%
dwellVox  = bsxfun(@minus,  dw_new(:,1:3), TPlan.image.ImagePositionPatient);
dwellVox  = bsxfun(@rdivide,dwellVox,     TPlan.image.Resolution); 
dwellVox  = dwellVox+1;
%
TPlan.plan.dwells((TPlan.plan.dwells(:,5)==cat),1:3)    = dw_new(:,1:3);
TPlan.plan.dwellVox((TPlan.plan.dwells(:,5)==cat),1:3) = dwellVox;
%
setappdata(handles.amb_interface,'TPlan',TPlan);
%
delete(findobj('Tag','dwell_pos'));
delete(findobj('Tag','current_dwell_pos'))
plotdwell(handles,cat,TPlan);
handles.view.dwell.Checked='on';
%



%%

function set_phantom_size_Callback(~, ~, handles)
%
if isempty(findobj('Tag','phant_xyz'))==0 
    delete(findobj('Tag','phant_xyz'));
    return;
end
%
setappdata(handles.amb_interface,'Phan_rec',1);
phantom_pos=getappdata(handles.amb_interface,'phantom_region');
if isempty(phantom_pos)==1 
  x=handles.amb_dos_01.XLim; y=handles.amb_dos_01.YLim;
  pos=[1 1 diff(x)*0.5 diff(y)*0.5];
  phantom_pos.xy_pos=double(pos);
  %
  x=handles.amb_dos_02.XLim; y=handles.amb_dos_02.YLim;
  pos=[1 1 diff(x)*0.5 diff(y)*0.5];
  phantom_pos.yz_pos=double(pos);
  %
  x=handles.amb_dos_03.XLim; y=handles.amb_dos_03.YLim;
  pos=[1 1 diff(x)*0.5 diff(y)*0.5];
  phantom_pos.xz_pos=double(pos);  
  %
end
TPlan=getappdata(handles.amb_interface,'TPlan');
phantom_pos.xy=imrect(handles.amb_dos_01,phantom_pos.xy_pos); %#ok<*AGROW>
set(phantom_pos.xy,'Tag','phant_xyz');   
setColor(phantom_pos.xy,'g');
addNewPositionCallback(phantom_pos.xy,@phantom_size);
setPositionConstraintFcn(phantom_pos.xy,makeConstrainToRectFcn('imrect', [1 double(TPlan.image.Nvoxels(2))], [1 double(TPlan.image.Nvoxels(1))]));
%
phantom_pos.yz=imrect(handles.amb_dos_02,phantom_pos.yz_pos); %#ok<*AGROW>
set(phantom_pos.yz,'Tag','phant_xyz');   
setColor(phantom_pos.yz,'g');
addNewPositionCallback(phantom_pos.yz,@phantom_size);
setPositionConstraintFcn(phantom_pos.yz,makeConstrainToRectFcn('imrect', [1 double(TPlan.image.Nvoxels(3))], [1 double(TPlan.image.Nvoxels(1))]));
%
phantom_pos.xz=imrect(handles.amb_dos_03,phantom_pos.xz_pos); %#ok<*AGROW>
set(phantom_pos.xz,'Tag','phant_xyz');   
setColor(phantom_pos.xz,'g');
addNewPositionCallback(phantom_pos.xz,@phantom_size);
setPositionConstraintFcn(phantom_pos.xz,makeConstrainToRectFcn('imrect', [1 double(TPlan.image.Nvoxels(3))], [1 double(TPlan.image.Nvoxels(2))]));
%
setappdata(handles.amb_interface,'phantom_region',phantom_pos);

%

function set_dose_grid_Callback(~, ~, handles)
if isempty(findobj('Tag','score_xyz'))==0 
    delete(findobj('Tag','score_xyz'));
    return;
end
%
setappdata(handles.amb_interface,'score_rec',1);
score_pos=getappdata(handles.amb_interface,'score_region');
%
TPlan=getappdata(handles.amb_interface,'TPlan');
%
if isempty(score_pos)==1
  %
  % Try to use TPS grid as reference
  if isfield(TPlan.dose,'ImagePositionPatient')==1
      %
      Pi = floor((TPlan.dose.ImagePositionPatient-TPlan.image.ImagePositionPatient)./TPlan.image.Resolution);
      Si = ceil(size(TPlan.dose.Dose_TPS_NI).*(TPlan.dose.Resolution./TPlan.image.Resolution));
      score_pos.xy_pos = [Pi(1) Pi(2) Si(2) Si(1)];
      score_pos.yz_pos = [Pi(3) Pi(2) Si(3) Si(1)];
      score_pos.xz_pos = [Pi(3) Pi(1) Si(3) Si(2)];    
      %
  else
      %
      x=handles.amb_dos_01.XLim; y=handles.amb_dos_01.YLim;
      pos=[1 1 diff(x)*0.5 diff(y)*0.5];
      score_pos.xy_pos=double(pos);
      %
      x=handles.amb_dos_02.XLim; y=handles.amb_dos_02.YLim;
      pos=[1 1 diff(x)*0.5 diff(y)*0.5];
      score_pos.yz_pos=double(pos);
      %
      x=handles.amb_dos_03.XLim; y=handles.amb_dos_03.YLim;
      pos=[1 1 diff(x)*0.5 diff(y)*0.5];
      score_pos.xz_pos=double(pos);  
      %
  end
end

score_pos.xy=imrect(handles.amb_dos_01,score_pos.xy_pos); %#ok<*AGROW>
set(score_pos.xy,'Tag','score_xyz');   
setColor(score_pos.xy,'yellow');
addNewPositionCallback(score_pos.xy,@score_size);
setPositionConstraintFcn(score_pos.xy,makeConstrainToRectFcn('imrect', [1 double(TPlan.image.Nvoxels(1))], [1 double(TPlan.image.Nvoxels(2))]));
%
score_pos.yz=imrect(handles.amb_dos_02,score_pos.yz_pos); %#ok<*AGROW>
set(score_pos.yz,'Tag','score_xyz');   
setColor(score_pos.yz,'yellow');
addNewPositionCallback(score_pos.yz,@score_size);
setPositionConstraintFcn(score_pos.yz,makeConstrainToRectFcn('imrect', [1 double(TPlan.image.Nvoxels(3))], [1 double(TPlan.image.Nvoxels(2))]));
%
score_pos.xz=imrect(handles.amb_dos_03,score_pos.xz_pos); %#ok<*AGROW>
set(score_pos.xz,'Tag','score_xyz');   
setColor(score_pos.xz,'yellow');
addNewPositionCallback(score_pos.xz,@score_size);
setPositionConstraintFcn(score_pos.xz,makeConstrainToRectFcn('imrect', [1 double(TPlan.image.Nvoxels(3))], [1 double(TPlan.image.Nvoxels(1))]));

%
setappdata(handles.amb_interface,'score_region',score_pos);




function dwell_table_CreateFcn(hObject, ~, ~)
hObject.Data=0;
hObject.ColumnName={'Time(s)'};


function dwell_table_CellEditCallback(hObject, eventdata, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
idx=find(TPlan.plan.dwells(:,5)==handles.cat_menu.Value);
idx=idx(eventdata.Indices(1));
TPlan.plan.dwells(idx,4)  = hObject.Data(eventdata.Indices(1),1);
TPlan.plan.dwellVox(idx,4)= hObject.Data(eventdata.Indices(1),1);
setappdata(handles.amb_interface,'TPlan',TPlan);
update_images(4,round(TPlan.plan.dwellVox(idx,[3 1 2])));
update_slice_guides;

function dwell_table_CellSelectionCallback(~, eventdata, handles)
TPlan=getappdata(handles.amb_interface,'TPlan'); drawnow;
idx=find(TPlan.plan.dwells(:,5)==handles.cat_menu.Value);
idx=idx(eventdata.Indices(1));
update_images(4,round(TPlan.plan.dwellVox(idx,[3 1 2])-0.5));
update_slice_guides;

function update_slice_guides
%
fig_r          =  findobj('Tag','amb_interface');
h              =  getappdata(fig_r,'view_patch');
sli            =  getappdata(fig_r,'slices');
%
% ax_01
P1=get(h.slice_rec_up01,'Position'); P1(1)=sli(2)-P1(3)/2;
P2=get(h.slice_rec_bt01,'Position'); P2(1)=P1(1);
set(h.slice_rec_up01,'Position',P1);
set(h.slice_rec_bt01,'Position',P2);
set(h.slice_lin_ve01,'XData',[sli(2) sli(2)]);
%
P1=get(h.slice_rec_lf01,'Position'); P1(2)=sli(3)-P1(4)/2;
P2=get(h.slice_rec_ri01,'Position'); P2(2)=P1(2);
set(h.slice_rec_lf01,'Position',P1);
set(h.slice_rec_ri01,'Position',P2);
set(h.slice_lin_ho01,'YData',[sli(3) sli(3)]);
% ax2
P1=get(h.slice_rec_up02,'Position'); P1(1)=sli(1)-P1(3)/2;
P2=get(h.slice_rec_bt02,'Position'); P2(1)=P1(1);
set(h.slice_rec_up02,'Position',P1);
set(h.slice_rec_bt02,'Position',P2);
set(h.slice_lin_ve02,'XData',[sli(1) sli(1)]);
%
P1=get(h.slice_rec_lf02,'Position'); P1(2)=sli(3)-P1(4)/2;
P2=get(h.slice_rec_ri02,'Position'); P2(2)=P1(2);
set(h.slice_rec_lf02,'Position',P1);
set(h.slice_rec_ri02,'Position',P2);
set(h.slice_lin_ho02,'YData',[sli(3) sli(3)]);
% ax3
P1=get(h.slice_rec_up03,'Position'); P1(1)=sli(1)-P1(3)/2;
P2=get(h.slice_rec_bt03,'Position'); P2(1)=P1(1);
set(h.slice_rec_up03,'Position',P1);
set(h.slice_rec_bt03,'Position',P2);
set(h.slice_lin_ve03,'XData',[sli(1) sli(1)]);
%
P1=get(h.slice_rec_lf03,'Position'); P1(2)=sli(2)-P1(4)/2;
P2=get(h.slice_rec_ri03,'Position'); P2(2)=P1(2);
set(h.slice_rec_lf03,'Position',P1);
set(h.slice_rec_ri03,'Position',P2);
set(h.slice_lin_ho03,'YData',[sli(2) sli(2)]);


function import_dose_Callback(~, ~, handles)
%
%
% select files
[files,folder]=uigetfile({'*.dcm;*.mat;*.o'},'Multiselect','on');
%
if size(files,1)==0; return; end
if iscell(files)==0; files = {files}; end
%
for kk=1:size(files,2)
    result = read_results(folder,files{kk});
    TPlan  = getappdata(handles.amb_interface,'TPlan');
    %
    if isstruct(result)==0 && result==-99
        dose_menu(handles,TPlan);
        dose_menu_Callback(1,1,handles);
        continue;
    end
    %
    dmm=0; 
    n=inputdlg('ID:','Import MC',[1 30],{files{kk}(1:end-4)});
    if size(n,2)==0
        return;
    end
    %
    % Mean energy 
    M_en=0;
    %
    % Absolute value? 
    if isprop(TPlan,'plan')==1 && isfield(TPlan.plan,'TotalAirKerma')==1
        fc=TPlan.plan.TotalAirKerma/2.6350e-04/1e6;  % plan_kerma/(sim_kerma*3600)*1E5 (=> Gy);
        answer=inputdlg('Conversion to dose - Type 1 to keep the original values','MC2DOSE',1,{num2str(fc,'%e')});
        if fc==str2double(answer{:})
          warndlg({'AMIGO multiplies the dose by an aproximate value. (Averaged air kerma rate from different sources) It is reasonable for most (NOT ALL) the cases.',' ','YOU SHOULD CALCULATE THE CONVERSION VALUE'}); 
        end
        fc=str2double(answer{:});
    end
    %
    for i=1:length(fieldnames(result))-1
        %
        interpolate=zeros(3,1);
        if       result.(sprintf('r%.0f',i)).tally_number==4   % dose to water  in medium 
            TPlan.dose.(sprintf([n{1} '_Dwm'])).Dose                       = result.(sprintf('r%.0f',i)).Result;
            TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf('uncert'))        = result.(sprintf('r%.0f',i)).Uncert;
            TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf('resolution'))(1) = mean(diff(result.(sprintf('r%.0f',i)).y))*10;  % mm
            TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf('resolution'))(2) = mean(diff(result.(sprintf('r%.0f',i)).z))*10;  % mm 
            TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf('resolution'))(3) = mean(diff(result.(sprintf('r%.0f',i)).x))*10;  % mm  
            %
            TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf('ref'))(1)        = result.(sprintf('r%.0f',i)).y(1)*10;  % mm
            TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf('ref'))(2)        = result.(sprintf('r%.0f',i)).z(1)*10;  % mm
            TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf('ref'))(3)        = result.(sprintf('r%.0f',i)).x(1)*10;  % mm       
            %
            TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf('ref'))           = TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf('ref'))+TPlan.image.ImagePositionPatient;
            %
            if TPlan.dose.(sprintf([n{1} '_Dwm'])).resolution ~= TPlan.image.Resolution 
                TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf([n{1} '_Dwm_NI']))=result.(sprintf('r%.0f',i)).Result;
                if isprop(TPlan,'plan')==1 && isfield(TPlan.plan,'TotalAirKerma')==1
                   TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf([n{1} '_Dwm_NI']))=TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf([n{1} '_Dwm_NI']))*fc;
                end
            end        
            %
            [TPlan.dose.(sprintf([n{1} '_Dwm'])).Dose,TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf('uncert'))] = ...
                              interp_mc_dose(TPlan,result.(sprintf('r%.0f',i)).Result,result.(sprintf('r%.0f',i)).Uncert,TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf('resolution')),TPlan.dose.(sprintf([n{1} '_Dwm'])).(sprintf('ref')));
            %
            % Absolute value? 
            if isprop(TPlan,'plan')==1 && isfield(TPlan.plan,'TotalAirKerma')==1
                TPlan.dose.(sprintf([n{1} '_Dwm'])).Dose = TPlan.dose.(sprintf([n{1} '_Dwm'])).Dose*fc;
            end        
            %
            % TPlan.dose.MC_Dwm(TPlan.plan.Mat_HU==1)=0;
            setappdata(handles.amb_interface,'TPlan',TPlan);
            %
            %
        end
        %
        if   result.(sprintf('r%.0f',i)).tally_number==14  % dose to medium in medium
            TPlan=getappdata(handles.amb_interface,'TPlan');
            TPlan.dose.(sprintf([n{1} '_Dmm'])).Dose                       = result.(sprintf('r%.0f',i)).Result;
            TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf('uncert'))        = result.(sprintf('r%.0f',i)).Uncert;
            TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf('resolution'))(1) = mean(diff(result.(sprintf('r%.0f',i)).y))*10;  % mm
            TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf('resolution'))(2) = mean(diff(result.(sprintf('r%.0f',i)).z))*10;  % mm 
            TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf('resolution'))(3) = mean(diff(result.(sprintf('r%.0f',i)).x))*10;  % mm  
            %
            TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf('ref'))(1)        = result.(sprintf('r%.0f',i)).y(1)*10;  % mm
            TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf('ref'))(2)        = result.(sprintf('r%.0f',i)).z(1)*10;  % mm
            TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf('ref'))(3)        = result.(sprintf('r%.0f',i)).x(1)*10;  % mm       
            %
            TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf('ref'))           = TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf('ref'))+TPlan.image.ImagePositionPatient;
            %

            %
            if TPlan.dose.(sprintf([n{1} '_Dmm'])).resolution ~= TPlan.image.Resolution 
                TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf([n{1} '_Dmm_NI']))=result.(sprintf('r%.0f',i)).Result;
                if isprop(TPlan,'plan')==1 && isfield(TPlan.plan,'TotalAirKerma')==1
                   TPlan.dose.(sprintf([n{1} '_Dmm'])).Dose_NI = TPlan.dose.(sprintf([n{1} '_Dmm'])).Dose_NI*fc;
                end
            end
            %        
            [TPlan.dose.(sprintf([n{1} '_Dmm'])).Dose,TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf('uncert'))] = ...
                              interp_mc_dose(TPlan,result.(sprintf('r%.0f',i)).Result,result.(sprintf('r%.0f',i)).Uncert,TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf('resolution')),TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf('ref')));
            %
            % TPlan.dose.MC_Dmm(TPlan.dose.MC_Dmm<0)=0;
            % Absolute value? 
            if isprop(TPlan,'plan')==1 && isfield(TPlan.plan,'TotalAirKerma')==1
                TPlan.dose.(sprintf([n{1} '_Dmm'])).Dose = TPlan.dose.(sprintf([n{1} '_Dmm'])).Dose*fc;
            end        
            %
            if isfield(TPlan.plan,'Density')==0
                warndlg({'You are trying to import Dmm values that need to be devided by the Density','However, there is no density map !!!'});
            else
                TPlan.dose.(sprintf([n{1} '_Dmm'])).Dose                                                  = TPlan.dose.(sprintf([n{1} '_Dmm'])).Dose./double(TPlan.plan.Density);
                TPlan.dose.(sprintf([n{1} '_Dmm'])).Dose(isnan(TPlan.dose.(sprintf([n{1} '_Dmm'])).Dose)) = 0;
                TPlan.dose.(sprintf([n{1} '_Dmm'])).Dose(isinf(TPlan.dose.(sprintf([n{1} '_Dmm'])).Dose)) = 0;
                % Non interpolated dat needs to be divided by the density 
                if isfield(TPlan.dose.(sprintf([n{1} '_Dmm'])),(sprintf([n{1} '_Dmm_NI'])))==1
                    %
                    Shift     = round((TPlan.dose.(sprintf([n{1} '_Dmm'])).ref-TPlan.image.ImagePositionPatient)./TPlan.image.Resolution);
                    Res_ratio = TPlan.dose.(sprintf([n{1} '_Dmm'])).resolution ./ TPlan.image.Resolution;
                    TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf([n{1} '_Dmm_NI'])) = divide_dens(TPlan.dose.(sprintf([n{1} '_Dmm'])).(sprintf([n{1} '_Dmm_NI'])),TPlan,Res_ratio,Shift);
                    %
                end
                %
                %
                warndlg('Dmm values were devided by the Density map');
            end
            setappdata(handles.amb_interface,'TPlan',TPlan);
        end
        %
        if       result.(sprintf('r%.0f',i)).tally_number==8  % *F8
            TPlan.dose.(sprintf([n{1} '_DOSE'])).resolution = result.r1.resolution;
            TPlan.dose.(sprintf([n{1} '_DOSE'])).ref        = result.r1.ref;
            TPlan.dose.(sprintf([n{1} '_DOSE'])).shift      = result.r1.shift;
            % adjust size
            % phantom size can be defined by the user and the tally index
            % refers to the origin of the phantom ... not necessarally the same
            % as the CT ... shift describes the number of voxel to skip using
            % the original image as reference.
            % 
            if TPlan.image.ImagePositionPatient == TPlan.dose.(sprintf([n{1} '_DOSE'])).ref 
               % no shift -- image was also imported from amcnp inp file so the
               % origin is the same as the tally
               TPlan.dose.(sprintf([n{1} '_DOSE'])).Dose       = result.r1.Dose;
               TPlan.dose.(sprintf([n{1} '_DOSE'])).uncert     = result.r1.Unc;
            else
               TPlan.dose.(sprintf([n{1} '_DOSE'])).Dose       = zeros(size(TPlan.image.Image)); 
               TPlan.dose.(sprintf([n{1} '_DOSE'])).uncert     = zeros(size(TPlan.image.Image));
               %
               s  = TPlan.dose.(sprintf([n{1} '_DOSE'])).shift([3,2,1]);
               sd = size(result.r1.Dose);
               %
               TPlan.dose.(sprintf([n{1} '_DOSE'])).Dose(s(1):s(1)-1+sd(1),s(2):s(2)-1+sd(2),s(3):s(3)-1+sd(3))  = result.r1.Dose;
               TPlan.dose.(sprintf([n{1} '_DOSE'])).uncert(s(1):s(1)-1+sd(1),s(2):s(2)-1+sd(2),s(3):s(3)-1+sd(3)) = result.r1.Unc;
            end
            setappdata(handles.amb_interface,'TPlan',TPlan);
        end
       if   result.(sprintf('r%.0f',i)).tally_number==24  % mean energy  
    %       % energy
            TPlan.dose.(sprintf([n{1} '_En'])).Dose                       = result.(sprintf('r%.0f',i)).Result;
            TPlan.dose.(sprintf([n{1} '_En'])).(sprintf('uncert'))        = result.(sprintf('r%.0f',i)).Uncert;
            TPlan.dose.(sprintf([n{1} '_En'])).(sprintf('resolution'))(1) = mean(diff(result.(sprintf('r%.0f',i)).y))*10;  % mm
            TPlan.dose.(sprintf([n{1} '_En'])).(sprintf('resolution'))(2) = mean(diff(result.(sprintf('r%.0f',i)).z))*10;  % mm 
            TPlan.dose.(sprintf([n{1} '_En'])).(sprintf('resolution'))(3) = mean(diff(result.(sprintf('r%.0f',i)).x))*10;  % mm  
            %
            TPlan.dose.(sprintf([n{1} '_En'])).(sprintf('ref'))(1)        = result.(sprintf('r%.0f',i)).y(1)*10;  % mm
            TPlan.dose.(sprintf([n{1} '_En'])).(sprintf('ref'))(2)        = result.(sprintf('r%.0f',i)).z(1)*10;  % mm
            TPlan.dose.(sprintf([n{1} '_En'])).(sprintf('ref'))(3)        = result.(sprintf('r%.0f',i)).x(1)*10;  % mm       
            %
            TPlan.dose.(sprintf([n{1} '_En'])).(sprintf('ref'))           = TPlan.dose.(sprintf([n{1} '_En'])).(sprintf('ref'))+TPlan.image.ImagePositionPatient;
            %
            [TPlan.dose.(sprintf([n{1} '_En'])).Dose,TPlan.dose.(sprintf([n{1} '_En'])).(sprintf('uncert'))] = ...
                              interp_mc_dose(TPlan,result.(sprintf('r%.0f',i)).Result,result.(sprintf('r%.0f',i)).Uncert,TPlan.dose.(sprintf([n{1} '_En'])).(sprintf('resolution')),TPlan.dose.(sprintf([n{1} '_En'])).(sprintf('ref')));
            %
            setappdata(handles.amb_interface,'TPlan',TPlan); 
       end
       %
       if   result.(sprintf('r%.0f',i)).tally_number==34  % mean energy
    %       % flux
            TPlan.dose.(sprintf([n{1} '_Fl'])).Dose                       = result.(sprintf('r%.0f',i)).Result;
            TPlan.dose.(sprintf([n{1} '_Fl'])).(sprintf('uncert'))        = result.(sprintf('r%.0f',i)).Uncert;
            TPlan.dose.(sprintf([n{1} '_Fl'])).(sprintf('resolution'))(1) = mean(diff(result.(sprintf('r%.0f',i)).y))*10;  % mm
            TPlan.dose.(sprintf([n{1} '_Fl'])).(sprintf('resolution'))(2) = mean(diff(result.(sprintf('r%.0f',i)).z))*10;  % mm 
            TPlan.dose.(sprintf([n{1} '_Fl'])).(sprintf('resolution'))(3) = mean(diff(result.(sprintf('r%.0f',i)).x))*10;  % mm  
            %
            TPlan.dose.(sprintf([n{1} '_Fl'])).(sprintf('ref'))(1)        = result.(sprintf('r%.0f',i)).y(1)*10;  % mm
            TPlan.dose.(sprintf([n{1} '_Fl'])).(sprintf('ref'))(2)        = result.(sprintf('r%.0f',i)).z(1)*10;  % mm
            TPlan.dose.(sprintf([n{1} '_Fl'])).(sprintf('ref'))(3)        = result.(sprintf('r%.0f',i)).x(1)*10;  % mm       
            %
            TPlan.dose.(sprintf([n{1} '_Fl'])).(sprintf('ref'))           = TPlan.dose.(sprintf([n{1} '_Fl'])).(sprintf('ref'))+TPlan.image.ImagePositionPatient;
            %
            [TPlan.dose.(sprintf([n{1} '_Fl'])).Dose,TPlan.dose.(sprintf([n{1} '_Fl'])).(sprintf('uncert'))] = ...
                              interp_mc_dose(TPlan,result.(sprintf('r%.0f',i)).Result,result.(sprintf('r%.0f',i)).Uncert,TPlan.dose.(sprintf([n{1} '_Fl'])).(sprintf('resolution')),TPlan.dose.(sprintf([n{1} '_Fl'])).(sprintf('ref')));
            %
            setappdata(handles.amb_interface,'TPlan',TPlan); 
            M_en=1;
       end
    %     %
    end
    %
    if M_en==1
        TPlan.dose.(sprintf([n{1} '_MeanEnergy']))      = TPlan.dose.(sprintf([n{1} '_Fl']));
        TPlan.dose.(sprintf([n{1} '_MeanEnergy'])).Dose = TPlan.dose.(sprintf([n{1} '_En'])).Dose./TPlan.dose.(sprintf([n{1} '_Fl'])).Dose;
        setappdata(handles.amb_interface,'TPlan',TPlan); 
    end
    %
    cla(handles.amb_dose_eval);
    handles.clinical_par_list.String=[];
    dose_menu(handles,TPlan);
    dose_menu_Callback(1,1,handles);
end


function [Dose_New,Uncert_New] = interp_mc_dose(TPlan,Dose,Uncert,Res,Ref) %#ok<INUSL>
%
% TPlan.image.Nvoxels = double(TPlan.image.Nvoxels);
% %   %
% if abs(Res(1)-TPlan.image.Resolution(1))<0.001
%     Res(1)=TPlan.image.Resolution(1);
% end
% if abs(Res(2)-TPlan.image.Resolution(2))<0.001
%     Res(2)=TPlan.image.Resolution(2);
% end
% if abs(Res(3)-TPlan.image.Resolution(3))<0.001
%     Res(3)=TPlan.image.Resolution(3);
% end
% 
% 
% Ref=Ref-TPlan.image.ImagePositionPatient; 
% [Xq,Yq,Zq]    = meshgrid(0:TPlan.image.Resolution(1):Res(1)*(size(Dose,2)-1),...
%                          0:TPlan.image.Resolution(2):Res(2)*(size(Dose,1)-1), ...
%                          0:TPlan.image.Resolution(3):Res(3)*(size(Dose,3)-1)); % image grid
%   %
% [X,Y,Z]       = meshgrid(0:Res(1):Res(1)*(size(Dose,2)-1),...
%                          0:Res(2):Res(2)*(size(Dose,1)-1), ...
%                          0:Res(3):Res(3)*(size(Dose,3)-1)); % dose grid
%      
% D = interp3(X,Y,Z,double(Dose),Xq,Yq,Zq,'linear');
% D(isnan(D))=0; 
% %
% U = interp3(X,Y,Z,double(Uncert),Xq,Yq,Zq,'Linear');
% U(isnan(D))=0; 

% if sum(round(Res*10)==round(TPlan.image.Resolution*10))~=3
%   si= round(Res./(round(TPlan.image.Resolution*10)/10).*size(Dose));
%   D = imresize(double(Dose),[si(1) si(2)]);         U = imresize(double(Uncert),[si(1) si(2)]); 
%   D = imresize(permute(D,[3 2 1]),[si(3) si(2)]);   U = imresize(permute(U,[3 2 1]),[si(3) si(2)]); 
%   D = permute(D,[3 2 1]);                           U = permute(U,[3 2 1]);
% else
% D=Dose;  U=Uncert;
% end
siz = (Res./TPlan.image.Resolution);
% % % there might be a issue in the 4th decimal place due to rounding in MCNP
siz = round(siz*1000)/1000;
% % %
% % if sum(siz)~=3
%      si = round(size(Dose).*siz);
% %     D  = imresize3(double(Dose),[si(1) si(2) si(3)],'linear');         
% %     U  = imresize3(double(Uncert),[si(1) si(2) si(3)],'linear'); 
% % else
% %     D=Dose;  U=Uncert;
% % end
% for i=1:2:size(Dose,3)
%    D(:,:,(ceil(i/2)))=mean(Dose(:,:,i:i+1),3);
% end
% 
% average in the z direction 
for i=1:1/siz(3):size(Dose,3)
    D(:,:,ceil(i*siz(3)))=mean(Dose(:,:,i:i+(1/siz(3))-1),3);
    U(:,:,ceil(i*siz(3)))=mean(Uncert(:,:,i:i+(1/siz(3))-1),3);
end
%
sh         = round((Ref-TPlan.image.ImagePositionPatient)./TPlan.image.Resolution);
Dose_New   = zeros(size(TPlan.image.Image));
Uncert_New = zeros(size(TPlan.image.Image));
di         = size(D);
%
%
Dose_New(sh(2)+1:sh(2)+di(1),sh(1)+1:sh(1)+di(2),sh(3)+1:sh(3)+di(3))  = single(D);
Uncert_New(sh(2)+1:sh(2)+di(1),sh(1)+1:sh(1)+di(2),sh(3)+1:sh(3)+di(3))= single(U*100);









function amb_save_project_Callback(~, ~, ~)













function amb_save_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
%
[File,Folder]=uiputfile('*.AMB_plan');
%
% materials
ed_all_mat  = handles.edit_all_materials.String;
mat_menu    = handles.material_pop_menu.String;
mat_limits  = handles.material_pop_menu.UserData;
phantom_pos = getappdata(handles.amb_interface,'phantom_region');
score_pos   = getappdata(handles.amb_interface,'score_region');
%
% % try
% %     TPlan.dose.MC_Dwm=single(TPlan.dose.MC_Dwm);
% %     TPlan.dose.MC_Dmm=single(TPlan.dose.MC_Dmm);
% %     TPlan.dose.MC_Dwm_uncert=single(TPlan.dose.MC_Dwm_uncert);
% %     TPlan.dose.MC_Dmm_uncert=single(TPlan.dose.MC_Dmm_uncert);
% % catch
    %
%end
%
save([Folder File],'TPlan','ed_all_mat','mat_menu','mat_limits','phantom_pos','score_pos','-v7.3');
%
msgbox('Done !');

function menu_isodose_Callback(~, ~, ~)


function set_dose_grid_CreateFcn(~, ~, ~)


function change_res_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
TPlan=change_resolution(TPlan);
if isnumeric(TPlan)==1; return; end
setappdata(handles.amb_interface,'TPlan',TPlan);
%
handles.cover_panel.Visible='on';
%
w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
set(w_bar.hand,'Visible',1,'Value',80);
%
% User most select phantom and grid size ....
handles.phantom_user.Checked   ='off';
handles.phantom_all.Checked    ='off';
handles.dosegrid_user.Checked  ='off';
handles.dosegrid_all.Checked   ='off';
%
% plot images
TPlan=getappdata(handles.amb_interface,'TPlan');
if isfield(TPlan.image,'Image')==0; return; end
plot_image_after_import(handles,TPlan);         set(w_bar.hand,'Visible',1,'Value',85);   
plot_empty_contour_layer(handles,TPlan);        set(w_bar.hand,'Visible',1,'Value',87);
plot_empty_dose_layer(handles,TPlan);           set(w_bar.hand,'Visible',1,'Value',89);
set_contour_list(TPlan,handles);                set(w_bar.hand,'Visible',1,'Value',91);
adjust_contrast_axes(handles,TPlan,1);          set(w_bar.hand,'Visible',1,'Value',93);
dose_menu(handles,TPlan);
dose_contrast_histogram(0);
%
material_menu(handles);                         set(w_bar.hand,'Visible',1,'Value',97);
ct_cal_menu(handles);
set_slice_guides;                         drawnow;
%
handles.cat_menu.String=strsplit(num2str((1:1:TPlan.plan.NCat)));
%
set(handles.cover_panel,'Visible','off'); drawnow;
set(w_bar.hand,'Visible',0,'Value',0);


function disp_mat_den_Callback(~, ~, handles)

TPlan=getappdata(handles.amb_interface,'TPlan');
if isfield(TPlan.plan,'Mat_HU')==0;  warndlg('Create a material map first'); return; end
if isfield(TPlan.plan,'Density')==0; warndlg('Create a density map first');  return; end
sli = getappdata(handles.amb_interface,'slices');
new_fig; colormap jet;
subplot(2,3,1);imagesc(TPlan.plan.Mat_HU(:,:,sli(1)));          impixelinfo;  title  Materials;
subplot(2,3,2);imagesc(squeeze(TPlan.plan.Mat_HU(:,sli(2),:))); impixelinfo;
subplot(2,3,3);imagesc(squeeze(TPlan.plan.Mat_HU(sli(3),:,:))); impixelinfo;
%
subplot(2,3,4);imagesc(TPlan.plan.Density(:,:,sli(1)));          impixelinfo;  title  Density;
subplot(2,3,5);imagesc(squeeze(TPlan.plan.Density(:,sli(2),:))); impixelinfo;
subplot(2,3,6);imagesc(squeeze(TPlan.plan.Density(sli(3),:,:))); impixelinfo;


function mat2cont_Callback(~, ~, handles)
c   =  handles.cont_pop_menu.Value;
idx =  handles.material_pop_menu.Value;
%
TPlan=getappdata(handles.amb_interface,'TPlan');
TPlan.plan.Mat_HU(TPlan.struct.contours.(sprintf('Item_%.0f',c)).Mask==1)=TPlan.plan.MatNamesID(idx);
%
warndlg({['Material  "' TPlan.plan.MatNames{idx} '"  assigned to contour   "' TPlan.struct.contours.(sprintf('Item_%.0f',c)).Name '" '], ...
              'Create a new material map will overwrite this', ...
              'Use ''Tools => Disp. Mat. Dens.'' to check the material map'});
setappdata(handles.amb_interface,'TPlan',TPlan);
update_images(4,getappdata(handles.amb_interface,'slices'));


function d_operations_Callback(~, ~, handles)
dose_operations;
uiwait(findobj('name','dose_op'));
dose_menu(handles,getappdata(handles.amb_interface,'TPlan'));
dose_menu_Callback(1,1,handles);


function imedit_fcn_menu_Callback(~, ~, handles)
%
    handles.imedit_erase.Visible='off';
    handles.imedit_undo.Visible ='off';
    handles.cont_add.Visible    ='off';
    handles.cont_rename.Visible ='off';
    handles.cont_delete.Visible ='off';
    handles.draw_on.Visible     ='off';
    handles.draw_sub.Visible    ='off';
%
if strcmp(handles.imedit_fcn_menu.String{handles.imedit_fcn_menu.Value},'Auto Contour')==1 ...
        || strcmp(handles.imedit_fcn_menu.String{handles.imedit_fcn_menu.Value},'Find body')==1
    handles.imedit_erase.Visible='on';
    handles.imedit_undo.Visible ='on';
    turn_draw_tool_off(handles);
elseif strcmp(handles.imedit_fcn_menu.String{handles.imedit_fcn_menu.Value},'Draw')==1
    handles.imedit_erase.Visible='on';
    handles.imedit_undo.Visible ='on';
    handles.draw_on.Visible     ='on';
    handles.draw_sub.Visible    ='on';
elseif strcmp(handles.imedit_fcn_menu.String{handles.imedit_fcn_menu.Value},'New/Delete Contour.')==1
    handles.cont_add.Visible    ='on'; handles.cont_add.Position    = [0.0224    0.7326    0.1716    0.0930];
    handles.cont_rename.Visible ='on'; handles.cont_rename.Position = [0.2313    0.7326    0.1716    0.0930];
    handles.cont_delete.Visible ='on'; handles.cont_delete.Position = [0.4313    0.7326    0.1716    0.0930];
%    
elseif strcmp(handles.imedit_fcn_menu.String{handles.imedit_fcn_menu.Value},'Interpolate Contour')==1
%
elseif strcmp(handles.imedit_fcn_menu.String{handles.imedit_fcn_menu.Value},'Copy contour')==1
%
else
    warndlg('Not available in this version');
    turn_draw_tool_off(handles);
end

function turn_draw_tool_off(handles)
global draw_countour
delete(findobj('Tag','draw_pointer'));
draw_countour.brush=0;
set(handles.amb_interface,'WindowButtonMotionFcn',[]);
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'draw_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.draw_on,'String',str);
handles.draw_on.Value=0;



function imedit_fcn_menu_CreateFcn(hObject, ~, ~)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function imedit_do_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
if strcmp(handles.imedit_fcn_menu.String{handles.imedit_fcn_menu.Value},'Interpolate Contour')==1
    w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
    set(w_bar.hand,'Visible',1,'Value',35);  
    interpolate_contour;
    set(w_bar.hand,'Visible',1,'Value',95);
    pause(1);
    set(w_bar.hand,'Visible',0,'Value',0);  
    TPlan=getappdata(handles.amb_interface,'TPlan');
    set_contour_list(TPlan,handles); drawnow;
elseif strcmp(handles.imedit_fcn_menu.String{handles.imedit_fcn_menu.Value},'Auto Contour')==1
   create_restore_points(handles);
   find_connections;
   update_all_contours;
elseif strcmp(handles.imedit_fcn_menu.String{handles.imedit_fcn_menu.Value},'Find body')==1
   create_restore_points(handles);
   find_body;
   update_all_contours;  
elseif strcmp(handles.imedit_fcn_menu.String{handles.imedit_fcn_menu.Value},'Copy contour')==1
   % find edit contour
   TPlan=getappdata(findobj('Tag','amb_interface'),'TPlan');
   for i=1:length(fieldnames(TPlan.struct.contours))
     if strcmp(TPlan.struct.contours.(sprintf('Item_%.0f',i)).Name,'Edit')==1
        TPlan.struct.contours.(sprintf('Item_%.0f',i)).Mask = ...
                   TPlan.struct.contours.(sprintf('Item_%.0f',handles.cont_pop_menu.Value)).Mask;
        %
        setappdata(handles.amb_interface,'TPlan',TPlan);
        break;
     end 
   end
   %
   update_all_contours;   
end

function create_restore_points(handles)
% create a restore point
TPlan=getappdata(handles.amb_interface,'TPlan');
for i=1:length(fieldnames(TPlan.struct.contours))
 if strcmp(TPlan.struct.contours.(sprintf('Item_%.0f',i)).Name,'Edit')==1
    TPlan.struct.Restore=TPlan.struct.contours.(sprintf('Item_%.0f',i)).Mask;
    break;
 end
end
setappdata(handles.amb_interface,'TPlan',TPlan);

function imedit_undo_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
id  = handles.cont_pop_menu.Value;
TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask=TPlan.struct.Restore;
setappdata(handles.amb_interface,'TPlan',TPlan);
update_all_contours;



function imedit_erase_Callback(~, ~, handles)
  TPlan  =  getappdata(handles.amb_interface,'TPlan'); 
  id  = handles.cont_pop_menu.Value;
  TPlan.struct.Restore=TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask;
  TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask=logical(zeros(TPlan.image.Nvoxels));
  %
  setappdata(handles.amb_interface,'TPlan',TPlan);
  update_all_contours;


function amb_dos_01_ButtonDownFcn(~, ~, ~)
global draw_countour
draw_countour.on=1;

function draw_on(varargin)
global draw_countour
draw_countour.on=1;
handles = guihandles;
s=round(draw_countour.size/2);
%
l(1,1:2)=xlim(handles.amb_dos_01);
l(2,1:2)=ylim(handles.amb_dos_01);
TPlan= getappdata(handles.amb_interface,'TPlan');
sli  = getappdata(handles.amb_interface,'slices');
C    = round(get(handles.amb_dos_01,'CurrentPoint'));
%
id  = handles.cont_pop_menu.Value;
% for id=1:length(fieldnames(TPlan.struct.contours))
%    if strcmp(TPlan.struct.contours.(sprintf('Item_%.0f',id)).Name,'Edit')==1
%       break;
%    end 
% end
% %
if C(1,2)-s<l(1,1); C(1,2)=l(1,1)+s; end 
if C(1,2)+s>l(1,2); C(1,2)=l(1,2)-s; end 
if C(1,1)-s<l(2,1); C(1,1)=l(2,1)+s; end 
if C(1,1)+s>l(2,2); C(1,1)=l(2,2)-s; end 
%
TPlan.struct.Restore = TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask;
ref                  = double(TPlan.image.Image(C(1,2),C(1,1),sli(1)));
r                    = getappdata(handles.amb_interface,'brush_range')/100;
range                = [ref-abs(ref*r) ref+abs(ref*r)];
Im                   = TPlan.image.Image(C(1,2)-s:C(1,2)+s,C(1)-s:C(1)+s,sli(1));
%
Im(Im<=range(1) | Im>=range(2))=0; 
%
Im=logical(Im);
if draw_countour.subtract==0
     TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask(C(1,2)-s:C(1,2)+s,C(1)-s:C(1)+s,sli(1))=TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask(C(1,2)-s:C(1,2)+s,C(1)-s:C(1)+s,sli(1)) ...
                                              + Im;
else
          a=TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask(C(1,2)-s:C(1,2)+s,C(1)-s:C(1)+s,sli(1))-Im;
          a(a<0)=0; a=logical(a);
          TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask(C(1,2)-s:C(1,2)+s,C(1)-s:C(1)+s,sli(1))=a;
end
drawnow;
setappdata(handles.amb_interface,'TPlan',TPlan); 
update_contours(4,TPlan,sli,getappdata(handles.amb_interface,'ax'),0.5);


%

function draw_on_Callback(~, ~, handles)
global draw_countour
if handles.draw_on.Value==1
    answer=inputdlg({'HU Range (%)', 'Brush size'},'Draw',1,{'20','20'});
    setappdata(handles.amb_interface,'brush_range',str2num(answer{1}));
    draw_countour.size =str2num(answer{2});
    draw_countour.brush=1;
    %
    lim(1,1:2)=xlim(handles.amb_dos_01);
    lim(2,1:2)=ylim(handles.amb_dos_01);
    %
    axes(handles.amb_dos_01);
    s=draw_countour.size;
    delete(findobj('Tag','draw_pointer'));
    x = [lim(1,1) lim(1,1)+s lim(1,1)+s lim(1,1)];        y = [lim(2,1) lim(2,1)  lim(2,1)+s lim(2,1)+s]; 
    h(1)=patch(x,y,'r');     set(h(1),'FaceColor','b','FaceAlpha',0.1,'EdgeColor','b','Tag','draw_pointer','ButtonDownFcn',@draw_on);
    %
    x = [lim(1,1) lim(1,1)+2 lim(1,1)+2 lim(1,1)];        y = [lim(2,1) lim(2,1)  lim(2,1)+2 lim(2,1)+2]; 
    h(2)=patch(x,y,'r');     set(h(2),'FaceColor','r','FaceAlpha',1,'EdgeColor','r','Tag','draw_pointer','ButtonDownFcn',@draw_on);
    %
    %
    % find edit ID
    %
    draw_countour.id= handles.cont_pop_menu.Value;
    %
    set(handles.amb_interface,'WindowButtonMotionFcn', {@brush_amb,h(:),handles.amb_dos_01,handles.amb_interface});
    %
    iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'draw_on.png'];   iconFilename = strrep(iconFilename, '\', '/');
    str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
    set(handles.draw_on,'String',str);
%
%
else
    delete(findobj('Tag','draw_pointer'));
    draw_countour.brush=0;
    set(handles.amb_interface,'WindowButtonMotionFcn',[]);
    iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'draw_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
    str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
    set(handles.draw_on,'String',str);
end
setappdata(handles.amb_interface,'draw_on',handles.draw_on.Value);


function draw_sub_Callback(hObject, ~, handles)
global draw_countour
if hObject.Value==1
    draw_countour.subtract=1;
    iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'draw_eraser.png'];   iconFilename = strrep(iconFilename, '\', '/');
else
    draw_countour.subtract=0;
    iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'draw_paint.png'];  iconFilename = strrep(iconFilename, '\', '/');
end
    str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
    set(handles.draw_sub,'String',str);


function cont_add_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
%
answer=inputdlg('Name:','Add',1,{'New_contour'});
%
if isempty(answer)==1; return; end
%
name=answer{1}(logical(~isspace(answer{1}))); 
if isfield(TPlan.struct,'contours')==0
    id = 1;
else
    id  =length(fieldnames(TPlan.struct.contours))+1;
end
%
name=answer{1}(logical(~isspace(answer{1})));  
%
TPlan.struct.contours.(sprintf('Item_%.0f',id)).Name = name;
TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask = logical(zeros(TPlan.image.Nvoxels));
%
setappdata(handles.amb_interface,'TPlan',TPlan);
set_contour_list(TPlan,handles); drawnow;


function cont_rename_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
%
answer=inputdlg('Name:','Rename',1,{'New_name'});
%
if isempty(answer)==1; return; end
%
name=answer{1}(logical(~isspace(answer{1})));  
%
id  = handles.cont_pop_menu.Value;
%
if  strcmpi(TPlan.struct.contours.(sprintf('Item_%.0f',id)).Name,'Edit')==1
   warndlg('You renamed this contour!');   return;
end
%
TPlan.struct.contours.(sprintf('Item_%.0f',id)).Name = name;
setappdata(handles.amb_interface,'TPlan',TPlan);
set_contour_list(TPlan,handles); drawnow;




function cont_delete_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
%
choice = questdlg('Are you sure?', ...
	'Delete', ...
	'Yes','No','No');
% Handle response
switch choice
    case 'No'
        return;
end
% 
id(1)  = handles.cont_pop_menu.Value;
id(2)  = length(fieldnames(TPlan.struct.contours));
%
if  strcmpi(TPlan.struct.contours.(sprintf('Item_%.0f',id(1))).Name,'Edit')==1
   warndlg('You cannot delete this contour !');   return;
end
%
if id(1)<id(2)
  for i=id(1):1:id(2)-1
     TPlan.struct.contours.(sprintf('Item_%.0f',i)) = TPlan.struct.contours.(sprintf('Item_%.0f',i+1));
  end
end
TPlan.struct.contours=rmfield(TPlan.struct.contours,['Item_' num2str(id(2))]); 
setappdata(handles.amb_interface,'TPlan',TPlan);
set_contour_list(TPlan,handles); drawnow;


function clinical_par_list_Callback(~, ~, ~)


function clinical_par_list_CreateFcn(hObject, ~, ~)

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cal_parameters_Callback(~, ~, handles)
clinical_parameters;



function dose_par_edit_Callback(~, ~, handles)
% Construct a questdlg with three options
choice = questdlg('Set your preferences', ...
	'Plot', ...
	'All','Single line','Cancel','Cancel');
% Handle response
h=get(handles.amb_dose_eval,'Child');
switch choice
    case 'All'
       prompt = {'Line Width:','Line Style (- -  :  -) '};
       dlg_title = 'Input';
       num_lines = 1;
       defaultans = {'2','--'};
       answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
       set(h,'LineWidth',str2num(answer{1}));
       set(h,'LineStyle',answer{2});
    case 'Single line'
        names=[];
        for i=1:size(h,1)
            names{end+1}=h(i).DisplayName;
        end
        %
        [s,~] = listdlg('PromptString','Select a line to edit:',...
                        'SelectionMode','single',...
                        'ListString',names);
        %
        color=uisetcolor;
        prompt = {'Line Width:','Line Style (- -  :  -) '};
        dlg_title = 'Input';
        num_lines = 1;
        defaultans = {'2','--'};
        answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
        set(h(s),'LineWidth',str2num(answer{1}));
        set(h(s),'LineStyle',answer{2});
        set(h(s),'Color',color); 
end


function pushbutton30_Callback(~, ~, handles)
prompt = {'X-Lim:','Y - Lim'};
dlg_title = 'Axes';
num_lines = 1;
defaultans = {num2str(handles.amb_dose_eval.XLim),num2str(handles.amb_dose_eval.YLim)};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
if size(answer,2)==0
    return;
end
set(handles.amb_dose_eval,'XLim',str2num(answer{1}),'YLim',str2num(answer{2}));



% --- Executes on selection change in EB_listbox_01.
function EB_listbox_01_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
%
if isfield(TPlan.plan,'modality')==1 && strcmp(TPlan.plan.modality,'Protons')
    %
    proton_geometry;
    if isfield(TPlan.plan,'isocenter')==0
        TPlan.plan.isocenter = [0;0;0];
    end
    %
    if isfield(TPlan.plan,'C_angle')==0
        TPlan.plan.C_angle   = 0;
    end
    setappdata(handles.amb_interface,'TPlan',TPlan);
    %
    B=str2double(handles.EB_listbox_01.String{handles.EB_listbox_01.Value}(6:end));
    %
    Snout_D               = TPlan.plan.(sprintf('Beam_%d',B)).Topas_spot(1,4)/10;
    G_angle               = TPlan.plan.(sprintf('Beam_%d',B)).Topas_spot(1,26);
    C_angle               = 90+TPlan.plan.(sprintf('Beam_%d',B)).Topas_spot(1,27);
    Shift                 = TPlan.plan.(sprintf('Beam_%d',B)).Isocenter - TPlan.plan.isocenter;
    TPlan.plan.isocenter  = TPlan.plan.(sprintf('Beam_%d',B)).Isocenter;
    %
    %
    if isfield(TPlan.plan.pat_model,'patch')==1 && isempty(TPlan.plan.pat_model.patch)==0
        TPlan.plan.pat_model.patch.Vertices(:,1)=TPlan.plan.pat_model.patch.Vertices(:,1)+Shift(1)/10;
        TPlan.plan.pat_model.patch.Vertices(:,2)=TPlan.plan.pat_model.patch.Vertices(:,2)-Shift(3)/10;
        TPlan.plan.pat_model.patch.Vertices(:,3)=TPlan.plan.pat_model.patch.Vertices(:,3)+Shift(2)/10;
        rotate(TPlan.plan.pat_model.patch,[0 0 1],C_angle-TPlan.plan.C_angle,[0 0 0]);
    end
%     %
     
    TPlan.plan.C_angle = C_angle;         
    % CT volume
    TPlan.plan.CT_Dim.Vertices(:,1)=TPlan.plan.CT_Dim.Vertices(:,1)+TPlan.plan.(sprintf('Beam_%d',B)).Isocenter(1)/10;
    TPlan.plan.CT_Dim.Vertices(:,2)=TPlan.plan.CT_Dim.Vertices(:,2)-TPlan.plan.(sprintf('Beam_%d',B)).Isocenter(3)/10;
    TPlan.plan.CT_Dim.Vertices(:,3)=TPlan.plan.CT_Dim.Vertices(:,3)+TPlan.plan.(sprintf('Beam_%d',B)).Isocenter(2)/10;
    %
    rotate(TPlan.plan.CT_Dim,[0 0 1],C_angle,[0 0 0]);
    % Nozzle movement 
    for i=1:14
      TPlan.plan.MLC(i).Vertices(:,3) = TPlan.plan.MLC(i).Vertices(:,3)+(33.6-Snout_D);
      rotate(TPlan.plan.MLC(i),[1 0 0],-G_angle,[0 0 0]);
    end
    TPlan.plan.Ext.Vertices(:,3) = TPlan.plan.Ext.Vertices(:,3)+(33.6-Snout_D);
    rotate(TPlan.plan.Ext,[1 0 0],-G_angle,[0 0 0]);
    for i=1:18
      TPlan.plan.RS(i).Vertices(:,3)  = TPlan.plan.RS(i).Vertices(:,3) +(33.6-Snout_D);
      rotate(TPlan.plan.RS(i),[1 0 0],-G_angle,[0 0 0]);
    end
    %
    %
    %
    setappdata(handles.amb_interface,'TPlan',TPlan);
elseif isfield(TPlan.plan,'BEAM_info')==1
    if strcmp(handles.EB_listbox_01.String{handles.EB_listbox_01.Value}(1:4),'Beam')==1
        return;
    end
    %
    cp=str2num(handles.EB_listbox_01.String{handles.EB_listbox_01.Value}(end-3:end));
    % find beam
    B=str2num(handles.EB_listbox_01.String{handles.EB_listbox_01.Value-cp}(5:end));
    %
    colimator_mlc(TPlan);
    %
    TPlan=getappdata(handles.amb_interface,'TPlan');
    show_field(TPlan.plan.MLC,TPlan.plan.Jaw,TPlan.plan.(sprintf('Beam_%d',B)).(sprintf('CP_%d',cp)),TPlan.plan.field);
    % 
    pos_adjust    =  TPlan.plan.(sprintf('Beam_%d',B)).(sprintf('CP_%d',cp)).Isocenter([1,3,2])/10;
    pos_adjust(3) = -pos_adjust(3);
    % keep the current isocenter update to avoid repeating the correction
    TPlan.plan.isocenter = TPlan.plan.(sprintf('Beam_%d',B)).(sprintf('CP_%d',cp)).Isocenter;
    %
    TPlan.plan.pat_model.patch.Vertices=bsxfun(@minus,TPlan.plan.pat_model.vertex,pos_adjust);
    %
    a=TPlan.plan.(sprintf('Beam_%d',B)).(sprintf('CP_%d',cp));
    handles.EB_text_info.String=['Energy (MeV) ' num2str(a.NominalBeamEnergy) '  Dose rate (MU) ' num2str(a.DoseRateSet) '  Gantry Angle ' num2str(a.GantryAngle)]; 
        %
elseif isfield(TPlan.IrIS,'Projections')==1
    % delete previous tracks
    delete(getappdata(handles.amb_interface,'proj'));
    %
    Projections = TPlan.IrIS.Projections.(sprintf(handles.EB_listbox_01.String{handles.EB_listbox_01.Value}));
   % Projections(Projections(:,13)==-1,:)=[]; hold on;
    %
    for i=1:size(Projections,1)
        
        p(i) = plot3([Projections(i,1) Projections(i,7)], ...
                     [Projections(i,5) Projections(i,9)], ...
                    -[Projections(i,3) Projections(i,8)],'b-.','LineWidth',1.0);        
    end
    setappdata(handles.amb_interface,'proj',p);
end
%
% if isfield(TPlan.plan.pat_model,'patch')==1 && isempty(TPlan.plan.pat_model.patch)==0
%     set(handles.EB_vis_01,'XLim',[min([-120,min(TPlan.plan.pat_model.patch.Vertices(:,1))]) max([120,max(TPlan.plan.pat_model.patch.Vertices(:,1))])]);
%     set(handles.EB_vis_01,'YLim',[min([-120,min(TPlan.plan.pat_model.patch.Vertices(:,2))]) max([120,max(TPlan.plan.pat_model.patch.Vertices(:,2))])]);
%     set(handles.EB_vis_01,'ZLim',[min([-120,min(TPlan.plan.pat_model.patch.Vertices(:,3))]) max([120,max(TPlan.plan.pat_model.patch.Vertices(:,3))])]);
% end
%


% --- Executes during object creation, after setting all properties.
function EB_listbox_01_CreateFcn(hObject,~, ~)
% hObject    handle to EB_listbox_01 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function rot3d_eb_OffCallback(~, ~, handles)
rotate3d(handles.EB_vis_01,'off');


% --------------------------------------------------------------------
function rot3d_eb_OnCallback(~, ~, handles)
rotate3d(handles.EB_vis_01,'on');


% --------------------------------------------------------------------
function menu_EB_Callback(hObject, eventdata, handles)
% hObject    handle to menu_EB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function EB_show_isocenter_Callback(~, ~, handles)
% hObject    handle to EB_show_isocenter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function EB_view_options_Callback(~, ~, handles)
% hObject    handle to EB_view_options (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% display X axes
function EB_view_x_Callback(hObject, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
if isempty(hObject.UserData)==1 || hObject.UserData==1
    TPlan.plan.pat_model.X_line.Color='none';
    hObject.UserData = 0;
else
    TPlan.plan.pat_model.X_line.Color='r';
    hObject.UserData = 1;
end
setappdata(handles.amb_interface,'TPlan',TPlan);
drawnow;


% --- Executes on button press in EB_view_y.
function EB_view_y_Callback(hObject, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
if isempty(hObject.UserData)==1 || hObject.UserData==1
    TPlan.plan.pat_model.Y_line.Color='none';
    hObject.UserData = 0;
else
    TPlan.plan.pat_model.Y_line.Color='g';
    hObject.UserData = 1;
end
setappdata(handles.amb_interface,'TPlan',TPlan);
drawnow;

% --- Executes on button press in EB_view_z.
function EB_view_z_Callback(hObject, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
if isempty(hObject.UserData)==1 || hObject.UserData==1
    TPlan.plan.pat_model.Z_line.Color='none';
    hObject.UserData = 0;
else
    TPlan.plan.pat_model.Z_line.Color='b';
    hObject.UserData = 1;
end
setappdata(handles.amb_interface,'TPlan',TPlan);
drawnow;

% --- Executes on button press in EB_view_JAWx.
function EB_view_JAWx_Callback(hObject, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
if isempty(hObject.UserData)==1 || hObject.UserData==1
    TPlan.plan.Jaw(1).Visible = 'off';
    TPlan.plan.Jaw(3).Visible = 'off';
    hObject.UserData = 0;
else
    TPlan.plan.Jaw(1).Visible = 'on';
    TPlan.plan.Jaw(3).Visible = 'on';
    hObject.UserData = 1;
end
setappdata(handles.amb_interface,'TPlan',TPlan);
drawnow;


% --- Executes on button press in EB_view_JAWy.
function EB_view_JAWy_Callback(hObject, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
if isempty(hObject.UserData)==1 || hObject.UserData==1
    TPlan.plan.Jaw(2).Visible = 'off';
    TPlan.plan.Jaw(4).Visible = 'off';
    hObject.UserData = 0;
else
    TPlan.plan.Jaw(2).Visible = 'on';
    TPlan.plan.Jaw(4).Visible = 'on';
    hObject.UserData = 1;
end
setappdata(handles.amb_interface,'TPlan',TPlan);
drawnow;


% --- Executes on button press in EB_view_MLC.
function EB_view_MLC_Callback(hObject, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
if isempty(hObject.UserData)==1 || hObject.UserData==1
   for i=1:size(TPlan.plan.MLC,1)
    TPlan.plan.MLC(i,1).Visible = 'off';
    TPlan.plan.MLC(i,2).Visible = 'off';
   end
   hObject.UserData = 0;
else
    for i=1:size(TPlan.plan.MLC,1)
      TPlan.plan.MLC(i,1).Visible = 'on';
      TPlan.plan.MLC(i,2).Visible = 'on';
    end
    hObject.UserData = 1;
end
setappdata(handles.amb_interface,'TPlan',TPlan);
drawnow;


% --- Executes on button press in EB_view_field.
function EB_view_field_Callback(hObject, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
if isempty(hObject.UserData)==1 || hObject.UserData==1
    TPlan.plan.field.Visible = 'off';
    hObject.UserData = 0;
else
    TPlan.plan.field.Visible = 'on';
    hObject.UserData = 1;
end
setappdata(handles.amb_interface,'TPlan',TPlan);
drawnow;

% --- Executes on button press in EB_view_settings.
function EB_view_settings_Callback(~, ~, handles)
%
TPlan=getappdata(handles.amb_interface,'TPlan');
%
prompt     = {'X - LIM (cm):','Y - LIM (cm)','Z - LIM (cm)','Face Alpha','Edge Alpha'};
dlg_title  = 'Input';
num_lines  = 1;
defaultans = {num2str(handles.EB_vis_01.XLim), ...
              num2str(handles.EB_vis_01.YLim), ...
              num2str(handles.EB_vis_01.ZLim), ...
              num2str(TPlan.plan.pat_model.patch.FaceAlpha), ...
              num2str(TPlan.plan.pat_model.patch.EdgeAlpha)};
answer     = inputdlg(prompt,dlg_title,num_lines,defaultans);
%
if isempty(answer) == 1
    return;
end
%
handles.EB_vis_01.XLim = str2num(answer{1});
handles.EB_vis_01.YLim = str2num(answer{2});
handles.EB_vis_01.ZLim = str2num(answer{3});
%
TPlan.plan.pat_model.patch.FaceAlpha = str2num(answer{4});
TPlan.plan.pat_model.patch.EdgeAlpha = str2num(answer{5});
%



% --- Executes on button press in EB_view_cla.
function EB_view_cla_Callback(~, ~, handles)
cla(handles.EB_vis_01);
TPlan=getappdata(handles.amb_interface,'TPlan');
if isfield(TPlan.plan,'pat_model')==1
  TPlan.plan.pat_model.patch=[];
end
TPlan.plan.isocenter = [0;0;0];
setappdata(handles.amb_interface,'TPlan',TPlan);



% --- Executes on button press in EB_field_info.
function EB_field_info_Callback(hObject, eventdata, handles)
% hObject    handle to EB_field_info (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in EB_show.
function EB_show_Callback(hObject, eventdata, handles)
% hObject    handle to EB_show (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% Import DECT  image
function import_DECT_Callback(hObject, eventdata, handles)
TPlan = getappdata(handles.amb_interface,'TPlan');
%
% check if 
if isempty(TPlan.image)==1
    warndlg('Open a Treatment Plan first');
    return; 
end
%
TPlan = import_DECT(TPlan);
%
setappdata(handles.amb_interface,'TPlan',TPlan);
%
% Make DECT panel visible
handles.DECT_panel.Visible  = 'on';
handles.DECT_panel.Position = [0.001    0.50    0.175    0.08];
%


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
%
TPlan = getappdata(handles.amb_interface,'TPlan');
%
TPlan.image.Image = int16(double(TPlan.DECT.Image_01)*(1-hObject.Value) + double(TPlan.DECT.Image_02)*(hObject.Value));
setappdata(handles.amb_interface,'TPlan',TPlan);
update_images(4,getappdata(handles.amb_interface,'slices'));
%



% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --------------------------------------------------------------------
function section_ImView_OffCallback(hObject, eventdata, handles)
handles.DECT_panel.Visible = 'off';


% --------------------------------------------------------------------
function exp_points_Callback(hObject, eventdata, handles)
c   =  handles.cont_pop_menu.Value;
idx =  handles.material_pop_menu.Value;
%

%
TPlan=getappdata(handles.amb_interface,'TPlan');
C_points = TPlan.struct.contours.(sprintf('Item_%.0f',c)).Points;
C_points(C_points(:,1)==9999,:)=[]; 
%
[File,fol] = uiputfile('*.ply');
%
ptCloud = pointCloud(C_points);
pcwrite(ptCloud,[fol File],'Encoding','ascii');
%
[t,tnorm]=MyRobustCrust(C_points);
%

% profile report;profile off;
%% plot the points cloud
figure;
set(gcf,'position',[0,0,1280,800]);
subplot(1,2,1)
hold on
axis equal
title('Points Cloud','fontsize',14)
plot3(C_points(:,1),C_points(:,2),C_points(:,3),'g.')
view(3);
axis vis3d
%% plot of the output triangulation
subplot(1,2,2)
hold on
title('Output Triangulation','fontsize',14)
axis equal
trisurf(t,C_points(:,1),C_points(:,2),C_points(:,3),'facecolor','c','edgecolor','b')%plot della superficie trattata
view(3);
axis vis3d
%
fv.vertices=C_points;
fv.faces   =t;
stlwrite([fol File(1:end-3) 'stl'],fv);
%


% --------------------------------------------------------------------
function import_struct_Callback(~, ~, ~)
read_additional_struct_dcm;
%


% --------------------------------------------------------------------
function menu_view_image_Callback(hObject, eventdata, handles)
% hObject    handle to menu_view_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function view_DCM_Callback(hObject, eventdata, handles)
if strcmp(hObject.Checked,'off')==1
    
    if   strcmp(handles.view_dens_map.Checked,'on')==1 % save contrast settings
            % 
            a=findobj('Tag','Contrast_02'); Cont_Den.C2=a(2).Position;
            a=findobj('Tag','Contrast_01'); Cont_Den.C1=a(2).Position;
            %
            setappdata(handles.amb_interface,'Cont_Den',Cont_Den);
    elseif strcmp(handles.view_mat_map.Checked,'on')==1 % save contrast settings
            % 
            a=findobj('Tag','Contrast_02'); Cont_Mat.C2=a(2).Position;
            a=findobj('Tag','Contrast_01'); Cont_Mat.C1=a(2).Position;
            %
            setappdata(handles.amb_interface,'Cont_Mat',Cont_Mat);
    end
    
    
    
    handles.view_DCM.Checked      ='on';
    handles.view_mat_map.Checked  ='off';
    handles.view_dens_map.Checked ='off';
    %
    update_images(4,double(getappdata(handles.amb_interface,'slices')));
    %
    TPlan=getappdata(handles.amb_interface,'TPlan');
    adjust_contrast_axes(handles,TPlan,1); set_slice_guides; drawnow;    
    %
    Cont_DCM=getappdata(handles.amb_interface,'Cont_DCM');
    a=findobj('Tag','Contrast_02'); 
       c_max         =  Cont_DCM.C2(1)+Cont_DCM.C2(3)/2;
       a(2).Position =  Cont_DCM.C2;
       a(1).XData    =  [c_max c_max];
    a=findobj('Tag','Contrast_01');
       c_min         =  Cont_DCM.C1(1)+Cont_DCM.C1(3)/2;
       a(2).Position =  Cont_DCM.C1;
       a(1).XData    =  [c_min c_min];
    %
    ax      = getappdata(handles.amb_interface,'ax');
    handles.amb_vis_01.CLim=([c_min-ax.contrast.shift c_max-ax.contrast.shift]);
    handles.amb_vis_02.CLim=([c_min-ax.contrast.shift c_max-ax.contrast.shift]);
    handles.amb_vis_03.CLim=([c_min-ax.contrast.shift c_max-ax.contrast.shift]);
    set(handles.amb_vis_contrast2,'CLim',[c_min c_max]);
    %
    %
end


% --------------------------------------------------------------------
function view_mat_map_Callback(hObject, ~, handles)
if strcmp(hObject.Checked,'off')==1
    
    TPlan=getappdata(handles.amb_interface,'TPlan');
    if isfield(TPlan.plan,'Mat_HU')==0
        errordlg('Create a material map first');
        return;
    end
    
    if   strcmp(handles.view_DCM.Checked,'on')==1 % save contrast settings
            % 
            a=findobj('Tag','Contrast_02'); Cont_DCM.C2=a(2).Position;
            a=findobj('Tag','Contrast_01'); Cont_DCM.C1=a(2).Position;
            %
            setappdata(handles.amb_interface,'Cont_DCM',Cont_DCM);
    elseif strcmp(handles.view_dens_map.Checked,'on')==1 % save contrast settings
            % 
            a=findobj('Tag','Contrast_02'); Cont_Den.C2=a(2).Position;
            a=findobj('Tag','Contrast_01'); Cont_Den.C1=a(2).Position;
            %
            setappdata(handles.amb_interface,'Cont_Den',Cont_Den);
    end
    
    
    handles.view_DCM.Checked      ='off';
    handles.view_mat_map.Checked  ='on';
    handles.view_dens_map.Checked ='off';
    update_images(4,double(getappdata(handles.amb_interface,'slices')));
    adjust_contrast_axes(handles,TPlan,1);
    set_slice_guides; drawnow;
    
    
    if isempty(getappdata(handles.amb_interface,'Cont_Mat'))==1
       handles.amb_vis_01.CLim=([min(TPlan.plan.Mat_HU(:))-1 max(TPlan.plan.Mat_HU(:))+1]);
       handles.amb_vis_02.CLim=([min(TPlan.plan.Mat_HU(:))-1 max(TPlan.plan.Mat_HU(:))+1]);
       handles.amb_vis_03.CLim=([min(TPlan.plan.Mat_HU(:))-1 max(TPlan.plan.Mat_HU(:))+1]);
    else
        Cont_Mat=getappdata(handles.amb_interface,'Cont_Mat');
        a=findobj('Tag','Contrast_02'); 
           c_max         =  Cont_Mat.C2(1)+Cont_Mat.C2(3)/2;
           a(2).Position =  Cont_Mat.C2;
           a(1).XData    =  [c_max c_max];
        a=findobj('Tag','Contrast_01');
           c_min         =  Cont_Mat.C1(1)+Cont_Mat.C1(3)/2;
           a(2).Position =  Cont_Mat.C1;
           a(1).XData    =  [c_min c_min];
        %
        ax      = getappdata(handles.amb_interface,'ax');
        handles.amb_vis_01.CLim=([c_min-ax.contrast.shift c_max-ax.contrast.shift]);
        handles.amb_vis_02.CLim=([c_min-ax.contrast.shift c_max-ax.contrast.shift]);
        handles.amb_vis_03.CLim=([c_min-ax.contrast.shift c_max-ax.contrast.shift]);
        set(handles.amb_vis_contrast2,'CLim',[c_min c_max]); 
    end
    
    
end


% --------------------------------------------------------------------
function view_dens_map_Callback(hObject, ~, handles)
if strcmp(hObject.Checked,'off')==1
    TPlan=getappdata(handles.amb_interface,'TPlan');
    if isfield(TPlan.plan,'Density')==0
        errordlg('Create a density map first');
        return;
    end   
    %
    if   strcmp(handles.view_DCM.Checked,'on')==1 % save contrast settings
            % 
            a=findobj('Tag','Contrast_02'); Cont_DCM.C2=a(2).Position;
            a=findobj('Tag','Contrast_01'); Cont_DCM.C1=a(2).Position;
            %
            setappdata(handles.amb_interface,'Cont_DCM',Cont_DCM);
    elseif strcmp(handles.view_mat_map,'on')==1 % save contrast settings
            % 
            a=findobj('Tag','Contrast_02'); Cont_Mat.C2=a(2).Position;
            a=findobj('Tag','Contrast_01'); Cont_Mat.C1=a(2).Position;
            %
            setappdata(handles.amb_interface,'Cont_Mat',Cont_Mat);
    end
    
    
    
    handles.view_DCM.Checked      ='off';
    handles.view_mat_map.Checked  ='off';
    handles.view_dens_map.Checked ='on';
    update_images(4,double(getappdata(handles.amb_interface,'slices')));
    adjust_contrast_axes(handles,TPlan,1);
    set_slice_guides; drawnow;
    
    if isempty(getappdata(handles.amb_interface,'Cont_Den'))==1
       handles.amb_vis_01.CLim=([min(TPlan.plan.Density(:)) max(TPlan.plan.Density(:))]);
       handles.amb_vis_02.CLim=([min(TPlan.plan.Density(:)) max(TPlan.plan.Density(:))]);
       handles.amb_vis_03.CLim=([min(TPlan.plan.Density(:)) max(TPlan.plan.Density(:))]);
    else
        Cont_Den=getappdata(handles.amb_interface,'Cont_Den');
        a=findobj('Tag','Contrast_02'); 
           c_max         =  Cont_Den.C2(1)+Cont_Den.C2(3)/2;
           a(2).Position =  Cont_Den.C2;
           a(1).XData    =  [c_max c_max];
        a=findobj('Tag','Contrast_01');
           c_min         =  Cont_Den.C1(1)+Cont_Den.C1(3)/2;
           a(2).Position =  Cont_Den.C1;
           a(1).XData    =  [c_min c_min];
        %
        ax      = getappdata(handles.amb_interface,'ax');
        handles.amb_vis_01.CLim=([c_min-ax.contrast.shift c_max-ax.contrast.shift]);
        handles.amb_vis_02.CLim=([c_min-ax.contrast.shift c_max-ax.contrast.shift]);
        handles.amb_vis_03.CLim=([c_min-ax.contrast.shift c_max-ax.contrast.shift]);
        set(handles.amb_vis_contrast2,'CLim',[c_min c_max]); 
    end
    
end


% --------------------------------------------------------------------
function export_dose_dcm_Callback(hObject, eventdata, handles)
export_dcm;


% --------------------------------------------------------------------
function start_topas_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
% get field names looking for Beams 
f    = fieldnames(TPlan.plan);
b    = contains(f,'Beam_');
for i=1:length(f)
  if b(i)==1  
     Plan.(sprintf('%s',f{i})) = TPlan.plan.(sprintf('%s',f{i}));
  end
end
%
%
answer = questdlg('Combine pulses into spots (simulations will run faster)?', ...
	'Pulses', ...
	'Yes','No','No');
% Handle response
switch answer
    case 'Yes'
        Plan = combine_pulses_simulation(Plan);
end
%
% Plan.Beam_1.Info{:,29:42}=Plan.Beam_1.Info{:,29:42}*1.05;
% define so parameters and then ask the user to confirm
%
% % Main folder  -  condor 
if ispc==1 % windows
    folder='\\172.18.60.122\cw\cw2\TP\';
else       % linux
    folder='/mnt/cw2/TP/';
end
%
% creates a sequence of T folders
% prepare condor jobs 
fol=dir([folder 't*']);
fol=fol(logical([fol.isdir])); % remove files from list
fol={fol.name};
%
if isempty(fol)==1
    n=1;
else
    % sort files
    l=[];
    for i=1:size(fol,2)
        [~,first,last] = regexp(fol{i},'\d+','match','start','end');
        l{str2double(fol{i}(first(end):last(end)))}=fol{i};
    end
    l=l(~cellfun('isempty',l));
    n=str2double(l{end}(2:end))+1;
end
% format it will use in the condor submit file
directorySimInCluster = ['/mnt/cw2/TP/t' num2str(n) '/'];
pathsimfolder         = [folder 't' num2str(n) '\'];
% Gets the right path of the the Scoring.txt to put in Plan.txt
CTDirectory = strcat(directorySimInCluster,'CT');
% CTDirectory = '/mnt/cw2/TP/t7/CT';
% define nr of cores (it is recommended to use 8)
nrCpus    = 8;
% total of protons
n_protons = str2double(handles.sim_nps.Text);
% splited into 
n_jobs    = 20;
%
% Initial and  final voxels - Works better with AMIGO
if strcmp(handles.phantom_user.Checked,'on')==0
    Vxyz       = [];
else
    Vxyz       = TPlan.plan.phantom;
end

prompt     = {'CPUS:','directorySimInCluster','pathsimfolder','CTDirectory','n_protons','n_jobs'};
dlg_title  = 'Settings';
defaultans = {num2str(nrCpus),directorySimInCluster,pathsimfolder,CTDirectory,num2str(n_protons),num2str(n_jobs)};
answer     = inputdlg(prompt,dlg_title,[1 20;1 60;1 60;1 60;1 60;1 60],defaultans);

if size(answer,2)==0
    return;
else
    nrCpus                  = str2num(answer{1});
    directorySimInCluster   = answer{2};
    pathsimfolder           = answer{3};
    CTDirectory             = answer{4};
    n_protons               = str2num(answer{5});
    n_jobs                  = str2num(answer{6});
end
makeSimulation(Plan,pathsimfolder,directorySimInCluster,nrCpus,n_protons,n_jobs,CTDirectory,Vxyz);
%


















%
% --------------------------------------------------------------------
function export_menu_Callback(hObject, eventdata, handles)
% hObject    handle to workspace_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function workspace_menu_Callback(hObject, eventdata, handles)
% hObject    handle to workspace_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_21_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function exp_fig_image_Callback(hObject, eventdata, handles)
% hObject    handle to exp_fig_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function exp_fig_dose_Callback(~, ~, handles)
%
TPlan    = getappdata(handles.amb_interface,'TPlan');
name     = handles.dose_menu.String{handles.dose_menu.Value}; %#ok<*CCAT1>
%
sli      = getappdata(handles.amb_interface,'slices');
%
prompt   = {'Colormap (jet, gray, hot, ratio):','Direction (1-axial 2-coronal  3-Sagittal)'}; dlg_title = 'Integral Dose';  num_lines = 1;
def      = {'ratio','1'};                                                                     answer    = inputdlg(prompt,dlg_title,num_lines,def);
%
Cmap     = answer{1};
Dir      = str2double(answer{2});
%
if     Dir ==1
    figure_set;     imagesc(TPlan.dose.(sprintf('%s',name)).Dose(:,:,sli(1)),[0.5 1.5]); 
elseif Dir ==2
    figure_set;     imagesc(squeeze(TPlan.dose.(sprintf('%s',name)).Dose(sli(3),:,:)),[0.5 1.5]); 
elseif Dir ==3
    figure_set;     imagesc(squeeze(TPlan.dose.(sprintf('%s',name)).Dose(:,sli(2),:)),[0.5 1.5]); 
end
%
title(name);

if length(Cmap)>4 && strcmp(Cmap,'ratio')==1
    load('Cmap_ratio'); 
    colormap(Cmap_ratio)
else
    colormap(Cmap);
end
%
impixelinfo; daspect([1,1,1]); imcontrast;


% --------------------------------------------------------------------
function exp_fig_hist_Callback(~,~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
%
name     = handles.dose_menu.String{handles.dose_menu.Value}; %#ok<*CCAT1>
%
l        =strfind(name,'__');
ref_name =name(l+2:end);
%
prompt = {'Minimum dose/value: (e.g. 0 1 2 ascending order)','Resolution'}; dlg_title = 'Integral Dose';  num_lines = 1;
def    = {'0','0.02'};                                answer    = inputdlg(prompt,dlg_title,num_lines,def);
%
if isempty(answer)==1; return; end
%
M_dose = str2num(answer{1});
Res    = str2double(answer{2});
%
figure_set;
for i=1:length(M_dose)
    TPlan.dose.(sprintf('%s',name)).Dose(TPlan.dose.(sprintf('%s',name)).Dose<M_dose(i))=-999;
    r=reshape(TPlan.dose.(sprintf('%s',name)).Dose,[],1);
    r(r==-999)=[];
    l_name{i} = ['Doses >' num2str(M_dose(i))];
    %
    % 
    n_bins = ceil((max(r)-min(r))/Res);
    %
    histogram(r,min(M_dose):Res:Res*n_bins,'normalization','probability');
    if i==1
        hold on;
    end
end
%
title(name);
xlabel('Bins');
ylabel('probability');
legend(l_name);
%







% --------------------------------------------------------------------
% Integrate the dose slice by slice 
function exp_fig_integral_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
%
[s,~] = listdlg('PromptString','Select the dose(s):','SelectionMode','multiple','ListString',{handles.dose_menu.String{2:end}}); %#ok<*CCAT1>
%
if isempty(s)==1; return; end
%
prompt = {'Minimun dose:','Direction (1-axial 2-coronal  3-Sagittal)'}; dlg_title = 'Integral Dose';  num_lines = 1;
def    = {'0','2'};                                                    answer    = inputdlg(prompt,dlg_title,num_lines,def);
%
if isempty(answer)==1; return; end
%
Lim = str2double(answer{1});
Dir = str2double(answer{2});
%
for i=1:length(s)
    % length for different directions
    %
    name{i} = handles.dose_menu.String{s(i)+1};
    %
    Dose = TPlan.dose.(sprintf('%s',handles.dose_menu.String{s(i)+1})).Dose;
    Dose(Dose<Lim) = 0;
    %
    len  = size(TPlan.dose.(sprintf('%s',handles.dose_menu.String{s(i)+1})).Dose); 
    if       Dir == 1 
       if i == 1;    In_D=zeros(len(3),length(s));         end
       for sli = 1:len(3)
         In_D(sli,i) = sum(sum(Dose(:,:,sli)));
       end
    elseif   Dir == 2
       if i == 1;    In_D=zeros(len(1),length(s));         end
       for sli = 1:len(1)
         In_D(sli,i) = sum(sum(Dose(sli,:,:)));
       end 
    elseif   Dir == 3
       if i == 1;    In_D=zeros(len(1),length(s));         end
       for sli = 1:len(2)
         In_D(sli,i) = sum(sum(Dose(:,sli,:)));
       end    
    end
end
% 
In_D = flipud(In_D);
%
% Plot restuls ------------------------------------
figure_set; 
plot(In_D,'-','LineWidth',2);
title('Integral dose');
xlabel('Depth (mm)');
ylabel('Absolute dose');
xlim(1)=1;
%
% Change axes from voxels to mm -------------------
if Dir == 1
   L=get(gca,'XTick')*TPlan.image.Resolution(3);
elseif Dir == 2
   L=get(gca,'XTick')*TPlan.image.Resolution(1); 
elseif Dir == 3
   L=get(gca,'XTick')*TPlan.image.Resolution(2); 
end
set(gca,'XTickLabel',strsplit(num2str(L),' '),'LineWidth',2);    
%
% if only two doses are used ... create a ratio and show in the right axes
if length(s)==2
    yyaxis right;
    plot(In_D(:,1)./In_D(:,2),'-','Linewidth',1.5,'Color','g');
    name{3} = [name{1} ' / ' name{2}];
    ylabel('Ratio')
    set(gca,'YColor','g'); 
end
%
legend(name);




%
% --------------------------------------------------------------------
function export_im_Callback(~, ~, handles)
TPlan    = getappdata(handles.amb_interface,'TPlan');
assignin('base','Images',TPlan.image);


% --------------------------------------------------------------------
function export_doses_Callback(~, ~, handles)
TPlan    = getappdata(handles.amb_interface,'TPlan');
assignin('base','Doses',TPlan.dose.doses);


% --------------------------------------------------------------------
function export_plan_Callback(~, ~, handles)
TPlan    = getappdata(handles.amb_interface,'TPlan');
assignin('base','Plan',TPlan.plan);

% --------------------------------------------------------------------
function export_st_Callback(~, ~, handles)
c     =  handles.cont_pop_menu.Value;
idx   =  handles.material_pop_menu.Value;
%
TPlan =  getappdata(handles.amb_interface,'TPlan');
%
assignin('base',TPlan.struct.contours.(sprintf('Item_%.0f',c)).Name,TPlan.struct.contours.(sprintf('Item_%.0f',c)));


% --------------------------------------------------------------------
function export_all_Callback(~, ~, handles)
TPlan    = getappdata(handles.amb_interface,'TPlan');
assignin('base','TPlan',TPlan);


% --------------------------------------------------------------------
function export_profile_Callback(~, ~, handles)
TPlan=getappdata(handles.amb_interface,'TPlan');
%
[s,~] = listdlg('PromptString','Select the dose(s):','SelectionMode','multiple','ListString',{handles.dose_menu.String{2:end}}); %#ok<*CCAT1>
%
if isempty(s)==1; return; end
%
sli=getappdata(handles.amb_interface,'slices');
%
prompt = {'Direction (1-axial 2-coronal  3-Sagittal)'}; dlg_title = 'Profiles';  num_lines = 1;
def    = {'1'};                                         answer    = inputdlg(prompt,dlg_title,num_lines,def);
%
if isempty(answer)==1; return; end
%
Dir = str2double(answer{1});
%

figure_set;
for i=1:length(s)
   % length for different directions
   %
   name{i*2-1} = handles.dose_menu.String{s(i)+1};
   name{i*2}   = handles.dose_menu.String{s(i)+1};
   %
   if Dir == 1
     Dose = TPlan.dose.(sprintf('%s',handles.dose_menu.String{s(i)+1})).Dose(:,:,sli(1));
     plot(Dose(:,sli(2)),'LineWidth',2); hold on;
     plot(Dose(sli(3),:),'LineWidth',2);
   elseif Dir ==2
     Dose = squeeze(TPlan.dose.(sprintf('%s',handles.dose_menu.String{s(i)+1})).Dose(sli(3),:,:));
     plot(Dose(:,sli(1)),'LineWidth',2); hold on;
     plot(Dose(sli(2),:),'LineWidth',2);
   elseif Dir ==3
     Dose = squeeze(TPlan.dose.(sprintf('%s',handles.dose_menu.String{s(i)+1})).Dose(:,sli(2),:));
     plot(Dose(:,sli(1)),'LineWidth',2); hold on;
     plot(Dose(sli(3),:),'LineWidth',2);
   end
   %
   legend(name);
   %
end
%


% --------------------------------------------------------------------
function del_dose_Callback(hObject, eventdata, handles)
c     =  handles.cont_pop_menu.Value;
idx   =  handles.material_pop_menu.Value;
%
TPlan = getappdata(handles.amb_interface,'TPlan');
Mask  = TPlan.struct.contours.(sprintf('Item_%.0f',c)).Mask;
%
answer = questdlg('This will delete dose values inside or outside the selected contour !', ...
	'Delete dose', ...
	'Inside','Outside','Cancel','Cancel');
% Handle response
switch answer
    case 'Inside'
        warndlg(['Dose values wihtin the contour: "' TPlan.struct.contours.(sprintf('Item_%.0f',c)).Name '" will be set to zero']);
    case 'Outside'
        warndlg(['Dose values wihtin the contour: "' TPlan.struct.contours.(sprintf('Item_%.0f',c)).Name '" will be set to zero']);
        Mask = ~Mask;
end
%
Doses=fieldnames(TPlan.dose.doses);
for i=1:length(Doses)
  TPlan.dose.(sprintf('%s',Doses{i})).Dose(Mask==1)=0;   
end
setappdata(handles.amb_interface,'TPlan',TPlan);
update_images(4,getappdata(handles.amb_interface,'slices'));


% --------------------------------------------------------------------
function read_IrIS_data_Callback(hObject, eventdata, handles)
% check path to read IrIS

% read and display EPID images


% --------------------------------------------------------------------
function IrIS_menu_Callback(hObject, eventdata, handles)
% hObject    handle to IrIS_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
% function available only for internal users ... 
function read_irIS_seq_Callback(~, ~, handles)
if isempty(getappdata(handles.amb_interface,'TPlan'))==1
    setappdata(handles.amb_interface,'TPlan',import_treatmentplan_V2(0,'IrIS'));
else
    setappdata(handles.amb_interface,'TPlan',read_IrIS_amb(getappdata(handles.amb_interface,'TPlan')));
end
%
TPlan=getappdata(handles.amb_interface,'TPlan');
if isempty(TPlan.image)==1; return; end
%
w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
set(w_bar.hand,'Visible',1,'Value',80);
% User most select phantom and grid size ....
handles.phantom_user.Checked   ='off';
handles.phantom_all.Checked    ='off';
handles.dosegrid_user.Checked  ='off';
handles.dosegrid_all.Checked   ='off';
%
handles.draw_on.Value=0;
global draw_countour
draw_countour.on=0;
draw_countour.subtract=0;
delete(findobj('Tag','draw_pointer'));
set(handles.amb_interface,'pointer','default');
set(handles.amb_interface,'WindowButtonMotionFcn',[]);
% plot images
if isfield(TPlan.image,'Image')==0; errordlg('no image available'); return; end
plot_image_after_import(handles,TPlan);         set(w_bar.hand,'Visible',1,'Value',85);   
plot_empty_contour_layer(handles,TPlan);        set(w_bar.hand,'Visible',1,'Value',87);
plot_empty_dose_layer(handles,TPlan);           set(w_bar.hand,'Visible',1,'Value',89);
set_contour_list(TPlan,handles);                set(w_bar.hand,'Visible',1,'Value',91);
adjust_contrast_axes(handles,TPlan,1);          set(w_bar.hand,'Visible',1,'Value',93);
dose_menu(handles,TPlan);
dose_contrast_histogram(0);
%
material_menu(handles);                         set(w_bar.hand,'Visible',1,'Value',97);
ct_cal_menu(handles);
set_slice_guides;                         drawnow;
%
%
cla(handles.amb_dose_eval);
handles.clinical_par_list.String=[];
%
imview_adjust_pos(handles);
set(handles.cover_panel,'Visible','off'); drawnow;
set(w_bar.hand,'Visible',0,'Value',0);


%


% IrIS projections
function Show_Dw_MK_Callback(hObject, eventdata, handles)
plot_dw_markers;


% --------------------------------------------------------------------
function Untitled_24_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_25_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_26_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

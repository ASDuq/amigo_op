function [TPlan] = change_resolution(TPlan)
% resize image / dose 
answer = inputdlg('Define the voxel size','Interp.',1,{num2str(TPlan.image.Resolution)});
nv     = str2num(answer{1});
%
if sum(round(nv*10)/10==round(TPlan.image.Resolution*10)/10)==3; TPlan=[]; TPlan=-1; return; end
%
r = TPlan.image.Resolution./nv;
si= round(double(TPlan.image.Nvoxels).*r);
%
TPlan.image.Resolution=nv;
TPlan.image.Nvoxels   =si;
% -----------------------------------------------------
% dwell pos
TPlan.plan.dwellVox(:,1:3)=bsxfun(@times,TPlan.plan.dwellVox(:,1:3),r);
% image
if isfield(TPlan.plan,'catheter')==1
     names=fieldnames(TPlan.plan.catheter);
     for i=1:length(names)
         TPlan.plan.catheter.(sprintf('Cat_%.0f',i)).PointsVox= bsxfun(@times,TPlan.plan.catheter.(sprintf('Cat_%.0f',i)).PointsVox,r);
         TPlan.plan.catheter.(sprintf('Cat_%.0f',i)).PointsEd = bsxfun(@times,TPlan.plan.catheter.(sprintf('Cat_%.0f',i)).PointsEd,r);
     end
end
%
TPlan.image.Image=imresize3(TPlan.image.Image,TPlan.image.Nvoxels); 
%
fnames=fieldnames(TPlan.dose.doses);
if isempty(fnames)==0
    for i=1:length(fnames)
        TPlan.dose.doses.(sprintf(fnames{i})) = imresize3(TPlan.dose.doses.(sprintf(fnames{i})),TPlan.image.Nvoxels);
    end
end

%
if isfield(TPlan.plan,'Mat_HU')==1
   TPlan.plan.Mat_HU =[];
   TPlan.plan.Density=[];
%   warndlg('Create new Material and Density maps');
end
%
%
% contours 
if isfield(TPlan.struct,'contours')==0; return; end
%
names = fieldnames(TPlan.struct.contours);

for i=1:length(names)
    D=imresize3(double(TPlan.struct.contours.(sprintf('Item_%.0f',i)).Mask),TPlan.image.Nvoxels); 
    TPlan.struct.contours.(sprintf('Item_%.0f',i)).Mask=[]; 
    TPlan.struct.contours.(sprintf('Item_%.0f',i)).Mask=logical(D);
end

end

% function [D] = interpolate_im_dose(Old,si)
% D = imresize(double(Old),[si(1) si(2)]);         
% D = imresize(permute(D,[3 2 1]),[si(3) si(2)]);    
% D = permute(D,[3 2 1]);                        
% end
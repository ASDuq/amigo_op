function [ ] = phantom_size(varargin)
fig_r          =  findobj('Tag','amb_interface');
p=varargin{:};
phantom_pos=getappdata(fig_r,'phantom_region');
%
%
if isempty(getappdata(fig_r,'Phan_rec'))==1
    setappdata(fig_r,'Phan_rec',1);
end
%
h=gca;
% 
if p(1)<1; p(1)=1; end
if p(2)<1; p(2)=1; end
% drawnow;
if getappdata(fig_r,'Phan_rec')==1
   setappdata(fig_r,'Phan_rec',2); 
   %
   % update here
   if      strcmp(h.Tag,'amb_dos_01')
       %
      phantom_pos.xy_pos=p;
      phantom_pos.yz_pos=[phantom_pos.yz_pos(1) p(2) phantom_pos.yz_pos(3) p(4)];
      phantom_pos.xz_pos=[phantom_pos.xz_pos(1) p(1) phantom_pos.xz_pos(3) p(3)];
      setPosition(phantom_pos.xy,p);
      setPosition(phantom_pos.yz,phantom_pos.yz_pos);
      setPosition(phantom_pos.xz,phantom_pos.xz_pos);
   elseif  strcmp(h.Tag,'amb_dos_02')
       %
      phantom_pos.xy_pos=[phantom_pos.xy_pos(1) p(2) phantom_pos.xy_pos(3) p(4)];
      phantom_pos.yz_pos=p;
      phantom_pos.xz_pos=[p(1) phantom_pos.xz_pos(2) p(3) phantom_pos.xz_pos(4)]; 
      setPosition(phantom_pos.xy,phantom_pos.xy_pos);
      setPosition(phantom_pos.xz,phantom_pos.xz_pos);
   elseif  strcmp(h.Tag,'amb_dos_03')
      phantom_pos.xy_pos=[p(2) phantom_pos.xy_pos(2) p(4) phantom_pos.xy_pos(4)];
      phantom_pos.yz_pos=[p(1) phantom_pos.yz_pos(2) p(3) phantom_pos.yz_pos(4)]; 
      phantom_pos.xz_pos=p;
      setPosition(phantom_pos.xy,phantom_pos.xy_pos);
      setPosition(phantom_pos.yz,phantom_pos.yz_pos);
   end
   %
   setappdata(fig_r,'Phan_rec',1);
end
setappdata(fig_r,'phantom_region',phantom_pos);
% 
% save simulation data
TPlan=getappdata(fig_r,'TPlan');
TPlan.plan.phantom(1,1:2)=[phantom_pos.xy_pos(1) phantom_pos.xy_pos(1)+phantom_pos.xy_pos(3)]; 
TPlan.plan.phantom(2,1:2)=[phantom_pos.xy_pos(2) phantom_pos.xy_pos(2)+phantom_pos.xy_pos(4)];
TPlan.plan.phantom(3,1:2)=[phantom_pos.yz_pos(1) phantom_pos.yz_pos(1)+phantom_pos.yz_pos(3)];
TPlan.plan.phantom=round(TPlan.plan.phantom);
setappdata(fig_r,'TPlan',TPlan);
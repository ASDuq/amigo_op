% HR contour
% create a weight factor depeding on voxel % withing the volume - divides
% each voxel in 10x10 parts --- 5x5 might be enough and faster ...  needs
% further testing
%
tic
fil= fieldnames(TPlan.struct.contours);
%
division = 5;
%
for k=1:size(fil,1)
    if isfield(TPlan.struct.contours.(sprintf(fil{k})),'Points')==0
        continue;
    end
    IDX=find(TPlan.struct.contours.(sprintf(fil{k})).Points(:,3)==9999);
    IDX(end+1) = size(TPlan.struct.contours.(sprintf(fil{k})).Points,1);
    % wieght matrix
    Weight = int8(zeros(size(TPlan.struct.contours.(sprintf(fil{k})).Mask)));
    %
    for i=1:size(IDX,1)-1
        M = poly2mask((TPlan.struct.contours.(sprintf(fil{k})).PointsIm(IDX(i)+1:IDX(i+1)-1,1)+1)*division,(TPlan.struct.contours.(sprintf(fil{k})).PointsIm(IDX(i)+1:IDX(i+1)-1,2)+1)*division,512*division,512*division);
        if i==1
            sz = size(M);
        end
        ind = find(M==1);
        ID=[];
        [ID(:,1),ID(:,2)]=ind2sub(sz,ind);
        pos_Z = round(TPlan.struct.contours.(sprintf(fil{k})).PointsIm(IDX(i)+1,3))+1;
        ID    = ceil(ID/division);
        % V     = int8(zeros(size(ID,1),1));
        for j=1:size(ind,1)
            ii = ID(j,1);
            jj = ID(j,2);
            Weight(ii,jj,pos_Z) = Weight(ii,jj,pos_Z) +1;
           % V(j) = V(j) + 1;
        end  
    end
    TPlan.struct.contours.(sprintf(fil{k})).Weight = Weight;
    TPlan.struct.contours.(sprintf(fil{k})).Name
    toc
end
toc
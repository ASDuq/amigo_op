function export_cli_param_excel
handles = guihandles; TPlan   = getappdata(handles.amb_interface,'TPlan');
%
[File,Folder] = uiputfile('*.xlsx');
filename      = [Folder File];
%
% organize it to export
Structures        =  fieldnames(TPlan.parameters.Structures);
%
Doses             =  fieldnames(TPlan.parameters);
% adjust name for table
for i=1:size(Doses,1)-1
Dose{i*2-1}=Doses{i};
end
%
DVH=[];
N  =[];
T  =[];
for k=1:size(Structures,1)
    if k==1
        T    = table2array(TPlan.parameters.Structures.(sprintf(Structures{k})).Clin_Parameters);
        N{1} = Structures{k};
        N    = vertcat(N,TPlan.parameters.Structures.(sprintf(Structures{k})).Clin_Parameters.Properties.RowNames);
        %
        D    = TPlan.parameters.Structures.(sprintf(Structures{k})).DVH;
        %
    else
        N = vertcat(N,' ',' ',Structures(k));
        N = vertcat(N,TPlan.parameters.Structures.(sprintf(Structures{k})).Clin_Parameters.Properties.RowNames);
        T = vertcat(T,zeros(3,size(T,2)));
        T = vertcat(T,table2array(TPlan.parameters.Structures.(sprintf(Structures{k})).Clin_Parameters));
    end
    
end
%
copyfile(which('exc_template.xlsx'),filename,'f');
%
tic
 xlswrite(filename,N,'Clinical Parameters','A1');
 xlswrite(filename,T,'Clinical Parameters','B2');
p=size(TPlan.parameters.Structures.(sprintf(Structures{k})).Clin_Parameters,1);
for k=1:size(Structures,1)
    row = (p+3)*(k-1)+1;
    xlswrite(filename,Doses(1:end-1)','Clinical Parameters',['B' num2str(row)]);
end
winopen(filename);
%
%
%
end
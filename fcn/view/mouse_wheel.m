function [  ] = mouse_wheel(~,callbackdata)
fig_r          =  findobj('Tag','amb_interface');
current_slices =  getappdata(fig_r,'slices');
h              =  getappdata(fig_r,'view_patch');
TPlan          =  getappdata(fig_r,'TPlan');
%
fig=gca; %#ok<*NASGU>
%
if     strcmp(fig.Tag,'amb_dos_01')==1
          sli(1)=current_slices(1)+callbackdata.VerticalScrollCount;
          if sli<1; sli=1; elseif sli> TPlan.image.Nvoxels(3); sli=TPlan.image.Nvoxels(3); end;
          update_images(1,sli);
          move_01(h,sli);
elseif strcmp(fig.Tag,'amb_dos_02')==1
          sli(2)=current_slices(2)+callbackdata.VerticalScrollCount;
          if sli<1; sli=1; elseif sli> TPlan.image.Nvoxels(1); sli=TPlan.image.Nvoxels(1); end;
          update_images(2,sli);
          move_02(h,sli);
elseif strcmp(fig.Tag,'amb_dos_03')==1
          sli(3)=current_slices(3)+callbackdata.VerticalScrollCount;
          if sli<1; sli=1; elseif sli> TPlan.image.Nvoxels(2); sli=TPlan.image.Nvoxels(2); end;
          update_images(3,sli);
          move_03(h,sli);
end
end

function move_01(h,sli)
       P1=get(h.slice_rec_up02,'Position'); P1(1)=sli(1)-P1(3)/2;
       P2=get(h.slice_rec_bt02,'Position'); P2(1)=P1(1);
       set(h.slice_rec_up02,'Position',P1);
       set(h.slice_rec_bt02,'Position',P2);
       set(h.slice_lin_ve02,'XData',[sli(1) sli(1)]);
       %
       P1=get(h.slice_rec_up03,'Position'); P1(1)=sli(1)-P1(3)/2;
       P2=get(h.slice_rec_bt03,'Position'); P2(1)=P1(1);
       set(h.slice_rec_up03,'Position',P1);
       set(h.slice_rec_bt03,'Position',P2);
       set(h.slice_lin_ve03,'XData',[sli(1) sli(1)]);
end

function move_02(h,sli)
       P1=get(h.slice_rec_up01,'Position'); P1(1)=sli(2)-P1(3)/2;
       P2=get(h.slice_rec_bt01,'Position'); P2(1)=P1(1);
       set(h.slice_rec_up01,'Position',P1);
       set(h.slice_rec_bt01,'Position',P2);
       set(h.slice_lin_ve01,'XData',[sli(2) sli(2)]);
       %
       P1=get(h.slice_rec_lf03,'Position'); P1(2)=sli(2)-P1(4)/2;
       P2=get(h.slice_rec_ri03,'Position'); P2(2)=P1(2);
       set(h.slice_rec_lf03,'Position',P1);
       set(h.slice_rec_ri03,'Position',P2);
       set(h.slice_lin_ho03,'YData',[sli(2) sli(2)]);       
end

function move_03(h,sli)
       P1=get(h.slice_rec_lf01,'Position'); P1(2)=sli(3)-P1(4)/2;
       P2=get(h.slice_rec_ri01,'Position'); P2(2)=P1(2);
       set(h.slice_rec_lf01,'Position',P1);
       set(h.slice_rec_ri01,'Position',P2);
       set(h.slice_lin_ho01,'YData',[sli(3) sli(3)]);
       %
       P1=get(h.slice_rec_lf02,'Position'); P1(2)=sli(3)-P1(4)/2;
       P2=get(h.slice_rec_ri02,'Position'); P2(2)=P1(2);
       set(h.slice_rec_lf02,'Position',P1);
       set(h.slice_rec_ri02,'Position',P2);
       set(h.slice_lin_ho02,'YData',[sli(3) sli(3)]);
end



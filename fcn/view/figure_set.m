fontsize=25;
font='Arial';
scrsz = get(0,'ScreenSize'); % half size of the screen
f=figure('Position',[scrsz(3)/8 scrsz(4)/8 scrsz(3)/1.5 scrsz(4)/1.3]);
bgcolor='w';
set(f, 'color',bgcolor);
set(f, 'DefaultTextFontSize', fontsize); 
set(f, 'DefaultAxesFontSize', fontsize); 
set(f, 'DefaultAxesFontName', font);
set(f, 'DefaultTextFontName', font);
title('Title');
xlabel('XLabel');
ylabel('YLabel');
box on;
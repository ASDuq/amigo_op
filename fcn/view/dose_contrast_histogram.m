%
function dose_contrast_histogram(exe)
handles = guihandles(findobj('Tag','amb_interface'));
TPlan=getappdata(handles.amb_interface,'TPlan');
h=handles.amb_dos_contrast;
ax=getappdata(handles.amb_interface,'ax');
%
pt = getappdata(handles.amb_interface,'view_patch');
%
if      exe ==0
    colormap(handles.amb_dos_01,'jet'); colormap(handles.amb_dos_02,'jet'); 
    colormap(handles.amb_dos_03,'jet'); colormap(handles.amb_dos_contrast2,'jet');
    set(h,'Color','none','Visible','on','xcolor','k','ycolor','k','Position',[0.001 0.62 0.174 0.08]); axis on; box on;
    set(handles.amb_dos_contrast2,'Visible','on','xcolor','k','ycolor','k','Position',[0.001 0.62 0.174 0.08]); axis on; box on;
    cla(h); 
    xy=[];   x=[1 100];   xy(1,:)=(x(1):diff(x)/128:x(2));
    %
    imagesc(handles.amb_dos_contrast2,xy,'AlphaData',0.9);                               hold(h,'on'); 
    area(h, 1:0.5:100.5,zeros(1,200),'FaceColor','none','EdgeColor','none'); drawnow;
    set(h,'xlim',[0 100],'XTickLabel',10:25:90,'XTick',10:25:90,'YTick',[],'ylim',[0.5 1.5]);
    %
    ax.dosecontrast.scale=get(handles.amb_dos_contrast2,'Child');
    ax.dosecontrast.bar  =get(h,'Child');
    set(handles.amb_dos_contrast,'ButtonDownFcn',@set_dosecontrast_axes_limit)
    set(ax.dosecontrast.bar,'ButtonDownFcn',@set_dosecontrast_axes_limit)
    setappdata(handles.amb_interface,'ax',ax);
%    
elseif  exe ==1
  if strcmp(handles.dose_menu.String{handles.dose_menu.Value},'none')==1
     set(ax.dosecontrast.bar,'Ydata',zeros(1,200),'FaceColor','none','EdgeColor','none');
     return;
  end
  I=TPlan.dose.(sprintf('%s',handles.dose_menu.String{handles.dose_menu.Value})).Dose;
  %
  I=reshape(single(I),[],1);
  %
  I(I==-999)=[]; % inf and nan were rplaced with -999 and should not be considered
  %
  [nelements,centers] = hist(I,300); nelements=nelements/max(nelements(2:end))+0.5; 
  %
  xy=[];   x=[0 max(centers)];   xy(1,:)=(x(1):diff(x)/128:x(2));
  set(ax.dosecontrast.scale,'CData',xy);          
  set(ax.dosecontrast.bar,'XData',centers,'YData',nelements,'FaceColor','w','EdgeColor','k'); drawnow;
  %
elseif exe == 2  % restore  
    centers= ax.dosecontrast.bar.XData;
    Y      = ax.dosecontrast.bar.YData-0.5;
    Y      = Y/max(Y(2:end))+0.5;
    ax.dosecontrast.bar.YData=Y;
    xy=[];   x=[0 max(centers)];
    xy(1,:)=(x(1):diff(x)/128:max(centers));
    set(ax.dosecontrast.scale,'CData',xy);
    %
elseif exe == 3    % adjust limits
    if strcmp(handles.dose_menu.String(handles.dose_menu.Value),'none')==1
        warndlg('Select a dose distribution first');
        return;
    end
    centers=[]; 
    centers(1)=pt.dosecontrast_min_lin.XData(1);
    centers(2)=pt.dosecontrast_max_lin.XData(1);
    %
    X=ax.dosecontrast.bar.XData;
    Y=ax.dosecontrast.bar.YData-0.5;
    Y=Y/max(Y(X>centers(1)))+0.5;
    ax.dosecontrast.bar.YData=Y;
    xy=[];   x=[centers(1) centers(2)];
    xy(1,:)=(x(1):diff(x)/128:x(2)); 
    set(ax.dosecontrast.scale,'CData',xy);          
    %
end
%
%
if isempty(pt) == 1;                return; end
if isvalid(pt.contrast_max_box)==0; return; end
%
set(h,'xlim',[min(centers) max(centers)], ...
      'XTickLabel',num2cell(round((min(centers)+diff(x)*0.05:(max(centers)-min(centers))/5:(max(centers)-diff(x)*0.05))*100)/100), ...
      'XTick',min(centers)+diff(x)*0.05:diff(x)/5:max(centers)-diff(x)*0.05,'YTick',[],'ylim',[0.5 1.5]);
%
l(1)=min(centers); l(2)=max(centers);
%
set(handles.amb_dos_01,        'CLim',[l(1) l(2)]);
set(handles.amb_dos_02,        'CLim',[l(1) l(2)]);
set(handles.amb_dos_03,        'CLim',[l(1) l(2)]);
set(handles.amb_dos_contrast2, 'CLim',[l(1) l(2)]);
%
pt.dosecontrast_min_box.Position(3)= diff(l)*0.06;
pt.dosecontrast_max_box.Position(3)= diff(l)*0.06;
%
pt.dosecontrast_min_box.Position(1)= l(1)-pt.dosecontrast_min_box.Position(3)/2;
pt.dosecontrast_min_lin.XData      = [l(1)  l(1)];
pt.dosecontrast_max_box.Position(1)= l(2)-pt.dosecontrast_max_box.Position(3)/2;
pt.dosecontrast_max_lin.XData      = [l(2)  l(2)];
%
end


function [] = set_dosecontrast_axes_limit(varargin)
persistent chk
if isempty(chk)
      chk = 1;
      pause(0.5); %Add a delay to distinguish single click from a double click
      if chk == 1
         % fprintf(1,'\nI am doing a single-click.\n\n');
          chk = [];
      end
else
      chk = [];
      %
      dose_contrast_histogram(3); 
    %
end
end
function [  ] = update_isodoses(fig,TPlan,sli,ax)
% TPlan=getappdata(handles.amb_interface,'TPlan');
% sli=getappdata(handles.amb_interface,'slices');
% ax=getappdata(handles.amb_interface,'ax');
iso = getappdata(ax.gui,'isod_settings');
%
%
for i=1:3   
    if i>size(iso.s,2); return; end
    %
        I1=        TPlan.dose.doses.(sprintf(ax.dose_menu.String{iso.s(i)+1}))(:,:,sli(1));
        I2=squeeze(TPlan.dose.doses.(sprintf(ax.dose_menu.String{iso.s(i)+1}))(:,sli(2),:));
        I3=squeeze(TPlan.dose.doses.(sprintf(ax.dose_menu.String{iso.s(i)+1}))(sli(3),:,:));
    %
    %
    color=iso.(sprintf('line0%.0f',i))(1:3);
    dash =iso.(sprintf('line0%.0f',i))(4);
    width=iso.(sprintf('line0%.0f',i))(5);
    %
    if fig==1 || fig==4
        if i==1; delete(findobj('Tag','Contour01')); end
        if dash==2
          [C,h] = contour(ax.dos01.Parent,I1,iso.dvalues,'LineColor',color,'LineWidth',width,'LineStyle', '--','Tag','Contour01');  %#ok<ASGLU>
        else
          [C,h] = contour(ax.dos01.Parent,I1,iso.dvalues,'LineColor',color,'LineWidth',width,'LineStyle', '-','Tag','Contour01'); 
        end
    end
    %
    if fig==2 || fig==4
        if i==1; delete(findobj('Tag','Contour02')); end
        if dash==2
          [C,h] = contour(ax.dos02.Parent,I2,iso.dvalues,'LineColor',color,'LineWidth',width,'LineStyle', '--','Tag','Contour02');  %#ok<ASGLU>
        else
          [C,h] = contour(ax.dos02.Parent,I2,iso.dvalues,'LineColor',color,'LineWidth',width,'LineStyle', '-','Tag','Contour02'); 
        end
    end
    %
    if fig==3 || fig==4
        if i==1; delete(findobj('Tag','Contour03')); end
        if dash==2
          [C,h] = contour(ax.dos03.Parent,I3,iso.dvalues,'LineColor',color,'LineWidth',width,'LineStyle', '--','Tag','Contour03');  %#ok<ASGLU>
        else
          [C,h] = contour(ax.dos03.Parent,I3,iso.dvalues,'LineColor',color,'LineWidth',width,'LineStyle', '-','Tag','Contour03'); 
        end
    end
end
end


function [ ] = patch_contrast_update( varargin )
set(findobj('Tag','amb_interface'),'WindowButtonMotionFcn', @update_contrast);
end

function [ ] = update_contrast( varargin )
%
handles = guihandles;
ax      = getappdata(handles.amb_interface,'ax');
pt      = getappdata(handles.amb_interface,'view_patch');
mouse_p = get(handles.amb_vis_contrast,'CurrentPoint');
x=xlim;
%
if strcmp(get(gco,'Tag'),'Contrast_01')==1
    P=pt.contrast_min_box.Position;
    P(1)=mouse_p(1,1)-P(3)/2;
    if P(1)>pt.contrast_max_box.Position(1)
       P(1)=pt.contrast_max_box.Position(1)-1;
    elseif P(1)<x(1)
       P(1)=x(1)-P(3)/2;
    end
    pt.contrast_min_box.Position=P;
    pt.contrast_min_lin.XData=[P(1)+P(3)/2 P(1)+P(3)/2];
else
    P=pt.contrast_max_box.Position;
    P(1)=mouse_p(1,1)-P(3)/2;
    if P(1)<pt.contrast_min_box.Position(1)
       P(1)=pt.contrast_min_box.Position(1)+1;
    elseif P(1)>x(2)
       P(1)=x(2)-P(3)/2;       
    end
    pt.contrast_max_box.Position=P;
    pt.contrast_max_lin.XData=[P(1)+P(3)/2 P(1)+P(3)/2];

end
l(1)=pt.contrast_min_box.Position(1)+pt.contrast_min_box.Position(3)/2 ...
     -ax.contrast.shift;
l(2)=pt.contrast_max_box.Position(1)+pt.contrast_max_box.Position(3)/2 ...
     -ax.contrast.shift;
%
set(handles.amb_vis_01,'CLim',[l(1) l(2)]);
set(handles.amb_vis_02,'CLim',[l(1) l(2)]);
set(handles.amb_vis_03,'CLim',[l(1) l(2)]);
set(handles.amb_vis_contrast2,'CLim',[l(1)+ax.contrast.shift l(2)+ax.contrast.shift]);
%

end
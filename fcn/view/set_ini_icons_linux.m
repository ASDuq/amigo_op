function  set_ini_icons_linux(varargin)
% Contour on
% 
handles = guihandles; 
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'View_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
im=imread(iconFilename,'BackgroundColor',[1 1 1]);
set(handles.show_material,'String','','CData',im);
set(handles.contour_on_off,'String','','CData',im);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'x_mark.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.contour_all_off,'String','','CData',im);
set(handles.show_mat_off,'String','','CData',im);
set(handles.ct_cal_delete,'String','','CData',im);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'iso_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.isodose_view,'String','','CData',im,'UserData',0);
%
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'save.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.save_mathu,'String','','CData',im,'UserData',0);
set(handles.Save_materials,'String','','CData',im,'UserData',0);
set(handles.ct_cal_save,'String','','CData',im,'UserData',0);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'open.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.load_mathu,'String','','CData',im,'UserData',0);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'mat_map.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.create_mat_map,'String','','CData',im,'UserData',0);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'density.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.creat_dens_map,'String','','CData',im,'UserData',0);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'materials.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.edit_materials,'String','','CData',im,'UserData',0);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'ct_cal.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.edit_ct_cal,'String','','CData',im,'UserData',0);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'new_file.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end


set(handles.new_ct_cal,'String','','CData',im);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'refresh_01.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end


set(handles.save_cat_change,'String','','CData',im);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'cube.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end

set(handles.set_phantom_size,'String','','CData',im);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'vox.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.set_dose_grid,'String','','CData',im);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'execute.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.imedit_do,'String','','CData',im);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'undo.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.imedit_undo,'String','','CData',im);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'erase.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.imedit_erase,'String','','CData',im);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'draw_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.draw_on,'String','','CData',im);
handles.draw_sub.Value=0;
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'draw_paint.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.draw_sub,'String','','CData',im);
handles.draw_sub.Value=0;
%
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'cont_add.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.cont_add,'String','','CData',im);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'cont_rename.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.cont_rename,'String','','CData',im);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'cont_delete.png'];   iconFilename = strrep(iconFilename, '\', '/');
[im,map]=imread(iconFilename,'BackgroundColor',[1 1 1]);
if size(im,3)==1
    im=ind2rgb(im,map);
end
set(handles.cont_delete,'String','','CData',im);
end
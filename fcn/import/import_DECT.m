function [TPlan] = import_DECT(TPlan)
[~,fname]        = uigetfile({'*.dcm;*.IMA'},'Select one file to open the whole folder'); 
%
if isnumeric(fname)==1; return; end

list  = dir(fname);
list  = list(logical([list.isdir]-1));       % remove folders from the list
% sort files
file=[];
for i=1:size(list,1)
  [~,first,last] = regexp(list(i).name,'\d+','match','start','end');
  file{str2num(list(i).name(first(end):last(end)))}= [fname list(i).name];  
end
file=file(~cellfun('isempty',file));
%
%
for i=1:size(file,2)
    info_dcm=dicominfo(file{i});
    if i==1
        if  isfield(TPlan,'DECT')==0 || isfield(TPlan.DECT,'Resolution')==0
           TPlan.DECT.Resolution(1:2)    = info_dcm.PixelSpacing;  
           if      isfield(info_dcm,'SliceThickness')==1     && isempty(info_dcm.SliceThickness)==0
               TPlan.DECT.Resolution(3)         = info_dcm.SliceThickness;
           elseif  isfield(info_dcm,'SpacingBetweenSlices')==1   && isempty(info_dcm.SpacingBetweenSlices)==0
               TPlan.DECT.Resolution(3)         = abs(info_dcm.SpacingBetweenSlices);
           else    
               TPlan.DECT.Resolution(3)         = 0;  
           end
           %
           %
           if isfield(info_dcm,'SpacingBetweenSlices')==1
               TPlan.DECT.SpacingBetweenSlices    = info_dcm.SpacingBetweenSlices;
           else
               TPlan.DECT.SpacingBetweenSlices    = 0;
           end
           TPlan.DECT.Nvoxels(1)                           = info_dcm.Width;
           TPlan.DECT.Nvoxels(2)                           = info_dcm.Height;
           TPlan.DECT.RescaleIntercept                     = info_dcm.RescaleIntercept;
           TPlan.DECT.RescaleSlope                         = info_dcm.RescaleSlope;
           TPlan.DECT.ImagePositionPatient01               = info_dcm.ImagePositionPatient;    % upper and bottom references
           TPlan.DECT.ImagePositionPatient01(4)            = info_dcm.InstanceNumber;
           TPlan.DECT.ImagePositionPatient02               = info_dcm.ImagePositionPatient;
           TPlan.DECT.ImagePositionPatient02(4)            = info_dcm.InstanceNumber;
           TPlan.DECT.ImageOrientationPatient   = info_dcm.ImageOrientationPatient;
        end
        %
        % use instance number and slice position to find the slice
        % thickness if it was not provided before
        if      TPlan.DECT.Resolution(3)==0 && isfield(TPlan.DECT,'ref_pos_z')==0
            TPlan.DECT.ref_pos_z(1,1) = info_dcm.ImagePositionPatient(3);
            TPlan.DECT.ref_pos_z(1,2) = info_dcm.InstanceNumber;
        elseif  TPlan.DECT.Resolution(3)==0 && isfield(TPlan.DECT,'ref_pos_z')==1
            TPlan.DECT.ref_pos_z(2,1) = info_dcm.ImagePositionPatient(3);
            TPlan.DECT.ref_pos_z(2,2) = info_dcm.InstanceNumber;
            %
            Res_z                    = abs(diff(obj.image.ref_pos_z));
            TPlan.DECT.Resolution(3)  = Res_z(1)/Res_z(2);
        end
        %
        %
        if isempty(list)==1
          TPlan.DECT.Nvoxels(3) = size(TPlan.DECT.Image,3);
          return; 
        end
        
        % Additional image information
        %
        TPlan.DECT.KVP                        = info_dcm.KVP;
        TPlan.DECT.ReconstructionDiameter     = info_dcm.ReconstructionDiameter;
        TPlan.DECT.XrayTubeCurrent            = info_dcm.XrayTubeCurrent;
        TPlan.DECT.FilterType                 = info_dcm.FilterType;
        TPlan.DECT.ConvolutionKernel          = info_dcm.ConvolutionKernel;
        TPlan.DECT.PatientPosition            = info_dcm.PatientPosition;  
        TPlan.DECT.TableSpeed                 = info_dcm.TableSpeed;
        TPlan.DECT.SpiralPitchFactor          = info_dcm.SpiralPitchFactor;
        TPlan.DECT.RescaleSlope               = info_dcm.RescaleSlope;
        TPlan.DECT.RescaleIntercept           = info_dcm.RescaleIntercept;
        %
        w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
        set(w_bar.hand,'Visible',1);
    end
    % read image
    if info_dcm.InstanceNumber>0
      t=evalc('TPlan.DECT.Image_02(:,:,info_dcm.InstanceNumber)=dicomread(file{i})');
    end
    %
    %
    if TPlan.DECT.ImagePositionPatient01(3)  > info_dcm.ImagePositionPatient(3)
        TPlan.DECT.ImagePositionPatient01    = info_dcm.ImagePositionPatient;
        TPlan.DECT.ImagePositionPatient01(4) = info_dcm.InstanceNumber;
    end
    %
    if TPlan.DECT.ImagePositionPatient02(3)  < info_dcm.ImagePositionPatient(3)
        TPlan.DECT.ImagePositionPatient02    = info_dcm.ImagePositionPatient;
        TPlan.DECT.ImagePositionPatient02(4) = info_dcm.InstanceNumber;
    end
    %
    set(w_bar.hand,'Value',i/size(file,2)*100);
%
end
%
% convert to HU
TPlan.DECT.Image_02=int16(TPlan.DECT.Image_02)/TPlan.DECT.RescaleSlope+TPlan.DECT.RescaleIntercept;
%
% Flip if necessary
if TPlan.DECT.ImagePositionPatient01(4)>TPlan.DECT.ImagePositionPatient02(4)
   TPlan.DECT.Image_02=flip(TPlan.DECT.Image_02,3);
end
%
% Slice thickness does not necessary represent tthe Spacing
% between the slices
%
%
%
TPlan.DECT.ImagePositionPatient  =  TPlan.DECT.ImagePositionPatient01(1:3)';
TPlan.DECT                       =  rmfield(TPlan.DECT,'ImagePositionPatient01');
TPlan.DECT                       =  rmfield(TPlan.DECT,'ImagePositionPatient02');
%
% Image_01 lowest kVP
if TPlan.image.KVP > TPlan.DECT.KVP
    TPlan.DECT.Image_01 = TPlan.DECT.Image_02;
    TPlan.DECT.Image_02 = TPlan.image.Image;
    TPlan.DECT.KVPs = [TPlan.DECT.KVP TPlan.image.KVP];
else
    TPlan.DECT.Image_01 = TPlan.image.Image;
    TPlan.DECT.KVPs     = [TPlan.image.KVP TPlan.DECT.KVP];
end
%
set(w_bar.hand,'Visible',0,'Value',0);
drawnow;
end


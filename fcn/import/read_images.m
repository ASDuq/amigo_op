function obj = read_images(obj,Files,nfolder)
%   Check how many studies and series 
        PT      = unique(Files(:,2));         % number of patients
        %
        for k =1:size(PT,1) % 
            % 
            % get only files related within the same study
            st  = {}; 
            IDX = find(ismember(Files(:,2),PT{k})); 
            st  = Files(IDX,:);
            % add additional "for" in case of multiple series
            % SE  = unique(cell2mat(Files(:,5))); % number of series per study
            %
            % Check modality and open CT images
            IDX = find(ismember(st(:,3),'CT'));
            %
            if isempty(IDX)==1; continue; end 
            %  
            if isempty(findobj('Tag','amb_interface'))==0
                w_bar.hand = getappdata(findobj('Tag','amb_interface'),'waitbarjava');
                set(w_bar.hand,'Visible',1,'Value',0);
            else
                w_bar=[];
            end
            %
            for i = 1:length(IDX)
                if isempty(w_bar)==0
                  set(w_bar.hand,'Visible',1,'Value',i/length(IDX)*100);
                end
                if i==1
                  obj.Studies.(sprintf(['P' PT{k}])).image.SliceThickness            = st{IDX(i),31};
                  obj.Studies.(sprintf(['P' PT{k}])).image.SpacingBetweenSlices      = st{IDX(i),32};
                  obj.Studies.(sprintf(['P' PT{k}])).image.KVP                       = st{IDX(i),22};
                  obj.Studies.(sprintf(['P' PT{k}])).image.ReconstructionDiameter    = st{IDX(i),26};     
                  obj.Studies.(sprintf(['P' PT{k}])).image.XrayTubeCurrent           = st{IDX(i),23};
                  obj.Studies.(sprintf(['P' PT{k}])).image.FilterType                = st{IDX(i),24};
                  obj.Studies.(sprintf(['P' PT{k}])).image.ConvolutionKernel         = st{IDX(i),25};
                  obj.Studies.(sprintf(['P' PT{k}])).image.ImagePositionPatient      = st{IDX(i),6}';
                  % Find min (z)
                  z = cell2mat(st(:,6)); 
                  obj.Studies.(sprintf(['P' PT{k}])).image.ImagePositionPatient(3)   = min(z(3:3:end));
                  %
                  obj.Studies.(sprintf(['P' PT{k}])).image.ImageOrientationPatient   = st{IDX(i),21}';  
                  obj.Studies.(sprintf(['P' PT{k}])).image.TableSpeed                = st{IDX(i),16};
                  obj.Studies.(sprintf(['P' PT{k}])).image.SpiralPitchFactor         = st{IDX(i),29};
                  obj.Studies.(sprintf(['P' PT{k}])).image.RescaleSlope              = st{IDX(i),19};
                  obj.Studies.(sprintf(['P' PT{k}])).image.RescaleIntercept          = st{IDX(i),18};
                  obj.Studies.(sprintf(['P' PT{k}])).image.Nvoxels                   = [st{IDX(i),17}  st{IDX(i),16} length(IDX)];
                  obj.Studies.(sprintf(['P' PT{k}])).image.Resolution                = [st{IDX(i),30}' st{IDX(i),31}];
                end
                % Read images
                % if instance number is missing calculate it
                %
                %
                if isempty(st{IDX(i),20})==1 || st{IDX(i),20}==0
                  Int_pos     = st{IDX(i),6}'-obj.Studies.(sprintf(['P' PT{k}])).image.ImagePositionPatient;
                  st{IDX(i),20} = round(Int_pos(3) / obj.Studies.(sprintf(['P' PT{k}])).image.Resolution(3)) + 1;
                end
                obj.Studies.(sprintf(['P' PT{k}])).image.Image(:,:,st{IDX(i),20})   = int16(dicomread([nfolder st{IDX(i),1}]))/st{IDX(i),19}+st{IDX(i),18};
            end 
            %
            % Some images needs to be flipped depending on the manufacturer 
            Pos         = reshape(cell2mat(st(:,6)),3,[])';
            [~,IDX_max] = max(Pos(:,3));
            [~,IDX_min] = min(Pos(:,3));
            %
            if st{IDX_min,20} > st{IDX_max,20}
                obj.Studies.(sprintf(['P' PT{k}])).image.Image = flip(obj.Studies.(sprintf(['P' PT{k}])).image.Image,3);
                disp('Image was flipped to match coordinate system');
            end
            %
        end
end
classdef import_treatmentplan 
   properties
      image      = [];
      plan       = [];
      dose       = []; 
      struct     = [];
      parameters = [];
      DECT       = [];
      IrIS       = [];
   end
   methods
      function obj = import_treatmentplan(varargin)
         if nargin == 0
            [file,nfolder]=uigetfile({'*.dcm;*.AMB_plan;*.IMA;*.egsphant;*.g4dcm;*.bin;*.inp;*.o'},'Select one file to open the whole folder'); 
            if isnumeric(nfolder)==1; return; end
         elseif nargin == 1
             nfolder=varargin{1};
         elseif nargin == 2 && strcmp(varargin{2},'IrIS')==1
             obj = read_IrIS_amb(obj);
             return;
         end
         %
         w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
         set(w_bar.hand,'Visible',1);
         %
         % AMIGO project
         if  size(file,2)>8 && strcmp(file(end-7:end),'AMB_plan')==1
             set(w_bar.hand,'Value',25);
             load([nfolder file],'-mat');
             obj.image                 = TPlan.image;
             obj.plan                  = TPlan.plan;
             obj.dose                  = TPlan.dose;
             obj.struct                = TPlan.struct;
             obj.image.load.ed_all_mat = ed_all_mat;
             obj.image.load.mat_menu   = mat_menu;
             obj.image.load.mat_limits = mat_limits;
             setappdata(gcf,'phantom_region',phantom_pos);
             setappdata(gcf,'score_region',score_pos);
             set(w_bar.hand,'Value',75);
             if isfield(TPlan.dose,'Dose_TPS')==1
                 obj.dose.doses.Dose_TPS=TPlan.dose.Dose_TPS;
                 obj.dose=rmfield(obj.dose,'Dose_TPS');
             end
             return;
         elseif size(file,2)>8 && strcmp(file(end-7:end),'egsphant')
             set(w_bar.hand,'Value',25);
             obj=import_egsphant(obj,[nfolder file]);
             set(w_bar.hand,'Value',75);
             obj.struct.contours.(sprintf('Item_%.0f',1)).Name='Edit';
             obj.struct.contours.(sprintf('Item_%.0f',1)).Mask=logical(zeros(obj.image.Nvoxels));
             return;
         elseif size(file,2)>5 && strcmp(file(end-4:end),'g4dcm')
             set(w_bar.hand,'Value',25);
             obj=import_g4dcm(obj,nfolder);
             set(w_bar.hand,'Value',75);
             obj.struct.contours.(sprintf('Item_%.0f',1)).Name='Edit';
             obj.struct.contours.(sprintf('Item_%.0f',1)).Mask=logical(zeros(obj.image.Nvoxels));
             return;  
         elseif strcmp(file(end-2:end),'bin')         % Moby
             set(w_bar.hand,'Value',25);
             obj=import_bin(obj,[nfolder file]);
             set(w_bar.hand,'Value',75);
             obj.struct.contours.(sprintf('Item_%.0f',1)).Name='Edit';
             obj.struct.contours.(sprintf('Item_%.0f',1)).Mask=logical(zeros(obj.image.Nvoxels));
             return;
         elseif strcmp(file(end-2:end),'IMA')          % CT image
             list=dir([nfolder '*.IMA']); 
             if length(list)==1
                 figure; imagesc(dicomread([nfolder list.name])); 
                 drawnow;
                 impixelinfo;
                 imcontrast; 
                 daspect([1,1,1]);
                 set(w_bar.hand,'Visible',0);
                 return;
             end
             % wait bar
             w_bar.files=size(list,1);  
             %
             obj=read_image_dcm(obj,nfolder,list,w_bar);
             obj.image.ImagePositionPatient=obj.image.ImagePositionPatient01(1:3)';
             d=obj.image.ImagePositionPatient01(3:4)-obj.image.ImagePositionPatient02(3:4);
             obj.image.Resolution(3)=abs(d(1)/d(2));
             if obj.image.ImagePositionPatient01(4)>obj.image.ImagePositionPatient02(4)
                obj.image.Image=flip(obj.image.Image,3);
                if isfield(obj.plan,'catheter')==1 && ...
                    isfield(obj.plan.catheter.Cat_1,'Points') % flip catheter
                     obj=flip_catheter(obj);
                end
             end
             %
             % Slice thickness does not necessary represent tthe Spacing
             % between the slices
             %
             %
             %
             obj.image=rmfield(obj.image,'ImagePositionPatient01');
             obj.image=rmfield(obj.image,'ImagePositionPatient02');
             %
             % Converte to HU
             obj.image.Image=int16(obj.image.Image)/obj.image.RescaleSlope+obj.image.RescaleIntercept;
             %
             return;
         end
         %
         % MCNP input or output
         if (size(file,2)>2 && strcmp(file(end-1:end),'.o')==1) || ...
            (size(file,2)>8 && strcmp(file(end-3:end),'.inp')==1)
         %
            set(w_bar.hand,'Value',25);
            obj=import_mcinp(obj,[nfolder file]);
            set(w_bar.hand,'Value',75);
            obj.struct.contours.(sprintf('Item_%.0f',1)).Name='Edit';
            obj.struct.contours.(sprintf('Item_%.0f',1)).Mask=logical(zeros(obj.image.Nvoxels));
            return;
         %
         end
         %
         list=dir([nfolder '*.dcm']);  
         %
         % get ref pos if image instance number is missing
         R_pos = [];
         for i=1:length(list)
             if strcmp(list(i).name(1),'R')==0
               info = dicominfo([nfolder list(i).name]);
               if isempty(R_pos)==1
                   R_pos = info.ImagePositionPatient;
               elseif info.ImagePositionPatient(3) > R_pos(3)
                   R_pos = info.ImagePositionPatient;
               end
             end
         end
         %
         %
         % assume if list <= 3 (RS, RP, RD)
         % this section create a dummy image in case user whants to open a
         % RTDOSE and doesnot have a CT
         s = size(list,1);
         if s<=3
             for i=1:s
                in = dicominfo([nfolder list(i).name]);
                %
                if  isfield(in,'Modality') && strcmp(in.Modality,'CT')==1 % means the folder has an CT image so no need to create a dummy one
                    break;
                end
                %
                if  isfield(in,'Modality') && strcmp(in.Modality,'RTDOSE')==1
                    RD = list(i).name;
                    %
                    % Create dummy image to be used as reference
                    if isfield(in,'SliceThickness')==0 || isempty(in.SliceThickness)==1
                        obj.image.Resolution              = [in.PixelSpacing(1) in.PixelSpacing(2) diff(in.GridFrameOffsetVector(1:2))];
                    else
                        obj.image.Resolution              = [in.PixelSpacing(1) in.PixelSpacing(2) SliceThickness];
                    end
                    obj.image.ImageOrientationPatient     =  in.ImageOrientationPatient';
                    obj.image.ImagePositionPatient        =  in.ImagePositionPatient';
                    obj.image.Image                       =  uint16(zeros([in.Height in.Width in.NumberOfFrames]));
                    obj.image.RescaleSlope                =  1;
                    obj.image.RescaleIntercept            =  -1024; 
                    obj.image.Nvoxels                     =  [in.Height in.Width in.NumberOfFrames];
                    %
                end
             end
         end
         % finish creation of dummy image
         %
         % wait bar
         w_bar.files=size(list,1);  
         %
         obj=read_image_dcm(obj,nfolder,list,w_bar,R_pos);
         %
         if isfield(obj.image,'ImagePositionPatient01')==1
           obj.image.ImagePositionPatient=obj.image.ImagePositionPatient01(1:3)';
           d=obj.image.ImagePositionPatient01(3:4)-obj.image.ImagePositionPatient02(3:4);
           obj.image.Resolution(3)=abs(d(1)/d(2));
           obj.image.ImagePositionPatient01
           obj.image.ImagePositionPatient02
          if obj.image.ImagePositionPatient01(4)>obj.image.ImagePositionPatient02(4) 
             obj.image.Image=flip(obj.image.Image,3);
%             if isfield(obj.plan,'catheter')==1  && ...
%                     isfield(obj.plan.catheter.Cat_1,'Points') % flip catheter
%                 obj=flip_catheter(obj);
%                 %
%                 disp('fliped');
%                 %
%             end
          end
          obj.image=rmfield(obj.image,'ImagePositionPatient01');
          obj.image=rmfield(obj.image,'ImagePositionPatient02');
         end
         %
         % Converte to HU
         obj.image.Image=int16(obj.image.Image)/obj.image.RescaleSlope+obj.image.RescaleIntercept;
         %
         obj=create_contour_masks(obj);
         %
         if isfield(obj.plan,'catheter')==1
             ncat=fieldnames(obj.plan.catheter);
             for i=1:length(ncat)  
               if isfield(obj.plan.catheter.(sprintf('%s',ncat{i})),'Points')==1
                     obj.plan.catheter.(sprintf('%s',ncat{i})).PointsVox = bsxfun(@minus,  obj.plan.catheter.(sprintf('%s',ncat{i})).Points, ... 
                                                                                           obj.image.ImagePositionPatient);
                     obj.plan.catheter.(sprintf('%s',ncat{i})).PointsVox = bsxfun(@rdivide,obj.plan.catheter.(sprintf('%s',ncat{i})).PointsVox, ...
                                                                                           obj.image.Resolution);
                     obj.plan.catheter.(sprintf('%s',ncat{i})).PointsVox = obj.plan.catheter.(sprintf('%s',ncat{i})).PointsVox+1;
                     obj.plan.catheter.(sprintf('%s',ncat{i})).PointsEd  = obj.plan.catheter.(sprintf('%s',ncat{i})).PointsVox;                                                                  
                end
             end
         end
         % dwell voxels coordinates
         if isfield(obj.plan,'dwells')==1
             obj.plan.dwellVox        = bsxfun(@minus,  obj.plan.dwells(:,1:3), obj.image.ImagePositionPatient);
             obj.plan.dwellVox        = bsxfun(@rdivide,obj.plan.dwellVox,      obj.image.Resolution); 
             obj.plan.dwellVox        = obj.plan.dwellVox+1;
             obj.plan.dwellVox(:,4:5) = obj.plan.dwells(:,4:5);
         end
         %
         %
         if isempty(obj.dose)==0
             if isfield(obj.dose,'Dose_TPS_NI')==0 && isfield(obj.dose,'Dose_NI_BEAM_1')==1
                 obj.dose.Dose_TPS_NI = obj.dose.Dose_NI_BEAM_1;
             end
           obj=interpolatedose(obj);
         end
         set(w_bar.hand,'Visible',0); drawnow;
         %
         % Create struct to draw
         if isfield(obj.struct,'contours')==1
           id=length(fieldnames(obj.struct.contours))+1;
         else
           id=1;
         end
         obj.struct.contours.(sprintf('Item_%.0f',id)).Name='Edit';
         obj.struct.contours.(sprintf('Item_%.0f',id)).Mask=logical(zeros(obj.image.Nvoxels));
         %
      end
      %
    
      %
      function obj = flip_catheter(obj)
          for i=1:length(fieldnames(obj.plan.catheter))
              obj.plan.catheter.(sprintf('Cat_%.0f',i)).Points=flip(obj.plan.catheter.(sprintf('Cat_%.0f',i)).Points,1);
          end
      end
      %
      function obj = read_image_dcm(obj,nfolder,list,w_bar,R_pos) %#ok<*INUSD>
          % 
          try % only works if amb is open
              set(w_bar.hand,'Value',(100-(size(list,1)/w_bar.files*100))); drawnow; 
          catch
              %
          end
          %
          info_dcm=dicominfo([nfolder list(1).name]);
          % image, plan, structure or dose
          if     strcmp(list(1).name(1:2),'RS')==1 || strcmp(list(1).name(1:3),'RTS')==1 || (isfield(info_dcm,'Modality') && strcmp(info_dcm.Modality,'RTSTRUCT')==1)
                    RD = list(1).name;
          % 
            obj=read_struct_dcm(obj,[nfolder list(1).name]);
          elseif strcmp(list(1).name(1:2),'RP')==1 || strcmp(list(1).name(1:3),'RTP')==1 || (isfield(info_dcm,'Modality') && strcmp(info_dcm.Modality,'RTPLAN')==1)
          % 
            obj=read_plan_dcm(obj,[nfolder list(1).name]);
          elseif strcmp(list(1).name(1:2),'RD')==1 || strcmp(list(1).name(1:3),'RTD')==1 || (isfield(info_dcm,'Modality') && strcmp(info_dcm.Modality,'RTDOSE')==1)
          %
            obj=read_dose_dcm(obj,[nfolder list(1).name]);
          elseif strcmp(list(1).name(1:2),'RI')==1 || strcmp(list(1).name(1:3),'RTI')==1
              %
              % RT Image - missing function to read the data
              %
          else % image 
            if  isfield(obj.image,'Resolution')==0
              obj.image.Resolution(1:2)            = info_dcm.PixelSpacing;  
              if      isfield(info_dcm,'SliceThickness')==1     && isempty(info_dcm.SliceThickness)==0
                  obj.image.Resolution(3)         = info_dcm.SliceThickness;
              elseif  isfield(info_dcm,'SpacingBetweenSlices')==1   && isempty(info_dcm.SpacingBetweenSlices)==0
                  obj.image.Resolution(3)         = abs(info_dcm.SpacingBetweenSlices);
              else    
                  obj.image.Resolution(3)         = 0;  
              end
              %
              % Additional image information
              %
              if      isfield(info_dcm,'KVP')==1  
                obj.image.KVP                        = info_dcm.KVP;
              else
                obj.image.KVP                        = 0;  
              end
              %
              if      isfield(info_dcm,'ReconstructionDiameter')==1
                obj.image.ReconstructionDiameter     = info_dcm.ReconstructionDiameter;
              else
                obj.image.ReconstructionDiameter     = 0;    
              end
              %
              if      isfield(info_dcm,'XrayTubeCurrent')==1
                obj.image.XrayTubeCurrent            = info_dcm.XrayTubeCurrent;
              else
                obj.image.XrayTubeCurrent            = 0; 
              end
              %
              if      isfield(info_dcm,'FilterType')==1
                obj.image.FilterType                 = info_dcm.FilterType;
              else
                obj.image.FilterType                 = 0;  
              end
              % 
              if       isfield(info_dcm,'ConvolutionKernel')==1
                obj.image.ConvolutionKernel          = info_dcm.ConvolutionKernel;
              else
                obj.image.ConvolutionKernel          = 0;
              end
              %
              if       isfield(info_dcm,'PatientPosition')==1
                obj.image.PatientPosition            = info_dcm.PatientPosition; 
              else
                obj.image.PatientPosition            = 0;  
              end
              if isfield(info_dcm,'TableSpeed')==1   
                obj.image.TableSpeed                 = info_dcm.TableSpeed;
              else
                obj.image.TableSpeed                 = 0;  
              end
              %
              if isfield(info_dcm,'SpiralPitchFactor ')==1 
                obj.image.SpiralPitchFactor          = info_dcm.SpiralPitchFactor;
              else
                obj.image.SpiralPitchFactor          = 0;  
              end
              %
              if isfield(info_dcm,'SpacingBetweenSlices')==1
                  obj.image.SpacingBetweenSlices    = info_dcm.SpacingBetweenSlices;
              else
                  obj.image.SpacingBetweenSlices    = 0;
              end
              %
              % Intance number migth be missing - Issue with images created
              % using RayStation
               if isfield(info_dcm,'InstanceNumber')==0 || isempty(info_dcm.InstanceNumber)==1
                   info_dcm.InstanceNumber = round((+R_pos(3) - info_dcm.ImagePositionPatient(3))/info_dcm.SliceThickness)+1;
               end
              %
              %
              obj.image.Nvoxels(2)                = info_dcm.Width;
              obj.image.Nvoxels(1)                = info_dcm.Height;
              obj.image.RescaleIntercept          = info_dcm.RescaleIntercept;
              obj.image.RescaleSlope              = info_dcm.RescaleSlope;
              obj.image.ImagePositionPatient01    = info_dcm.ImagePositionPatient;    % upper and bottom references
              obj.image.ImagePositionPatient01(4) = info_dcm.InstanceNumber;
              obj.image.ImagePositionPatient02    = info_dcm.ImagePositionPatient;
              obj.image.ImagePositionPatient02(4) = info_dcm.InstanceNumber;
              obj.image.ImageOrientationPatient   = info_dcm.ImageOrientationPatient;
            end
            %
            % Intance number migth be missing - Issue with images created
            % using RayStation
            if isfield(info_dcm,'InstanceNumber') ==0 || isempty(info_dcm.InstanceNumber)==1 || info_dcm.InstanceNumber==0
                   info_dcm.InstanceNumber = round((R_pos(3) - info_dcm.ImagePositionPatient(3))/info_dcm.SliceThickness)+1;
            end
            % use instance number and slice position to find the slice
            % thickness if it was not provided before
            if      obj.image.Resolution(3)==0 && isfield(obj.image,'ref_pos_z')==0
                obj.image.ref_pos_z(1,1) = info_dcm.ImagePositionPatient(3);
                obj.image.ref_pos_z(1,2) = info_dcm.InstanceNumber;
            elseif  obj.image.Resolution(3)==0 && isfield(obj.image,'ref_pos_z')==1
                obj.image.ref_pos_z(2,1) = info_dcm.ImagePositionPatient(3);
                obj.image.ref_pos_z(2,2) = info_dcm.InstanceNumber;
                %
                Res_z                    = abs(diff(obj.image.ref_pos_z));
                obj.image.Resolution(3)  = Res_z(1)/Res_z(2);
            end
            
            % ***** 
            % Edit Sophie 25/06/2019: The Instance numbers of the CT slices
            % start from 0 --> the original version leaves out the first CT
            % slice. This edit was done after one CT set could not be
            % opened because the contour points ranged outside of the image
            % 
%             if info_dcm.InstanceNumber>=0
%               t=evalc('obj.image.Image(:,:,info_dcm.InstanceNumber+1)=dicomread([nfolder list(1).name])');
%             end

            % Original version before edit
            % read image
            if info_dcm.InstanceNumber>0
              t=evalc('obj.image.Image(:,:,info_dcm.InstanceNumber)=dicomread([nfolder list(1).name])');
            end
            
            % *****
            
            if obj.image.ImagePositionPatient01(3)  > info_dcm.ImagePositionPatient(3)
                obj.image.ImagePositionPatient01    = info_dcm.ImagePositionPatient;
                obj.image.ImagePositionPatient01(4) = info_dcm.InstanceNumber;
            end
            %
            if obj.image.ImagePositionPatient02(3)  < info_dcm.ImagePositionPatient(3)
                obj.image.ImagePositionPatient02    = info_dcm.ImagePositionPatient;
                obj.image.ImagePositionPatient02(4) = info_dcm.InstanceNumber;
            end
            %
          end
          list(1)=[];
          if isempty(list)==1
              obj.image.Nvoxels(3) = size(obj.image.Image,3);
              return; 
          end
          if exist('R_pos','var')==0
              R_pos=[];
          end
          obj=read_image_dcm(obj,nfolder,list,w_bar,R_pos);
      end
      
      
      %
       function obj = read_dose_dcm(obj,file)
         info_dcm=dicominfo(file);
         if isempty(info_dcm.Width)==1
             obj.dose=[];
             return;
         end
         obj.dose.Nvoxels(1)            = info_dcm.Width;
         obj.dose.Nvoxels(2)            = info_dcm.Height;
         obj.dose.Nvoxels(3)            = info_dcm.NumberOfFrames;
         obj.dose.DoseUnits             = info_dcm.DoseUnits;
         obj.dose.ImagePositionPatient  = info_dcm.ImagePositionPatient';
         obj.dose.DoseGridScaling       = info_dcm.DoseGridScaling;
         %
         %
         %
         if isfield(info_dcm,'DoseSummationType')==1 && strcmp(info_dcm.DoseSummationType,'BEAM')==1
             Beam = info_dcm.ReferencedRTPlanSequence.Item_1.ReferencedFractionGroupSequence.Item_1.ReferencedBeamSequence.Item_1.ReferencedBeamNumber;
             obj.dose.(sprintf('Dose_NI_BEAM_%d',Beam)) = single(squeeze(dicomread(file)))*obj.dose.DoseGridScaling;
         else
             obj.dose.Dose_TPS_NI                       = single(squeeze(dicomread(file)))*obj.dose.DoseGridScaling;
         end
         %
         obj.dose.Resolution(1:2)  = info_dcm.PixelSpacing;
         if isempty(info_dcm.SliceThickness)==1 || info_dcm.SliceThickness==0
             if isfield(info_dcm,'GridFrameOffsetVector')==1
                 obj.dose.Resolution(3)  = diff(info_dcm.GridFrameOffsetVector(1:2,1));
             else
                 obj.dose.Resolution(3)  = 0;  
             end
         else
           obj.dose.Resolution(3)  = info_dcm.SliceThickness;
         end
         %
         if obj.dose.Resolution(3)==0
            def_answer={num2str(obj.dose.Resolution(1))};
            %c
            answer=inputdlg('Missing dose slice tickness ... the value below is just a guess',...
                            'Slice tickness:',1,def_answer);
            %
            obj.dose.Resolution(3)=str2num(answer{1});
         end
         %
         % RayStation uses a different orientation
%          if strcmp(info_dcm.Manufacturer,'RaySearch Laboratories')==1
%          %
%           obj.dose.ImagePositionPatient(2) = obj.dose.ImagePositionPatient(2) + obj.dose.Resolution(2);
%          %
%          end
         %
         %
         if isfield(info_dcm,'TissueHeterogeneityCorrection')
            obj.dose.DoseType=info_dcm.TissueHeterogeneityCorrection;
         end
       end
       %
       function obj = interpolatedose(obj,file)
          si= round(obj.dose.Resolution./obj.image.Resolution.*size(obj.dose.Dose_TPS_NI));
%           D = imresize3(double(obj.dose.Dose_TPS_NI),[si(1) si(2) si(3)]);
%           % D = permute(D,[3 2 1]);
%           % shift
%           sh=round((obj.dose.ImagePositionPatient-obj.image.ImagePositionPatient)./obj.image.Resolution);
%           obj.dose.Dose_TPS=zeros(size(obj.image.Image));
%           di=size(D);
% %         %
%           obj.dose.Dose_TPS(sh(2)+1:sh(2)+di(1),sh(1)+1:sh(1)+di(2),sh(3)+1:sh(3)+di(3))=D;
% %         %
%           obj.dose.Dose_TPS    = single(obj.dose.Dose_TPS);
%           obj.dose.Dose_TPS_NI = double(obj.dose.Dose_TPS_NI);
%           %
          if isfield(obj.dose,'Dose_TPS_NI') == 1 && sum(obj.dose.Nvoxels == obj.image.Nvoxels)~=3
             %
              obj.image.Nvoxels = double(obj.image.Nvoxels);
              obj.dose.Nvoxels  = double(obj.dose.Nvoxels);
              %
              [Xq,Yq,Zq]    = meshgrid(obj.image.ImagePositionPatient(1):obj.image.Resolution(1):obj.image.ImagePositionPatient(1)+obj.image.Resolution(1)*(obj.image.Nvoxels(1)-1),...
                                       obj.image.ImagePositionPatient(2):obj.image.Resolution(2):obj.image.ImagePositionPatient(2)+obj.image.Resolution(2)*(obj.image.Nvoxels(2)-1), ...
                                       obj.image.ImagePositionPatient(3):obj.image.Resolution(3):obj.image.ImagePositionPatient(3)+obj.image.Resolution(3)*(obj.image.Nvoxels(3)-1)); % image grid
              %
              [X,Y,Z]       = meshgrid(obj.dose.ImagePositionPatient(1)-obj.image.Resolution(1)/2:obj.dose.Resolution(1):obj.dose.ImagePositionPatient(1)-obj.image.Resolution(1)/2+obj.dose.Resolution(1)*(obj.dose.Nvoxels(1)-1),...
                                       obj.dose.ImagePositionPatient(2)-obj.image.Resolution(2)/2:obj.dose.Resolution(2):obj.dose.ImagePositionPatient(2)-obj.image.Resolution(2)/2+obj.dose.Resolution(2)*(obj.dose.Nvoxels(2)-1), ...
                                       obj.dose.ImagePositionPatient(3)-obj.image.Resolution(3)/2:obj.dose.Resolution(3):obj.dose.ImagePositionPatient(3)-obj.image.Resolution(3)/2+obj.dose.Resolution(3)*(obj.dose.Nvoxels(3)-1)); % dose grid
              %
             %
             obj.dose.doses.Dose_TPS = interp3(X,Y,Z,obj.dose.Dose_TPS_NI,Xq,Yq,Zq,'linear');
             obj.dose.doses.Dose_TPS(isnan(obj.dose.doses.Dose_TPS))=0;
          else
             obj.dose.doses.Dose_TPS = obj.dose.Dose_TPS;
             obj.dose.doses.Dose_TPS(isnan(obj.dose.doses.Dose_TPS))=0; 
          end
          %
          a  = fieldnames(obj.dose);
          idx= find(contains(a,'Dose_NI_BEAM_'));
          if isempty(idx)==0
              for i=1:size(idx,1)
                  %
                  %
                  obj.image.Nvoxels = double(obj.image.Nvoxels);
                  obj.dose.Nvoxels  = double(obj.dose.Nvoxels);
                  %
                  [Xq,Yq,Zq]    = meshgrid(obj.image.ImagePositionPatient(1):obj.image.Resolution(1):obj.image.ImagePositionPatient(1)+obj.image.Resolution(1)*(obj.image.Nvoxels(1)-1),...
                                           obj.image.ImagePositionPatient(2):obj.image.Resolution(2):obj.image.ImagePositionPatient(2)+obj.image.Resolution(2)*(obj.image.Nvoxels(2)-1), ...
                                           obj.image.ImagePositionPatient(3):obj.image.Resolution(3):obj.image.ImagePositionPatient(3)+obj.image.Resolution(3)*(obj.image.Nvoxels(3)-1)); % image grid
                  %
                  [X,Y,Z]       = meshgrid(obj.dose.ImagePositionPatient(1)-obj.image.Resolution(1)/2:obj.dose.Resolution(1):obj.dose.ImagePositionPatient(1)-obj.image.Resolution(1)/2+obj.dose.Resolution(1)*(obj.dose.Nvoxels(1)-1),...
                                           obj.dose.ImagePositionPatient(2)-obj.image.Resolution(2)/2:obj.dose.Resolution(2):obj.dose.ImagePositionPatient(2)-obj.image.Resolution(2)/2+obj.dose.Resolution(2)*(obj.dose.Nvoxels(2)-1), ...
                                           obj.dose.ImagePositionPatient(3)-obj.image.Resolution(3)/2:obj.dose.Resolution(3):obj.dose.ImagePositionPatient(3)-obj.image.Resolution(3)/2+obj.dose.Resolution(3)*(obj.dose.Nvoxels(3)-1)); % dose grid
                  %
                  %
                  %
                  obj.dose.doses.(sprintf('%s',['BEAM_' a{idx(i)}(14:end)])) = interp3(X,Y,Z,obj.dose.(sprintf(a{idx(i)})),Xq,Yq,Zq,'linear');
                  obj.dose.doses.(sprintf('%s',['BEAM_' a{idx(i)}(14:end)]))(isnan(obj.dose.doses.(sprintf('%s',['BEAM_' a{idx(i)}(14:end)]))))=0;
              end
          end
          %
       end
       %
       function obj = read_struct_dcm(obj,file)
         info_dcm=dicominfo(file);
         if isfield(info_dcm,'StructureSetROISequence')==0; return; end
         %
         if isfield(info_dcm,'RTROIObservationsSequence')==1 
             cont=fieldnames(info_dcm.RTROIObservationsSequence);
             for i=1:size(cont,1)
                 %
                 if isfield(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})),'Private_3007_10xx_Creator') == 1 ...
                     || strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType,'ORGAN')==1 ...
                     || strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType,'CTV')==1 ...
                     || strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType,'EXTERNAL')==1 ... % nucletron
                     || strcmp(info_dcm.Manufacturer,'Nucletron')==1  % nucletron
                     %
                     obj=import_struct_nuc_var(obj,info_dcm,i,cont);
                     %
                 elseif strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType,'BRACHY_CHANNEL')==1
                     % varian
                     obj=import_cat_cont_varian(obj,info_dcm,i,cont);
                     %
                 else
                     info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType;  
                   % obj=inport_struct_catheter_varian(obj,info_dcm,i,cont); 
                     obj=import_struct_nuc_var(obj,info_dcm,i,cont);
                 end
                 %
             end
         else
             cont=fieldnames(info_dcm.ROIContourSequence);
             for i=1:size(cont,1)
                 %
                 if strcmp(info_dcm.ROIContourSequence.(sprintf('Item_%d',i)).ContourSequence.Item_1.ContourGeometricType,'CLOSED_PLANAR')==1  
                     %
                     obj=import_struct_nuc_var(obj,info_dcm,i,cont);
                 else
                     try
                       % varian
                        obj=import_cat_cont_varian(obj,info_dcm,i,cont);
                       %
                     catch
                         disp('Structure misidentified as a catheter is the RS file - Warning'); 
                     end
                 end
                 %
             end    
         end
         %
       end
       %
       function obj = import_cat_cont_varian(obj,info_dcm,i,cont)
        % if isfield(obj,'plan')==1 && isfield(obj.plan,'catheter')==1
              c_lis=fieldnames(obj.plan.catheter);
              for j=1:length(c_lis)
                if  obj.plan.catheter.(sprintf('%s',c_lis{j})).ReferencedROINumber == ...
                        info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ReferencedROINumber
                   %
                   obj.plan.catheter.(sprintf('%s',c_lis{j})).Points=reshape(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence.Item_1.ContourData,3,[])';
                   return;
                end
              end
%          else
%              disp('RP file might be missing ... Cathethers identified in the RS file, but there is no plan available')
%          end
       end
       %
       function obj=import_struct_nuc_var(obj,info_dcm,i,cont)
         if isfield(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)),'ContourSequence')==0
             return;
         end
         items=fieldnames(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence);
         %
         if isempty(obj.struct)==1
             con=1;
         else
             con=length(fieldnames(obj.struct.contours))+1;
         end
         %
         obj.struct.contours.(sprintf('Item_%.0f',con)).Name      = info_dcm.StructureSetROISequence.(sprintf('%s',cont{i})).ROIName;
         obj.struct.contours.(sprintf('Item_%.0f',con)).Algorithm = info_dcm.StructureSetROISequence.(sprintf('%s',cont{i})).ROIGenerationAlgorithm;  
         obj.struct.contours.(sprintf('Item_%.0f',con)).Points=[];
         for j=1:size(items)
             if j==1
                 obj.struct.contours.(sprintf('Item_%.0f',con)).Type = ...
                     info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence.(sprintf('%s',items{j})).ContourGeometricType;
             end
              POINTS=reshape(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence.(sprintf('%s',items{j})).ContourData,3,[])';
              obj.struct.contours.(sprintf('Item_%.0f',con)).Points(end+1,1:3)=[9999 9999 9999];
              obj.struct.contours.(sprintf('Item_%.0f',con)).Points(end+1:end+size(POINTS,1),:)=POINTS;
         end
             %
       end     
       %
       function obj = create_contour_masks(obj)
         if isfield(obj.struct,'contours')==0; return; end
         h = warndlg({'AMIGO will import all the contours and create masks',...
                      'To improve speed during the execution ',...
                      'It may take few minutes depending on the number of contours'},'Contours','replace');
         items=fieldnames(obj.struct.contours);
         for i=1:size(items,1) 
           obj.struct.contours.(sprintf('%s',items{i})).PointsIm=bsxfun(@minus,obj.struct.contours.(sprintf('%s',items{i})).Points, ...
                                                                                obj.image.ImagePositionPatient);
           obj.struct.contours.(sprintf('%s',items{i})).PointsIm=bsxfun(@rdivide,obj.struct.contours.(sprintf('%s',items{i})).PointsIm, ...
                                                                                obj.image.Resolution);   
           obj.struct.contours.(sprintf('%s',items{i})).Mask=zeros(obj.image.Nvoxels); 
           %
           [r,~]=find(obj.struct.contours.(sprintf('%s',items{i})).Points(:,3)==9999);
           obj.struct.contours.(sprintf('%s',items{i})).PointsIm(r,1)=9999;
           obj.struct.contours.(sprintf('%s',items{i})).PointsIm(r,2)=9999;
           obj.struct.contours.(sprintf('%s',items{i})).PointsIm(r,3)=9999;
           %
           %
           P=obj.struct.contours.(sprintf('%s',items{i})).PointsIm;
           for j=1:size(r,1)
             if j<size(r,1)  
              C_POINTS=[]; C_POINTS(:,1:3)=P(r(j)+1:r(j+1)-1,:); 
             elseif r(j)<size(P,1)
              C_POINTS=[]; C_POINTS(:,1:3)=P(r(j)+1:end,:);
             else 
              break   
             end
             mask=poly2mask(C_POINTS(:,1)+1,C_POINTS(:,2)+1,double(obj.image.Nvoxels(1)),double(obj.image.Nvoxels(2)));
             obj.struct.contours.(sprintf('%s',items{i})).Mask(:,:,round(C_POINTS(1,3))+1)=obj.struct.contours.(sprintf('%s',items{i})).Mask(:,:,round(C_POINTS(1,3))+1)+mask;
           end           
%            while isempty(P)==0
%                C_POINTS=[]; C_POINTS(:,1:3)=P(P(:,3)==P(1,3),:); P(P(:,3)==P(1,3),:)=[];
%                mask=poly2mask(C_POINTS(:,1)+1,C_POINTS(:,2)+1,double(obj.image.Nvoxels(1)),double(obj.image.Nvoxels(2)));
%                obj.struct.contours.(sprintf('%s',items{i})).Mask(:,:,round(C_POINTS(1,3))+1)=mask;
%            end
            obj.struct.contours.(sprintf('%s',items{i})).Mask=logical(obj.struct.contours.(sprintf('%s',items{i})).Mask);
%            %
           % h = warndlg(['Name: ' obj.struct.contours.(sprintf('%s',items{i})).Name '  Contour ' num2str(i) ' of ' num2str(size(items,1))],'Contours','replace');
           %
         end
         %
         if isvalid(h)==1; close(h); end
         %
       end    
       %
       %
       function obj = read_plan_dcm(obj,file)
         %
         info_dcm=dicominfo(file);
         if isfield(info_dcm,'StationName')
           obj.plan.StationName             =  info_dcm.StationName;
         end
         if isfield(info_dcm,'BrachyTreatmentTechnique')
           obj.plan.BrachyTreatmentTechnique  =  info_dcm.BrachyTreatmentTechnique;  
         end
         if isfield(info_dcm,'SourceSequence')
           obj.plan.source                    =  info_dcm.SourceSequence.Item_1; 
           obj.plan.TotalAirKerma             =  info_dcm.SourceSequence.Item_1.ReferenceAirKermaRate;
         end
         if isfield(info_dcm,'TreatmentMachineSequence')
           obj.plan.machine                   =  info_dcm.TreatmentMachineSequence.Item_1;
         end
         if isfield(info_dcm,'ApplicationSetupSequence')
           obj.plan.NCat                      =  size(fieldnames(info_dcm.ApplicationSetupSequence.Item_1.ChannelSequence),1);
         end
         %
         if isfield(info_dcm,'FractionGroupSequence') ==1 && ...
            isfield(info_dcm.FractionGroupSequence.Item_1,'ReferencedBrachyApplicationSetupSequence')==1 && ...
            isfield(info_dcm.FractionGroupSequence.Item_1.ReferencedBrachyApplicationSetupSequence.Item_1,'BrachyApplicationSetupDose')==1
            obj.plan.presc_dose             =  info_dcm.FractionGroupSequence.Item_1.ReferencedBrachyApplicationSetupSequence.Item_1.BrachyApplicationSetupDose;
         else
            obj.plan.presc_dose             =  0;
         end
         %
         if isfield(info_dcm,'ApplicationSetupSequence')==1
            obj=get_dwell_pos(obj,info_dcm.ApplicationSetupSequence.Item_1.ChannelSequence);
         end
         %
         if isfield(info_dcm,'Private_300f_1000')==1                % nucletron
            obj=get_cat_pos(obj,info_dcm);
         end
         %
         %% import linac plan
         if isfield(info_dcm,'BeamSequence')==1
                %
                %
                % import patient model
                load('model_pat.mat');
                obj.plan.pat_model.face=face;
                obj.plan.pat_model.vertex=vertex;
                %
                %
                EB_plan=dicominfo(file);
                % Number of beam
                %
                obj.plan.NBeams  =  size(fieldnames(EB_plan.BeamSequence),1);
                cont=1;
                for i=1:obj.plan.NBeams
                 %
                 obj.plan.BEAM_info{cont}=['Beam ' num2str(i)]; 
                 cont=cont+1;
                 %
                 obj.plan.(sprintf('Beam_%d',i)).BeamNumber                    =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamNumber;
                 obj.plan.(sprintf('Beam_%d',i)).BeamName                      =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamName;
                 %
                 obj.plan.(sprintf('Beam_%d',i)).BeamType                      =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamType;
                 obj.plan.(sprintf('Beam_%d',i)).NumberOfControlPoints         =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).NumberOfControlPoints;
                 %
                 obj.plan.(sprintf('Beam_%d',i)).ControlPointSequence          =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence;
                 %
                 obj.plan.(sprintf('Beam_%d',i)).SourceAxisDistance            =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).SourceAxisDistance;
                 %
                 if isfield(EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamLimitingDeviceSequence,'Item_3')==1
                    obj.plan.(sprintf('Beam_%d',i)).LeafPositionBoundaries     =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamLimitingDeviceSequence.Item_3.LeafPositionBoundaries;
                 end
                 %
                 obj.plan.(sprintf('Beam_%d',i)).PatientPosition               =  EB_plan.PatientSetupSequence.(sprintf('Item_%d',i)).PatientPosition;
                 obj.plan.(sprintf('Beam_%d',i)).SetupTechnique                =  EB_plan.PatientSetupSequence.(sprintf('Item_%d',i)).SetupTechnique;
                 %
                 for j=1:obj.plan.(sprintf('Beam_%d',i)).NumberOfControlPoints
                      %
                      obj.plan.BEAM_info{cont}=['  Control Point   ' num2str(j)]; cont=cont+1;
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).Jaw01   = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDevicePositionSequence.Item_1.LeafJawPositions;
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).Jaw03   = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDevicePositionSequence.Item_2.LeafJawPositions;
                      %
                      if isfield(EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDevicePositionSequence,'Item_3')==1
                        obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).MLC   = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDevicePositionSequence.Item_3.LeafJawPositions;
                      end
                      %
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).NominalBeamEnergy          = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.NominalBeamEnergy;
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).DoseRateSet                = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.DoseRateSet;
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).GantryAngle                = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.GantryAngle;
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).GantryRotationDirection    = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.GantryRotationDirection;
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).CollimatorAngle            = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDeviceAngle;
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).Isocenter                  = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.IsocenterPosition';
                      %
                      % Table
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).TableTopEccentricAngle       = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.TableTopEccentricAngle;
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).TableTopVerticalPosition     = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.TableTopVerticalPosition;
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).TableTopLongitudinalPosition = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.TableTopLongitudinalPosition;
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).TableTopLateralPosition      = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.TableTopLateralPosition;
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).TableTopPitchAngle           = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.TableTopPitchAngle;
                      obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).TableTopRollAngle            = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.TableTopRollAngle;
                      %
                      if isfield(EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1,'SourceToSurfaceDistance' )==1
                        obj.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).SourceToSurfaceDistance  = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.SourceToSurfaceDistance;
                      end
                      %
                 end
                end
                %
                obj.plan.isocenter = [0,0,0];
               % caculate MCNP TR cards
               for k=1:obj.plan.NBeams
               % check each control point
                 for j=1:obj.plan.(sprintf('Beam_%d',k)).NumberOfControlPoints
                     JAW_X      = obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).Jaw01;
                     JAW_Y      = obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).Jaw03;
                     Gantry     = obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).GantryAngle;
                     Collimator = obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).CollimatorAngle;
                     %
                     % TRCL = o1 o2 o3 xx' yx' zx' xy' yy' zy' xz' yz' zz'
                     %
                     % LINAC HEAD TR1 - Accounts for gantry 
                     TR_1(1:3)   = [0,                 0,             -100]; % reference corrdinate for rotation - isocenter  
                     TR_1(4:6)   = [Gantry,           90,        90+Gantry];
                     TR_1(7:9)   = [90,                0,               90];
                     TR_1(10:12) = [90-Gantry,        90,           Gantry];
                     TR_1(13)    = -1;
                     %
                     % Collimator
                     TR_2(1:3)   = [0,                         0,    0];    % reference corrdinate for rotation - isocenter  
                     TR_2(4:6)   = [Collimator,    90-Collimator,   90];
                     TR_2(7:9)   = [90+Collimator,    Collimator,   90];
                     TR_2(10:12) = [90,                       90,    0];
                     TR_2(13)    = -1;
                     % JAW X  - TR2 and TR3
                     JAW_X =atand(JAW_X/1000);                              % mm  - converting to deg to find rotation
                     % Positive side
                     TR_3(1:3)   = [0,                   0,         36.88]; % reference corrdinate for rotation - isocenter  
                     TR_3(4:6)   = [JAW_X(2),           90,   90-JAW_X(2)];
                     TR_3(7:9)   = [90,                  0,            90];
                     TR_3(10:12) = [90+JAW_X(2),        90,      JAW_X(2)];
                     TR_3(13)    = -1;
                     % Negative side
                     TR_4(1:3)   = [0,                   0,         36.88]; % reference corrdinate for rotation - isocenter  
                     TR_4(4:6)   = [JAW_X(1),           90,   90-JAW_X(1)];
                     TR_4(7:9)   = [90,                  0,            90];
                     TR_4(10:12) = [90+JAW_X(1),        90,      JAW_X(1)];
                     TR_4(13)    = -1;
                     % JAW Y  - TR4 and TR5
                     JAW_Y       = atand(JAW_Y/1000);                       % mm  - converting to deg to find rotation
                     % Positive side
                     TR_5(1:3)   = [0,             0,         28.16]; % reference corrdinate for rotation - isocenter  
                     TR_5(4:6)   = [0,            90,         90.00];
                     TR_5(7:9)   = [90,     JAW_Y(2),   90-JAW_Y(2)];
                     TR_5(10:12) = [90,  90+JAW_Y(2),      JAW_Y(2)];
                     TR_5(13)    = -1;
                     % Negative side
                     TR_6(1:3)   = [0,             0,         28.16]; % reference corrdinate for rotation - isocenter  
                     TR_6(4:6)   = [0,            90,         90.00];
                     TR_6(7:9)   = [90,     JAW_Y(1),   90-JAW_Y(1)];
                     TR_6(10:12) = [90,  90+JAW_Y(1),      JAW_Y(1)];
                     TR_6(13)    = -1;
                     %
                     obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_1=TR_1;
                     obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_2=TR_2;
                     obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_3=TR_3;
                     obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_4=TR_4;
                     obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_5=TR_5;
                     obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_6=TR_6;
                     %
                     % check if there is a MLC field ...
                     if isfield(obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)),'MLC')==1
                         MLC = obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).MLC;
                         for i=1:size(MLC,1)
                         end
                     end   
                     %
                     % SOURCE TR 7
                     TR_7(1:3)   = [0,                 0,            -46.3]; % reference corrdinate for rotation - isocenter  
                     TR_7(4:6)   = [Gantry,           90,        90+Gantry];
                     TR_7(7:9)   = [90,                0,               90];
                     TR_7(10:12) = [90-Gantry,        90,           Gantry];
                     TR_7(13)    = -1;
                     %
                     obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_7 = TR_7;
                     %
                     obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_8(1:3)  = obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).Isocenter([1 3 2]);
                     % different orientation for X
                     obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_8(1)    =-obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_8(1);
                 end
               end
               %
         end
         %
         if isfield(info_dcm,'Manufacturer')
           obj.plan.Manufacturer              =  info_dcm.Manufacturer;
           % Proton plan ?
          if strcmp(obj.plan.Manufacturer,'RaySearch Laboratories')==1
             obj.plan            = read_mevion_rt_plan(info_dcm,1);
             obj.plan.modality   = 'Protons';
             obj                 = combine_plan(obj);
             obj.plan.pat_model  = struct;
             %
             if isfield(info_dcm,'DoseReferenceSequence')==1
               obj.plan.presc_dose = info_dcm.DoseReferenceSequence.Item_1.TargetPrescriptionDose;
             end
             %
             if isfield(info_dcm,'FractionGroupSequence')==1
               obj.plan.fractions = info_dcm.FractionGroupSequence.Item_1.NumberOfFractionsPlanned;
             end
             %
          end
          %
         end
         %
       end
     
       
       %
       function obj = get_dwell_pos(obj,channels)
           ch=fieldnames(channels);
           obj.plan.dwells=[];
           for i=1:size(ch,1)
               ch_number=channels.(sprintf('%s',ch{i})).ChannelNumber;
               obj.plan.catheter.(sprintf('Cat_%.0f',ch_number)).ChannelLength               = channels.(sprintf('%s',ch{i})).ChannelLength;
             if isfield(channels.(sprintf('%s',ch{i})),'FinalCumulativeTimeWeight')==1
               obj.plan.catheter.(sprintf('Cat_%.0f',ch_number)).FinalCumulativeTimeWeight   = channels.(sprintf('%s',ch{i})).FinalCumulativeTimeWeight;
             else
               obj.plan.catheter.(sprintf('Cat_%.0f',ch_number)).FinalCumulativeTimeWeight   = 1;  
             end
               obj.plan.catheter.(sprintf('Cat_%.0f',ch_number)).ChannelTotalTime            = channels.(sprintf('%s',ch{i})).ChannelTotalTime;
               obj.plan.catheter.(sprintf('Cat_%.0f',ch_number)).SourceApplicatorStepSize    = channels.(sprintf('%s',ch{i})).SourceApplicatorStepSize;
               obj.plan.catheter.(sprintf('Cat_%.0f',ch_number)).SourceApplicatorLength      = channels.(sprintf('%s',ch{i})).SourceApplicatorLength;
               obj.plan.catheter.(sprintf('Cat_%.0f',ch_number)).ChannelLength               = channels.(sprintf('%s',ch{i})).ChannelLength;
               obj.plan.catheter.(sprintf('Cat_%.0f',ch_number)).ReferencedROINumber         = channels.(sprintf('%s',ch{i})).ReferencedROINumber;
               %
               %   
               try   % Nucletron uses a time weight 
                 ref_time=channels.(sprintf('%s',ch{i})).ChannelTotalTime/channels.(sprintf('%s',ch{i})).FinalCumulativeTimeWeight;
               catch % Varian does not
                 ref_time=1;  
               end
               %
               cum_time=0;
               for j=1:size(fieldnames(channels.(sprintf('%s',ch{i})).BrachyControlPointSequence),1)
                   if cum_time<channels.(sprintf('%s',ch{i})).BrachyControlPointSequence.(sprintf('Item_%.0f',j)).CumulativeTimeWeight
                     %
                     obj.plan.dwells(end+1,1:3) =  channels.(sprintf('%s',ch{i})).BrachyControlPointSequence.(sprintf('Item_%.0f',j)).ControlPoint3DPosition;
                     obj.plan.dwells(end,4)     =  (channels.(sprintf('%s',ch{i})).BrachyControlPointSequence.(sprintf('Item_%.0f',j)).CumulativeTimeWeight - cum_time)*ref_time;
                     obj.plan.dwells(end,5)     =  i;
                     cum_time                   =  channels.(sprintf('%s',ch{i})).BrachyControlPointSequence.(sprintf('Item_%.0f',j)).CumulativeTimeWeight;
                   end
               end
           %
           end
       end
       %
       %
       function obj = get_cat_pos(obj,info_dcm)
           cat_inf=info_dcm.Private_300f_1000.Item_1.RTROIObservationsSequence;
           cat_pos=info_dcm.Private_300f_1000.Item_1.ROIContourSequence;
           cat_names=fieldnames(cat_pos);
           cont=1;
           for i=1:length(cat_names)
               if strcmp(cat_inf.(sprintf('Item_%d',i)).RTROIInterpretedType,'BRACHY_CHANNEL')==1
                 obj.plan.catheter.(sprintf('Cat_%d',cont)).Points=reshape(cat_pos.(sprintf('%s',cat_names{i})).ContourSequence.Item_1.ContourData,3,[])';
                 cont=cont+1;
               end
           end
       end
       %
       %
        function obj = import_egsphant(obj,filename)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% EGSPhantFile - check
            % verify the number of materials
            fid=fopen(filename);
            Mat = fscanf(fid, '%d', [1 1]);
            % get material list
            for i=1:Mat+1
               Mat_names{i,1}=fgetl(fid); %#ok<*NASGU>
            end
            % ignore the next lines (1 values)
            fgetl(fid);
            %
            N_voxel=str2num(fgetl(fid));
            %
            def_pos = ftell(fid);
            %
            % there are two types of Egsphant  (with and without space between the material numbers) 
            %
            if   isempty(fgetl(fid))==1 % egsphant withoput space
                Eg_type=0;
            else
                %
                fseek(fid,def_pos,'bof');
                Eg_type=1;
                %
            end
            while size(N_voxel,2)~=3
                N_voxel=str2num(fgetl(fid));
            end

            % Chenge reference to AMB_Format
            obj.image.Nvoxels(1:3)=[N_voxel(2) N_voxel(1) N_voxel(3)]; 
            %
            % get the voxel resolution
            R = fscanf(fid, '%f %f', [1 2]); fgetl(fid);
            obj.image.Resolution(1)=(R(1,2)-R(1,1))*10;     % mm
            %
            obj.image.ImagePositionPatient(1)=R(1,1)*10;  
            %
            R = fscanf(fid, '%f %f', [1 2]);fgetl(fid);
            obj.image.Resolution(2)=(R(1,2)-R(1,1))*10;     % mm
            obj.image.ImagePositionPatient(2)=R(1,1)*10;
            %
            R = fscanf(fid, '%f %f', [1 2]);fgetl(fid);
            obj.image.ImagePositionPatient(3)=R(1,1)*10;  
            obj.image.Resolution(3)=(R(1,2)-R(1,1))*10;     % mm
            %
          obj.plan.Mat_HU=zeros(obj.image.Nvoxels);  
          for k=1:N_voxel(3)
            for i=1:N_voxel(2)
             a=fgetl(fid);
             if isinf(str2num(a))==1 || Eg_type==0
               obj.plan.Mat_HU(i,:,k)=str2num(a(:));
             else
               obj.plan.Mat_HU(i,:,k)=str2num(a);  
             end  
            end
            fgetl(fid);
          end
          %
          %
          obj.plan.Density=zeros(obj.image.Nvoxels); 
          for k=1:N_voxel(3)
            for i=1:N_voxel(2)   
             obj.plan.Density(i,:,k)=str2num(fgetl(fid));
            end
            fgetl(fid);
          end
          fclose('all');
          obj.plan.Mat_HU  = flip(obj.plan.Mat_HU,1);
          obj.plan.Density = flip(obj.plan.Density,1);
          obj.image.Image  = obj.plan.Mat_HU;
        end
      %%
        function   obj = import_bin(obj,fname)
            fid = fopen(fname,'r');
            s   = dir(fname);
            %
            obj.image.Nvoxels(1:2) = [256 256];
            obj.image.Nvoxels(3)   = (s.bytes/(256*256*4)*4)/4;
            %
            prompt      = {'Enter matrix size (separeted with spaces):','Pixel Slice (cm):'};
            dlg_title   = 'Input';
            num_lines   = 1;
            defaultans  = {['256 256 ' num2str(obj.image.Nvoxels(3))],'0.0145 0.0145'};
            answer      = inputdlg(prompt,dlg_title,num_lines,defaultans);
            %
            obj.image.Nvoxels(1:3)         = str2num(answer{1});
            %
            Res = str2num(answer{2});
            %
            obj.image.Resolution(1)        = Res(1);
            obj.image.Resolution(2)        = Res(1);
            obj.image.Resolution(3)        = Res(2);
            obj.image.ImagePositionPatient = [0,0,0];
            %
            obj.image.Image                = zeros(obj.image.Nvoxels);
            for i=1:obj.image.Nvoxels(3)
               obj.image.Image(:,:,i)  = fread(fid,[256 256],'*real*4','l');
            end
            fclose(fid);
        end
      %% 
       function   obj = import_mcinp(obj,fname)
            fid = fopen(fname,'r');
            while ~feof(fid)
                a=fgetl(fid);
                % find image size
                if contains(a,'fill ')==1
                    a(1:strfind(a,'fill ')+4) = [];
                    a                         = strrep(a,':',' ');
                    dim                       = str2num(a(1:end-1));
                    obj.image.Nvoxels(1)      = dim(2)-dim(1)+1;
                    obj.image.Nvoxels(2)      = dim(4)-dim(3)+1;
                    obj.image.Nvoxels(3)      = dim(6)-dim(5)+1;
                    % Create variables
                    MCNP.Uni                  = zeros(obj.image.Nvoxels);
                    obj.plan.Mat_HU           = zeros(obj.image.Nvoxels);
                    obj.plan.Density          = zeros(obj.image.Nvoxels);
                    % read all voxels
                    cont=1; % voxel ID
                    while cont<=obj.image.Nvoxels(1)*obj.image.Nvoxels(2)*obj.image.Nvoxels(3)
                        a=fgetl(fid); a=strsplit(a);
                        for i=1:size(a,2)
                            if isempty(a{i})==0 && strcmp(a{i}(end),'r')==0 && strcmp(a{i}(end),'&')==0 ...
                                    && (strcmp(a{i}(end),'c')==0 || strcmp(a{i}(end),'&')==0) % new uni
                                uni=str2num(a{i});
                                MCNP.Uni(cont)=uni;
                                cont=cont+1;
                            elseif isempty(a{i})==0 && strcmp(a{i}(end),'r')==1  ...
                                    && (strcmp(a{i}(end),'c')==0 || strcmp(a{i}(end),'&')==0)                           % repetition
                                r = str2num(a{i}(1:end-1));
                                MCNP.Uni(cont:cont+r-1)=uni;
                                cont=cont+r;
                            end       
                        end
                    end
                    %
                    fgetl(fid);fgetl(fid);
                    % read universes - mat and dens
                    a=fgetl(fid); a=strsplit(a);
                    while isempty(a{1})==1
                       uni=str2num(a{11}(3:end));
                       mat=str2num(a{3});
                       den=abs(str2num(a{4}));
                       obj.plan.Mat_HU(MCNP.Uni==uni) =mat;
                       obj.plan.Density(MCNP.Uni==uni)=den;
                       a=fgetl(fid); a=strsplit(a);
                    end
                end
                %
                if contains(a,'c  Lattice')==1
                  for i=1:3
                    a = fgetl(fid); b(1) = str2num(a(11:end));
                    a = fgetl(fid); b(2) = str2num(a(11:end));
                    %
                    obj.image.ImagePositionPatient(i) = b(1);
                    obj.image.Resolution(i)           = b(2)-b(1);
                    %
                  end
                  break;
                end
                
                %
            end
            %
            %
            obj.image.Image = obj.plan.Mat_HU;
            %
            fclose(fid);
        end
      
      %%
        function obj = import_g4dcm(obj,folder)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% G4dcm isabel's code
        fi=dir([folder filesep '*.g4dcm']);
        %
        for sli=1:size(fi,1)
          % verify the number of materials
          fid=fopen([folder filesep fi(sli).name]);
          if sli==1
            Mat = fscanf(fid, '%d', [1 1]);
            % get material list
            fgetl(fid);
            for i=1:Mat
               Mat_names{i,1}=fgetl(fid); %#ok<*NASGU>
            end
            %
            N_voxel=str2num(fgetl(fid));
            while size(N_voxel,2)~=3
                N_voxel=str2num(fgetl(fid));
            end
            %
            N_voxel(3)=size(fi,1);
            %
            % Chenge reference to AMB_Format
            obj.image.Nvoxels(1:3)=[N_voxel(2) N_voxel(1) N_voxel(3)]; 
            %
            % get the voxel resolution
            R = fscanf(fid, '%f %f', [1 2]); fgetl(fid);
            obj.image.Resolution(1)=diff(R)/N_voxel(1);     % mm
            %
            obj.image.ImagePositionPatient(1)=R(1,1);  
            %
            R = fscanf(fid, '%f %f', [1 2]);fgetl(fid);
            obj.image.Resolution(2)=diff(R)/N_voxel(2);     % mm
            obj.image.ImagePositionPatient(2)=R(1,1);
            %
            R = fscanf(fid, '%f %f', [1 2]);fgetl(fid);
            obj.image.Resolution(3)=diff(R);     % mm
            obj.image.ImagePositionPatient(3)=R(1,1);  
            %
            Im=zeros(N_voxel(1)*2,N_voxel(1),N_voxel(3));
          else
            for i=1:Mat+5
                fgetl(fid);
            end
          end
          Im(:,:,sli)=cell2mat(textscan(fid,repmat('%f',[1,N_voxel(2)]),'CollectOutput',1));
          fclose('all');
        end
         % obj.plan.Mat_HU  = flip(obj.plan.Mat_HU,1);
         % obj.plan.Density = flip(obj.plan.Density,1);
          obj.plan.Mat_HU  = Im(1:size(Im,1)/2,:,:);
          obj.plan.Density = Im(size(Im,1)/2+1:end,:,:);
          obj.image.Image  = obj.plan.Mat_HU;
          clear Im;
        end
      %%
      %
   end
end
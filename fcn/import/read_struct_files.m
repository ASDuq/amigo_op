function obj = read_struct_files(obj,Files)
%   Check how many studies and series 
        PT      = unique(Files(:,2));         % number of patients
        %
        %% ---------------------------------------------------------------------------------------------------------------
        % Read struct files
        for k =1:size(PT,1) % 
            % 
            % get only files related within the same study
            IDX = find(ismember(Files(:,2),PT{k})); 
            st  = Files(IDX,:);
            % add additional "for" in case of multiple series
            % SE  = unique(cell2mat(Files(:,5))); % number of series per study
            %
            % Check modality and open STRUCT images
            IDX = find(ismember(st(:,3),'RTSTRUCT'));
            %
            if isempty(IDX)==1; continue; end 
            %  
            stru = obj.Studies.(sprintf(['P' PT{k}]));
            stru.struct = [];
            %
            for i = 1:length(IDX)
               stru = read_struct_dcm(stru,st{IDX,36});
               obj.Studies.(sprintf(['P' PT{k}])) = create_contour_masks(stru);
            end
        end
        % 
end


         
% ------------------------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------------------------
function rts = read_struct_dcm(rts,info_dcm)
 % are there structures ? 
 if isfield(info_dcm,'StructureSetROISequence')==0; warndlg('No contours available in the RTStruct file'); return; end
 %
 if isfield(info_dcm,'RTROIObservationsSequence')==1 
     cont=fieldnames(info_dcm.RTROIObservationsSequence);
     for i=1:size(cont,1)
         %
         if isfield(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})),'Private_3007_10xx_Creator') == 1 ...
             || strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType,'ORGAN')==1 ...
             || strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType,'CTV')==1 ...
             || strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType,'EXTERNAL')==1 ... % nucletron
             || strcmp(info_dcm.Manufacturer,'Nucletron')==1  % nucletron
             %
             rts=import_struct_nuc_var(rts,info_dcm,i,cont);
             %
         elseif strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType,'BRACHY_CHANNEL')==1
             % varian
             rts=import_cat_cont_varian(rts,info_dcm,i);
             %
         else
             info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType;  
             rts=import_struct_nuc_var(rts,info_dcm,i,cont);
         end
         %
     end
 else
     cont=fieldnames(info_dcm.ROIContourSequence);
     for i=1:size(cont,1)
         %
         if strcmp(info_dcm.ROIContourSequence.(sprintf('Item_%d',i)).ContourSequence.Item_1.ContourGeometricType,'CLOSED_PLANAR')==1  
             %
             rts=import_struct_nuc_var(rts,info_dcm,i,cont);
         else
             try
               % varian
                rts=import_cat_cont_varian(rts,info_dcm,i);
               %
             catch
                 disp('Structure misidentified as a catheter is the RS file - Warning'); 
             end
         end
         %
     end    
 end
 %
 %
end
%
function rts = import_cat_cont_varian(rts,info_dcm,i)
% if isfield(obj,'plan')==1 && isfield(obj.plan,'catheter')==1
      c_lis=fieldnames(rts.plan.catheter);
      for j=1:length(c_lis)
        if  rts.plan.catheter.(sprintf('%s',c_lis{j})).ReferencedROINumber == ...
                info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ReferencedROINumber
           %
           rts.plan.catheter.(sprintf('%s',c_lis{j})).Points=reshape(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence.Item_1.ContourData,3,[])';
           return;
        end
      end
end
%
function rts=import_struct_nuc_var(rts,info_dcm,i,cont)
 if isfield(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)),'ContourSequence')==0
     return;
 end
 items=fieldnames(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence);
 %
 if isempty(rts.struct)==1
     con=1;
 else
     con=length(fieldnames(rts.struct.contours))+1;
 end
 %
 rts.struct.contours.(sprintf('Item_%.0f',con)).Name      = info_dcm.StructureSetROISequence.(sprintf('%s',cont{i})).ROIName;
 rts.struct.contours.(sprintf('Item_%.0f',con)).Algorithm = info_dcm.StructureSetROISequence.(sprintf('%s',cont{i})).ROIGenerationAlgorithm;  
 rts.struct.contours.(sprintf('Item_%.0f',con)).Points=[];
 for j=1:size(items)
     if j==1
         rts.struct.contours.(sprintf('Item_%.0f',con)).Type = ...
             info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence.(sprintf('%s',items{j})).ContourGeometricType;
     end
      POINTS=reshape(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence.(sprintf('%s',items{j})).ContourData,3,[])';
      rts.struct.contours.(sprintf('Item_%.0f',con)).Points(end+1,1:3)=[9999 9999 9999];
      rts.struct.contours.(sprintf('Item_%.0f',con)).Points(end+1:end+size(POINTS,1),:)=POINTS;
 end
     %
end     
%
function rts = create_contour_masks(rts)
 if isfield(rts.struct,'contours')==0; return; end
 h = warndlg({'AMIGO will import all the contours and create masks',...
              'To improve speed during the execution ',...
              'It may take few minutes depending on the number of contours'},'Contours','replace');
 items=fieldnames(rts.struct.contours);
 for i=1:size(items,1) 
   rts.struct.contours.(sprintf('%s',items{i})).PointsIm=bsxfun(@minus,rts.struct.contours.(sprintf('%s',items{i})).Points, ...
                                                                        rts.image.ImagePositionPatient);
   rts.struct.contours.(sprintf('%s',items{i})).PointsIm=bsxfun(@rdivide,rts.struct.contours.(sprintf('%s',items{i})).PointsIm, ...
                                                                        rts.image.Resolution);   
   rts.struct.contours.(sprintf('%s',items{i})).Mask=zeros(rts.image.Nvoxels); 
   %
   [r,~]=find(rts.struct.contours.(sprintf('%s',items{i})).Points(:,3)==9999);
   rts.struct.contours.(sprintf('%s',items{i})).PointsIm(r,1)=9999;
   rts.struct.contours.(sprintf('%s',items{i})).PointsIm(r,2)=9999;
   rts.struct.contours.(sprintf('%s',items{i})).PointsIm(r,3)=9999;
   %
   %
   P=rts.struct.contours.(sprintf('%s',items{i})).PointsIm;
   for j=1:size(r,1)
     if j<size(r,1)  
      C_POINTS=[]; C_POINTS(:,1:3)=P(r(j)+1:r(j+1)-1,:); 
     elseif r(j)<size(P,1)
      C_POINTS=[]; C_POINTS(:,1:3)=P(r(j)+1:end,:);
     else 
      break   
     end
     mask=poly2mask(C_POINTS(:,1)+1,C_POINTS(:,2)+1,double(rts.image.Nvoxels(1)),double(rts.image.Nvoxels(2))); 
     rts.struct.contours.(sprintf('%s',items{i})).Mask(:,:,round(C_POINTS(1,3))+1)=rts.struct.contours.(sprintf('%s',items{i})).Mask(:,:,round(C_POINTS(1,3))+1)+mask;
   end           
    rts.struct.contours.(sprintf('%s',items{i})).Mask=logical(rts.struct.contours.(sprintf('%s',items{i})).Mask);
 end
 %
 if isvalid(h)==1; close(h); end
 %
end
function obj = read_dose_files(obj,Files)
%   Check how many studies and series 
PT  = unique(Files(:,2));         % number of patients
%
% If thre are multiples doses but only one study ... adjust the id and load
% all the doses
if size(PT,1)>1
    for k =1:size(PT,1) % 
       IDX = find(ismember(Files(:,2),PT{k})); 
       if length(IDX) > 4 % most likely CT image ID
           Ref_ID = Files{IDX(1),2};
       end
    end
    for k =1:size(PT,1) % 
       IDX = find(ismember(Files(:,2),PT{k})); 
       if length(IDX) == 1 &&  strcmpi(Files{IDX(1),3},'RTDOSE')==1    % most likely CT image ID
           Files{IDX(1),2}=Ref_ID;
       end
    end
    PT  = unique(Files(:,2)); 
end
%
for k =1:size(PT,1) % 
    % 
    % get only files related within the same study
    IDX = find(ismember(Files(:,2),PT{k})); 
    st  = Files(IDX,:);
    %
    %
    % add additional "for" in case of multiple series
    % SE  = unique(cell2mat(Files(:,5))); % number of series per study
    %
    % Check modality and open STRUCT images
    IDX = find(ismember(st(:,3),'RTDOSE'));
    %
    if isempty(IDX)==1; continue; end 
    %  
    if isfield(obj.Studies,['P' PT{k}])==1
       stru      = obj.Studies.(sprintf(['P' PT{k}]));
       stru.dose = [];
    else
       stru      = obj;
       stru.dose = [];
    end
    %
    for i = 1:length(IDX)
       name     = st{IDX(i),8};
       info_dcm = st{IDX(i),36};
       if isfield(info_dcm,'DoseSummationType')==1 && strcmp(info_dcm.DoseSummationType,'BEAM')==1
          Beam = info_dcm.ReferencedRTPlanSequence.Item_1.ReferencedFractionGroupSequence.Item_1.ReferencedBeamSequence.Item_1.ReferencedBeamNumber;
          name = [name '_BEAM' num2str(Beam)];
       end
       %
       name = strrep(name,' ','');
       name = strrep(name,':','_');
       name = strrep(name,'-','_');
       name = strrep(name,'.','_');
       name = strrep(name,'(','_');
       name = strrep(name,')','_');
       name = strrep(name,'[','_');
       name = strrep(name,']','_');
       %
       % show file name in case the user need to name the dose
       f_n = findstr(st{IDX(i),37},filesep);
       f_n = st{IDX(i),37}(f_n(end)+1:end);
       f_n = strrep(f_n,'_',' ');
       dlgtitle = 'Dose ID';
       prompt   = ['Dose ID ... do not use space, symbols or start with a number! File name: ' f_n] ;
       definput = {name};
       opts.Interpreter = 'tex';
       answer = inputdlg(prompt,dlgtitle,[1 60],definput,opts);
       name = answer{1};
       %
       %
       if isvarname(name)==0
            prompt = {'Please remove symbols (,:-#;.etc and make sure the name cannot start with a number'};
            dlgtitle = 'Not a valid name - Dose string';
            definput = {name};
            opts.Interpreter = 'tex';
            answer = inputdlg(prompt,dlgtitle,[1 40],definput,opts);
            name = answer{1};
            %
            if isvarname(name)==0
                errordlg('Not a valid number ... try to import the data again');
                return;
            end
            %
       end
       %
       % check if field alread exist
       if isfield(stru.dose,name)==1
           %
           warndlg('Dose already exist ... replacing the file!');
           %
       end
       %
       TP_temp = read_dose_dcm(stru,st(IDX(i),:)); 
       stru.dose.(sprintf(name)) = TP_temp.dose;
    end
    %
    % conversion factor from TOPAS to MEVION
     if strcmp(info_dcm.Manufacturer,'TOPAS')==1
          prompt           = {'Conversion factor TOPAS'};
          dlgtitle         = 'CF value';
          %
      %    CF               = 8.3586e+07*info_dcm.DoseValue*info_dcm.NumberOfFractionsDelivered;
      %   after adjusting beam model
          CF               = 8.46976938e+07*info_dcm.DoseValue*info_dcm.NumberOfFractionsDelivered;
          %
          definput         = {num2str(CF,'%.6e')};
          opts.Interpreter = 'tex';
          answer           = inputdlg(prompt,dlgtitle,[1 40],definput,opts);
          %
          CF                                = str2double(answer{1});
          stru.dose.(sprintf(name)).Dose_NI = stru.dose.(sprintf(name)).Dose_NI * CF;
          %stru.dose.(sprintf(name)).Dose_NI = flip(stru.dose.(sprintf(name)).Dose_NI,3);
          %
     end    
    %
    if isempty(stru.dose)==0
%       if isfield(stru.dose,'Dose_TPS_NI')==0 && isfield(stru.dose,'Dose_NI_BEAM_1')==1
%         stru.dose.Dose_TPS_NI = stru.dose.Dose_NI_BEAM_1;
%       end
      stru=interpolatedose(stru);
    end
    %
    %
    if isfield(obj.Studies,['P' PT{k}])==1
       obj.Studies.(sprintf(['P' PT{k}]))=stru;
    else
       obj.dose.(sprintf(name)) = stru.dose.(sprintf(name));
    end
    %
end
end



function rts = read_dose_dcm(rts,file)
 % cm info loaded before
 info_dcm=file{36};
 % 
 if isempty(info_dcm.Width)==1
     rts.dose=[];
     return;
 end
 rts.dose.Nvoxels(1)            = info_dcm.Width;
 rts.dose.Nvoxels(2)            = info_dcm.Height;
 rts.dose.Nvoxels(3)            = info_dcm.NumberOfFrames;
 %
 if isfield(info_dcm,'DoseUnits') == 1
    rts.dose.DoseUnits          = info_dcm.DoseUnits;
 else
    rts.dose.DoseUnits          = [];
 end
 rts.dose.ImagePositionPatient  = info_dcm.ImagePositionPatient';
 rts.dose.DoseGridScaling       = info_dcm.DoseGridScaling;
 %
 %
 %
 rts.dose.Dose_NI               = single(squeeze(dicomread([file{37} file{1}])))*rts.dose.DoseGridScaling;
 %
 rts.dose.Resolution(1:2)  = info_dcm.PixelSpacing;
 if isempty(info_dcm.SliceThickness)==1 || info_dcm.SliceThickness==0
     if isfield(info_dcm,'GridFrameOffsetVector')==1
         rts.dose.Resolution(3)  = diff(info_dcm.GridFrameOffsetVector(1:2,1));
     else
         rts.dose.Resolution(3)  = 0;  
     end
 else
   rts.dose.Resolution(3)  = info_dcm.SliceThickness;
 end
 %
 if rts.dose.Resolution(3)==0
    def_answer={num2str(rts.dose.Resolution(1))};
    %c
    answer=inputdlg('Missing dose slice tickness ... the value below is just a guess',...
                    'Slice tickness:',1,def_answer);
    %
    rts.dose.Resolution(3)=str2num(answer{1});
 end
 %
 if isfield(info_dcm,'TissueHeterogeneityCorrection')
    rts.dose.DoseType=info_dcm.TissueHeterogeneityCorrection;
 end
 %
end
%




function rts = interpolatedose(rts)
%
%  
if isempty(findobj('Tag','amb_interface'))==0
    w_bar.hand = getappdata(findobj('Tag','amb_interface'),'waitbarjava');
    set(w_bar.hand,'Visible',1,'Value',0);
else
    w_bar=[];
end
%
d_names = fieldnames(rts.dose);
for kk=1:length(d_names)
  if isfield(rts.dose.(sprintf(d_names{kk})),'Dose_NI') == 1 && sum(rts.dose.(sprintf(d_names{kk})).Nvoxels == rts.image.Nvoxels)~=3 && isfield(rts.dose.(sprintf(d_names{kk})),'Dose') == 0
     %
     if isempty(w_bar)==0
        set(w_bar.hand,'Visible',1,'Value',25);
     end
     %
      rts.image.Nvoxels                        = double(rts.image.Nvoxels);
      rts.dose.(sprintf(d_names{kk})).Nvoxels  = double(rts.dose.(sprintf(d_names{kk})).Nvoxels);
      %
      [Xq,Yq,Zq]    = meshgrid(rts.image.ImagePositionPatient(1):rts.image.Resolution(1):rts.image.ImagePositionPatient(1)+rts.image.Resolution(1)*(rts.image.Nvoxels(1)-1),...
                               rts.image.ImagePositionPatient(2):rts.image.Resolution(2):rts.image.ImagePositionPatient(2)+rts.image.Resolution(2)*(rts.image.Nvoxels(2)-1), ...
                               rts.image.ImagePositionPatient(3):rts.image.Resolution(3):rts.image.ImagePositionPatient(3)+rts.image.Resolution(3)*(rts.image.Nvoxels(3)-1)); % image grid
      %
      [X,Y,Z]       = meshgrid(rts.dose.(sprintf(d_names{kk})).ImagePositionPatient(1)-rts.image.Resolution(1)/2:rts.dose.(sprintf(d_names{kk})).Resolution(1):rts.dose.(sprintf(d_names{kk})).ImagePositionPatient(1)-rts.image.Resolution(1)/2+rts.dose.(sprintf(d_names{kk})).Resolution(1)*(rts.dose.(sprintf(d_names{kk})).Nvoxels(1)-1),...
                               rts.dose.(sprintf(d_names{kk})).ImagePositionPatient(2)-rts.image.Resolution(2)/2:rts.dose.(sprintf(d_names{kk})).Resolution(2):rts.dose.(sprintf(d_names{kk})).ImagePositionPatient(2)-rts.image.Resolution(2)/2+rts.dose.(sprintf(d_names{kk})).Resolution(2)*(rts.dose.(sprintf(d_names{kk})).Nvoxels(2)-1), ...
                               rts.dose.(sprintf(d_names{kk})).ImagePositionPatient(3)-rts.image.Resolution(3)/2:rts.dose.(sprintf(d_names{kk})).Resolution(3):rts.dose.(sprintf(d_names{kk})).ImagePositionPatient(3)-rts.image.Resolution(3)/2+rts.dose.(sprintf(d_names{kk})).Resolution(3)*(rts.dose.(sprintf(d_names{kk})).Nvoxels(3)-1)); % dose grid
      %
     %
     rts.dose.(sprintf(d_names{kk})).Dose                                 = interp3(X,Y,Z,rts.dose.(sprintf(d_names{kk})).Dose_NI,Xq,Yq,Zq,'linear');
     rts.dose.(sprintf(d_names{kk})).Dose(isnan(rts.dose.(sprintf(d_names{kk})).Dose)) = 0;
  else
     rts.dose.(sprintf(d_names{kk})).Dose                                 = rts.dose.(sprintf(d_names{kk})).Dose;
     rts.dose.(sprintf(d_names{kk})).Dose(isnan(rts.dose.(sprintf(d_names{kk})).Dose)) = 0; 
  end
end
  %
   if isempty(w_bar)==0
     set(w_bar.hand,'Visible',0,'Value',0);
   end
  %
end
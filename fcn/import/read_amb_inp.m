% Import MCNP phantom created using AMIGOBrachy
% It is not a script to open general mcnp inputs !!! 
[File,Folder] = uigetfile('*.*');
%
fid=fopen([Folder File],'r');
%
cont=1; % voxel ID
%
while ~feof(fid)
    a=fgetl(fid);
    if strcmp(a(1:8),'c  Phant')
        % skip two lines
        fgetl(fid);         fgetl(fid);
        % get info about number of voxels
        a=fgetl(fid);         a=a(53:end);
        % find number separation
        l1=strfind(a,':');    l2=strfind(a,' ');
        % define N_voxels +1 beacuse MCNP starts at 0
        MCNP.Voxels(1)=str2num(a(l1(1)+1:l2(1)-1))+1;
        MCNP.Voxels(2)=str2num(a(l1(2)+1:l2(2)-1))+1;
        MCNP.Voxels(3)=str2num(a(l1(3)+1:l2(3)-1))+1;
        % Create variables
        MCNP.Uni = zeros(MCNP.Voxels);
        MCNP.Mat = zeros(MCNP.Voxels);
        MCNP.Den = zeros(MCNP.Voxels);
        % read all voxels
        while cont<=MCNP.Voxels(1)*MCNP.Voxels(2)*MCNP.Voxels(3)
            a=fgetl(fid); a=strsplit(a);
            for i=1:size(a,2)
                if isempty(a{i})==0 && strcmp(a{i}(end),'r')==0 && strcmp(a{i}(end),'&')==0 ...
                        && (strcmp(a{i}(end),'c')==0 || strcmp(a{i}(end),'&')==0) % new uni
                    uni=str2num(a{i});
                    MCNP.Uni(cont)=uni;
                    cont=cont+1;
                elseif isempty(a{i})==0 && strcmp(a{i}(end),'r')==1  ...
                        && (strcmp(a{i}(end),'c')==0 || strcmp(a{i}(end),'&')==0)                           % repetition
                    r = str2num(a{i}(1:end-1));
                    MCNP.Uni(cont:cont+r-1)=uni;
                    cont=cont+r;
                end       
            end
        end
        %
        fgetl(fid);fgetl(fid);
        % read universes - mat and dens
        a=fgetl(fid); a=strsplit(a);
        while isempty(a{1})==1
           uni=str2num(a{11}(3:end));
           mat=str2num(a{3});
           den=abs(str2num(a{4}));
           MCNP.Mat(MCNP.Uni==uni)=mat;
           MCNP.Den(MCNP.Uni==uni)=den;
           a=fgetl(fid); a=strsplit(a);
        end
        break;
    end
end
fclose(fid);
clear a i Folder File den mat uni r fid ans l1 l2 cont

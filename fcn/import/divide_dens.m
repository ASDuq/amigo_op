function [Dose] = divide_dens(Dose,TPlan,ratio,shift)
ratio=round(ratio*1000)/1000;
for k=1:size(Dose,3)
    k2 = ceil(k*ratio(3)) + shift(3);
    for j=1:size(Dose,2)
        j2    = ceil(j*ratio(1)) + shift(1);
        for i=1:size(Dose,1)
           i2 = ceil(i*ratio(2)) + shift(2);
           Dose(i,j,k) = Dose(i,j,k)/double(TPlan.plan.Density(i2,j2,k2));
        end
    end
end
end


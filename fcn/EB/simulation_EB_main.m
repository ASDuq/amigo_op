function [] = simulation_EB_main()
% get simulation info
%
handles = guihandles;
TPlan=getappdata(handles.amb_interface,'TPlan');

% parameters from user
simulation   = get_sim_info;
%
if isnumeric(simulation)==1
    return;
end
% convert dwell positions 
TPlan=getappdata(findobj('Tag','amb_interface'),'TPlan');
%
if isfield(TPlan.plan,'dosegrid')==1
  TPlan.plan.dosegrid     = double(TPlan.plan.dosegrid);
end
TPlan.plan.phantom        = double(TPlan.plan.phantom);
%
%
[Folder]= uigetdir;
if size(Folder,2)==1
    return;
end
%
% create input model
    tr         = fopen([Folder filesep 'B_' num2str(1) 'CP_' num2str(1) '.inp'],'Wb+');
    %
    w_bar.hand = getappdata(findobj('Tag','amb_interface'),'waitbarjava');
    %
    write_header(tr,simulation,TPlan);           set(w_bar.hand,'Visible',1,'Value',40);
    write_cells(tr,simulation,TPlan);            set(w_bar.hand,'Visible',1,'Value',50);
    write_surfaces(tr,TPlan);                    set(w_bar.hand,'Visible',1,'Value',60);
    write_source(tr);                            set(w_bar.hand,'Visible',1,'Value',70);
    write_tally(tr,simulation,TPlan);            set(w_bar.hand,'Visible',1,'Value',90);
    write_material(tr,TPlan);                    set(w_bar.hand,'Visible',1,'Value',91);
    %
    fprintf(tr,'PHYS:P\n');
    fprintf(tr,'cut:p j 0.001\n');
    fprintf(tr,'cut:e j 0.050\n');
    fprintf(tr,'prdmp j 1e+7 j j\n');
    fprintf(tr,'dbcn 17j 1 30j 2\n');
    %
    fprintf(tr,'nps %d\n',simulation.Nps);
    fclose(tr);
%
for i=TPlan.plan.NBeams:-1:1
    for j=TPlan.plan.(sprintf('Beam_%d',i)).NumberOfControlPoints:-1:1
        if i~=1 || j~=1 
          copyfile([Folder filesep 'B_' num2str(1) 'CP_' num2str(1) '.inp'],[Folder filesep 'B_' num2str(i) 'CP_' num2str(j) '.inp']);
        end
        tr         = fopen([Folder filesep 'B_' num2str(i) 'CP_' num2str(j) '.inp'],'A');
        write_linac_tr(tr,TPlan.plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)));
        fclose(tr);
    end
end
%
set(w_bar.hand,'Visible',0,'Value',0);
end
%


function [simulation] =  get_sim_info()
%
handles      = guihandles;
%
simulation=[];
%
% Phantom 
if        strcmp(handles.phantom_user.Checked,'on')==1
             simulation.Phantom='User';
elseif    strcmp(handles.phantom_all.Checked,'on')==1
             simulation.Phantom='All';
else 
           warndlg('Select the phantom size'); 
           simulation=[]; simulation=1; return; %#ok<*NASGU>
end
%
% dose grid 
if        strcmp(handles.dosegrid_user.Checked,'on')==1
             simulation.Dosegrid='User';
elseif    strcmp(handles.dosegrid_all.Checked,'on')==1
             simulation.Dosegrid='All';
else 
          warndlg('Select the dose grid'); return;             
end
%
% ------ score
%%       only *F8 for now
% if    strcmp(handles.score_dwm.Checked,'on')==1
%              simulation.Score{1}='Dwm';
% end
% if    strcmp(handles.score_dmm.Checked,'on')==1
%              simulation.Score{2}='Dmm';
% end
% if    strcmp(handles.score_energy.Checked,'on')==1
%              simulation.Score{3}='Mean';             
% end
% if    strcmp(handles.score_dose.Checked,'on')==1
             simulation.Score{4}='Dose';             
% end
%
% voxels
if        strcmp(handles.vox_on.Checked,'on')==1
             simulation.Voxel='On';
elseif    strcmp(handles.vox_off.Checked,'on')==1
             simulation.Voxel='Off';           
end
%
% density
if        strcmp(handles.dens_lib.Checked,'on')==1
             simulation.Density='Lib';
elseif    strcmp(handles.dens_ct.Checked,'on')==1
             simulation.Density='CT';           
end
%
% 
% nps
simulation.Nps=str2double(handles.sim_nps.Label);
%
end



%
function []    =  write_header(tr,simulation,TPlan)
fprintf(tr,'AMIGOBrachy - EXTERNAL BEAM \n');  
%  
%
end
%


function [] = write_cells(tr,simulation,TPlan)
TPlan.plan.phantom=double(TPlan.plan.phantom);
    %% LINAC - MLC - JAW
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c LINAC HEAD - CUBE FILLED WIT AIR MLC AND JAW \n');
    fprintf(tr,'  1 0   1  -2  3  -4   5  -6               trcl=1     fill=1002 imp:p=1 imp:e=1\n');
    fprintf(tr,'c COLLIMATOR 1 - X \n');
    fprintf(tr,'  2 1     -0.0012    7  -8  -9      trcl=2   fill=1001   u=1002 imp:p=1 imp:e=1\n');
    fprintf(tr,'  3 1     -0.0012    #2                                  u=1002 imp:p=1 imp:e=1\n');
    fprintf(tr,'c JAW 1 - X \n');
    fprintf(tr,'  4 1001 -17.85   11 -12 13 -14  15 -16         trcl=3   u=1001 imp:p=1 imp:e=1\n');
    fprintf(tr,'c JAW 2 - X \n');
    fprintf(tr,'  5 1001 -17.85   21 -22 23 -24  25 -26         trcl=4   u=1001 imp:p=1 imp:e=1\n');
    fprintf(tr,'c JAW 3 - Y \n');
    fprintf(tr,'  6 1001 -17.85   31 -32 33 -34  35 -36         trcl=5   u=1001 imp:p=1 imp:e=1\n');
    fprintf(tr,'c JAW 4 - Y \n');
    fprintf(tr,'  7 1001 -17.85   41 -42 43 -44  45 -46         trcl=6   u=1001 imp:p=1 imp:e=1\n');
    fprintf(tr,'c \n');
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c MLC HALF-LEAF PAIR  UPPER RIGHT \n');
    fprintf(tr,' 20 1001 -17.85  101 -102  103 -104 105 -106    trcl=20  u=1001 imp:p=1 imp:e=1\n');
    fprintf(tr,' 21 1001 -17.85  101 -102  104 -107 105 -106    trcl=21  u=1001 imp:p=1 imp:e=1\n');
    fprintf(tr,'c \n');
    for i=22:39 
       if mod(i,2)==0   
         fprintf(tr,' %d like 20 but trcl=%d\n',i,i);
       else
         fprintf(tr,' %d like 21 but trcl=%d\n',i,i);   
       end
    end
    fprintf(tr,'c \n');
    %
    fprintf(tr,'c MLC FULL-LEAF PAIR  UPPER RIGHT \n');
    fprintf(tr,' 40 1001 -17.85 151 -152  153 -154 155 -156     trcl=40  u=1001 imp:p=1 imp:e=1\n');
    fprintf(tr,' 41 1001 -17.85 151 -152  154 -157 155 -156     trcl=41  u=1001 imp:p=1 imp:e=1\n');
    fprintf(tr,'c \n');
    for i=42:49 
      if mod(i,2)==0   
         fprintf(tr,' %d like 40 but trcl=%d\n',i,i);
      else
         fprintf(tr,' %d like 41 but trcl=%d\n',i,i);   
      end
    end
    fprintf(tr,'c \n');
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c MLC HALF-LEAF PAIR  LOWER RIGHT \n');
    fprintf(tr,' 50 1001 -17.85 201 -202  203 -204 205 -206     trcl=50  u=1001 imp:p=1 imp:e=1\n');
    fprintf(tr,' 51 1001 -17.85 201 -202 -203  207 205 -206     trcl=51  u=1001 imp:p=1 imp:e=1\n');
    fprintf(tr,'c \n');
    for i=52:69 
      if mod(i,2)==0   
         fprintf(tr,' %d like 50 but trcl=%d\n',i,i);
      else
         fprintf(tr,' %d like 51 but trcl=%d\n',i,i);   
      end
     end
     fprintf(tr,'c \n');
     fprintf(tr,'c MLC FULL-LEAF PAIR  LOWER RIGHT \n');
     fprintf(tr,'70 1001 -17.85 251 -252  253 -254 255 -256      trcl=70  u=1001 imp:p=1 imp:e=1\n');
     fprintf(tr,'71 1001 -17.85 251 -252 -253  257 255 -256      trcl=71  u=1001 imp:p=1 imp:e=1\n');
     fprintf(tr,'c \n');
     for i=72:79 
       if mod(i,2)==0   
         fprintf(tr,' %d like 70 but trcl=%d\n',i,i);
       else
         fprintf(tr,' %d like 71 but trcl=%d\n',i,i);   
       end
     end
     fprintf(tr,'c \n');
     fprintf(tr,'c ---------------------------------------------------------------------------80\n');
     fprintf(tr,'c MLC HALF-LEAF PAIR  UPPER left \n');
     fprintf(tr,' 80 1001 -17.85 301 -302  303 -304 305 -306     trcl=80  u=1001 imp:p=1 imp:e=1\n');
     fprintf(tr,' 81 1001 -17.85 301 -302  304 -307 305 -306     trcl=81  u=1001 imp:p=1 imp:e=1\n');
     fprintf(tr,'c \n');
     for i=82:99 
       if mod(i,2)==0   
         fprintf(tr,' %d like 80 but trcl=%d\n',i,i);
       else
         fprintf(tr,' %d like 81 but trcl=%d\n',i,i);   
       end
     end
     fprintf(tr,'c \n');
     fprintf(tr,'c MLC FULL-LEAF PAIR  UPPER left \n');
     fprintf(tr,' 100 1001 -17.85 351 -352  353 -354 355 -356   trcl=100  u=1001 imp:p=1 imp:e=1\n');
     fprintf(tr,' 101 1001 -17.85 351 -352  354 -357 355 -356   trcl=101  u=1001 imp:p=1 imp:e=1\n');
     fprintf(tr,'c \n');
     for i=102:109 
        if mod(i,2)==0   
          fprintf(tr,' %d like 100 but trcl=%d\n',i,i);
        else
          fprintf(tr,' %d like 101 but trcl=%d\n',i,i);   
        end
      end
      fprintf(tr,'c \n');
      fprintf(tr,'c ---------------------------------------------------------------------------80\n');
      fprintf(tr,'c MLC HALF-LEAF PAIR  LOWER left\n');
      fprintf(tr,' 110 1001 -17.85 401 -402  403 -404 405 -406   trcl=110  u=1001 imp:p=1 imp:e=1\n');
      fprintf(tr,' 111 1001 -17.85 401 -402 -403  407 405 -406   trcl=111  u=1001 imp:p=1 imp:e=1\n');
      fprintf(tr,'c \n');
      for i=112:129 
         if mod(i,2)==0   
           fprintf(tr,' %d like 110 but trcl=%d\n',i,i);
         else
           fprintf(tr,' %d like 111 but trcl=%d\n',i,i);   
         end
       end
       fprintf(tr,'c \n');
       fprintf(tr,'c MLC FULL-LEAF PAIR  LOWER left  \n');
       fprintf(tr,' 130 1001 -17.85 451 -452  453 -454 455 -456  trcl=130  u=1001 imp:p=1 imp:e=1\n');
       fprintf(tr,' 131 1001 -17.85 451 -452 -453  457 455 -456  trcl=131  u=1001 imp:p=1 imp:e=1\n');
       fprintf(tr,'c \n');
       for i=132:139 
         if mod(i,2)==0   
           fprintf(tr,' %d like 130 but trcl=%d\n',i,i);
         else
           fprintf(tr,' %d like 131 but trcl=%d\n',i,i);   
         end
       end
       %
       fprintf(tr,'c ---------------------------------------------------------------------------80\n');
       fprintf(tr,'c \n');
       fprintf(tr,' 150 1    -0.1 #4 #5 #6 #7 #20  #21  #22  #23  #24  #25  #26  #27  #28\n');
       fprintf(tr,'                           #29  #30  #31  #32  #33  #34  #35  #36  #37  #38\n');
       fprintf(tr,'                           #39  #40  #41  #42  #43  #44  #45  #46  #47  #48\n');
       fprintf(tr,'                           #49  #50  #51  #52  #53  #54  #55  #56  #57  #58\n');
       fprintf(tr,'                           #59  #60  #61  #62  #63  #64  #65  #66  #67  #68\n');
       fprintf(tr,'                           #69  #70  #71  #72  #73  #74  #75  #76  #77  #78\n');
       fprintf(tr,'                           #79  #80  #81  #82  #83  #84  #85  #86  #87  #88\n');
       fprintf(tr,'                           #89  #90  #91  #92  #93  #94  #95  #96  #97  #98\n');
       fprintf(tr,'                           #99  #100 #101 #102 #103 #104 #105 #106 #107 #108\n');
       fprintf(tr,'                           #109 #110 #111 #112 #113 #114 #115 #116 #117 #118\n');
       fprintf(tr,'                           #119 #120 #121 #122 #123 #124 #125 #126 #127 #128\n');
       fprintf(tr,'                           #129 #130 #131 #132 #133 #134 #135 #136 #137 #138\n');
       fprintf(tr,'                           #139                       u=1001 imp:p=1 imp:e=1\n');
       fprintf(tr,'c ---------------------------------------------------------------------------80\n');
       %% ---------------------------------------------------------------------------------------------
       % ----------------------------------------------------------------------------------------------
       fprintf(tr,'c  Phant. PITCH. ROLL, ROTATION and TRANSLATION\n');
       fprintf(tr,'    500 0        501 -502 503 -504 505 -506  trcl=8  fill=999  imp:p=1 imp:e=1\n');
       % -----------------------------------------------------------------------------------------------
       fprintf(tr,'c ---------------------------------------------------------------------------80\n');
       % Voxel Phantom
       write_phantom(tr, simulation, TPlan);
       % region of interet 
       fprintf(tr,'c  Region of interest\n');
       fprintf(tr,'    998  1 -0.0012  971 -972 973 -974 975 -976 #1 #500          imp:p=1 imp:e=1\n');
       % ------------------------------------------------------------------------------------------------------------------------
       % Region of no interest
       fprintf(tr,'c  Region of no interest\n');
       fprintf(tr,'    999  0    -971:972:-973:974:-975:976                        imp:p=0 imp:e=0\n');
       fprintf(tr,'c ---------------------------------------------------------------------------80\n\n');
end



function [] = write_surfaces(tr,TPlan)
    % Surfaces
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'9999 pz 90.0\n');
    fprintf(tr,'c LINAC HEAD\n');
    fprintf(tr,'c\n');
    fprintf(tr,'1 px   -30.0\n');
    fprintf(tr,'2 px    30.0\n');
    fprintf(tr,'3 py   -30.0\n');
    fprintf(tr,'4 py    30.0\n');
    fprintf(tr,'5 pz   -60.0\n');
    fprintf(tr,'6 pz   -20.0\n');
    fprintf(tr,'c\n');
    fprintf(tr,'c Collimator\n');
    fprintf(tr,'7 pz    -59\n');
    fprintf(tr,'8 pz    -15\n');
    fprintf(tr,'9 cz     29.0\n');
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c JAW 1 - X\n');
    fprintf(tr,'c\n');
    fprintf(tr,'11 px    0.0\n');
    fprintf(tr,'12 px   12.0\n');
    fprintf(tr,'13 py  -10.0\n');
    fprintf(tr,'14 py   10.0\n');
    fprintf(tr,'15 pz   -7.8\n');
    fprintf(tr,'16 pz    0.0\n');
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c JAW 2 - X\n');
    fprintf(tr,'c\n');
    fprintf(tr,'21 px  -12.0\n');
    fprintf(tr,'22 px    0.0\n');
    fprintf(tr,'23 py  -10.0\n');
    fprintf(tr,'24 py   10.0\n');
    fprintf(tr,'25 pz   -7.8\n');
    fprintf(tr,'26 pz    0.0\n');
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c JAW 3 - Y\n');
    fprintf(tr,'c\n');
    fprintf(tr,'31 px  -10.00\n');
    fprintf(tr,'32 px   10.00\n');
    fprintf(tr,'33 py    0.00\n');
    fprintf(tr,'34 py   12.00\n');
    fprintf(tr,'35 pz   -7.77 \n');
    fprintf(tr,'36 pz    0.00\n');
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c JAW 4 - Y\n');
    fprintf(tr,'c\n');
    fprintf(tr,'41 px  -10.00\n');
    fprintf(tr,'42 px   10.00\n');
    fprintf(tr,'43 py  -12.00\n');
    fprintf(tr,'44 py    0.00\n');
    fprintf(tr,'45 pz   -7.77\n');
    fprintf(tr,'46 pz    0.00\n');
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c\n');
    fprintf(tr,'c MLC - HALF-LEAF - PAIR - Upper Right\n');
    fprintf(tr,'c\n');
    fprintf(tr,'101 px    0.00\n');
    fprintf(tr,'102 px   10.00\n');
    fprintf(tr,'103 py    0.00\n');
    fprintf(tr,'104 py    0.25\n');
    fprintf(tr,'105 pz   -7.77\n');
    fprintf(tr,'106 pz    0.00\n');
    fprintf(tr,'c\n');
    fprintf(tr,'107 py    0.50\n');
    fprintf(tr,'c\n');
    fprintf(tr,'c MLC - HALF-LEAF - PAIR - Upper Right\n');
    fprintf(tr,'c\n');
    fprintf(tr,'151 px    0.00\n');
    fprintf(tr,'152 px   10.00\n');
    fprintf(tr,'153 py    5.00\n');
    fprintf(tr,'154 py    5.50\n');
    fprintf(tr,'155 pz   -7.77\n');
    fprintf(tr,'156 pz    0.00\n');
    fprintf(tr,'c\n');
    fprintf(tr,'157 py    6.00\n');
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c\n');
    fprintf(tr,'c MLC - HALF-LEAF - PAIR - lower Right\n');
    fprintf(tr,'c\n');
    fprintf(tr,'201 px    0.00\n');
    fprintf(tr,'202 px   10.00\n');
    fprintf(tr,'203 py   -0.25\n');
    fprintf(tr,'204 py    0.00\n');
    fprintf(tr,'205 pz   -7.77\n');
    fprintf(tr,'206 pz    0.00\n');
    fprintf(tr,'c\n');
    fprintf(tr,'207 py   -0.50\n');
    fprintf(tr,'c\n');
    fprintf(tr,'c MLC - HALF-LEAF - PAIR - lower Right\n');
    fprintf(tr,'c\n');
    fprintf(tr,'251 px    0.00\n');
    fprintf(tr,'252 px   10.00\n');
    fprintf(tr,'253 py   -5.50\n');
    fprintf(tr,'254 py   -5.00\n');
    fprintf(tr,'255 pz   -7.77\n');
    fprintf(tr,'256 pz    0.00\n');
    fprintf(tr,'c\n');
    fprintf(tr,'257 py   -6.00\n');
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c\n');
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c\n');
    fprintf(tr,'c MLC - HALF-LEAF - PAIR - Upper left\n');
    fprintf(tr,'c\n');
    fprintf(tr,'301 px  -10.00\n');
    fprintf(tr,'302 px    0.00\n');
    fprintf(tr,'303 py    0.00\n');
    fprintf(tr,'304 py    0.25\n');
    fprintf(tr,'305 pz   -7.77\n');
    fprintf(tr,'306 pz    0.00\n');
    fprintf(tr,'c\n');
    fprintf(tr,'307 py    0.50\n');
    fprintf(tr,'c\n');
    fprintf(tr,'c MLC - HALF-LEAF - PAIR - Upper left\n');
    fprintf(tr,'c\n');
    fprintf(tr,'351 px  -10.00\n');
    fprintf(tr,'352 px    0.00\n');
    fprintf(tr,'353 py    5.00\n');
    fprintf(tr,'354 py    5.50\n');
    fprintf(tr,'355 pz   -7.77\n');
    fprintf(tr,'356 pz    0.00\n');
    fprintf(tr,'c\n');
    fprintf(tr,'357 py    6.00\n');
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c\n');
    fprintf(tr,'c MLC - HALF-LEAF - PAIR - lower left\n');
    fprintf(tr,'c\n');
    fprintf(tr,'401 px  -10.00\n');
    fprintf(tr,'402 px    0.00\n');
    fprintf(tr,'403 py   -0.25\n');
    fprintf(tr,'404 py    0.00\n');
    fprintf(tr,'405 pz   -7.77\n');
    fprintf(tr,'406 pz    0.00\n');
    fprintf(tr,'c\n');
    fprintf(tr,'407 py   -0.50\n');
    fprintf(tr,'c\n');
    fprintf(tr,'c MLC - HALF-LEAF - PAIR - lower left\n');
    fprintf(tr,'c\n');
    fprintf(tr,'451 px  -10.00\n');
    fprintf(tr,'452 px    0.00\n');
    fprintf(tr,'453 py   -5.50\n');
    fprintf(tr,'454 py   -5.00\n');
    fprintf(tr,'455 pz   -7.77\n');
    fprintf(tr,'456 pz    0.00\n');
    fprintf(tr,'c\n');
    fprintf(tr,'457 py   -6.00\n');
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    %%
    % ------------------------------------------------------------------------------------------------------------------------
    % write the phantom adding 2cm of air around it.
    %
    % calculate phantom edges
    Pha_edges(1,1) =  (TPlan.plan.phantom(3,1)-1)*TPlan.image.Resolution(3)/10+(TPlan.image.ImagePositionPatient(3))/10;
    Pha_edges(1,2) =  TPlan.plan.phantom(3,2)    *TPlan.image.Resolution(3)/10+(TPlan.image.ImagePositionPatient(3))/10;
    Pha_edges(2,1) =  (TPlan.plan.phantom(1,1)-1)*TPlan.image.Resolution(1)/10+(TPlan.image.ImagePositionPatient(1))/10;
    Pha_edges(2,2) =  TPlan.plan.phantom(1,2)    *TPlan.image.Resolution(1)/10+(TPlan.image.ImagePositionPatient(1))/10;
    Pha_edges(3,1) = -TPlan.plan.phantom(2,2)    *TPlan.image.Resolution(2)/10-(TPlan.image.ImagePositionPatient(2))/10;
    Pha_edges(3,2) = -(TPlan.plan.phantom(2,1)-1)*TPlan.image.Resolution(2)/10-(TPlan.image.ImagePositionPatient(2))/10;
    %
    %
    fprintf(tr,'c  Phanton\n');
    fprintf(tr,'501 py  %.6f\n',Pha_edges(1,1)+0.001);
    fprintf(tr,'502 py  %.6f\n',Pha_edges(1,2)-0.001);
    fprintf(tr,'503 px  %.6f\n',Pha_edges(2,1)+0.001);
    fprintf(tr,'504 px  %.6f\n',Pha_edges(2,2)-0.001);
    fprintf(tr,'505 pz  %.6f\n',Pha_edges(3,1)+0.001);
    fprintf(tr,'506 pz  %.6f\n',Pha_edges(3,2)-0.001);
    fprintf(tr,'c  Lattice\n');
    fprintf(tr,'601 py   %.6f\n',(TPlan.plan.phantom(3,1)-1)*TPlan.image.Resolution(3)/10 -0.000001+(TPlan.image.ImagePositionPatient(3))/10);
    fprintf(tr,'602 py   %.6f\n', TPlan.plan.phantom(3,1)   *TPlan.image.Resolution(3)/10 +0.000001+(TPlan.image.ImagePositionPatient(3))/10);
    fprintf(tr,'603 px   %.6f\n',(TPlan.plan.phantom(1,1)-1)*TPlan.image.Resolution(1)/10 -0.000001+(TPlan.image.ImagePositionPatient(1))/10);
    fprintf(tr,'604 px   %.6f\n', TPlan.plan.phantom(1,1)   *TPlan.image.Resolution(1)/10 +0.000001+(TPlan.image.ImagePositionPatient(1))/10);
    fprintf(tr,'605 pz   %.6f\n',-TPlan.plan.phantom(2,2)   *TPlan.image.Resolution(2)/10 -0.000001-(TPlan.image.ImagePositionPatient(2))/10);
    fprintf(tr,'606 pz   %.6f\n',-(TPlan.plan.phantom(2,2)-1)*TPlan.image.Resolution(2)/10+0.000001-(TPlan.image.ImagePositionPatient(2))/10);
    fprintf(tr,'c  Edge\n'); 
    % check CP for phantom translations and rotations to define the limits of the
    % room
    c=1;
    for i=1:TPlan.plan.NBeams
        field=TPlan.plan.(sprintf('Beam_%d',i));
        for j=1:field.NumberOfControlPoints
            CP(c,1:3)=field.(sprintf('CP_%d',j)).TR_8/10;
            c=c+1;
        end
    end
    %
    S_x(1)=min(CP(:,1));
    S_x(2)=max(CP(:,1));
    S_y(1)=min(CP(:,2));
    S_y(2)=max(CP(:,2));
    S_z(1)=min(CP(:,3));
    S_z(2)=max(CP(:,3));
    %
    fprintf(tr,'971 px  %f\n',min([(TPlan.plan.phantom(3,1)-1)*TPlan.image.Resolution(3)/10+0.001+(TPlan.image.ImagePositionPatient(3))/10-1+S_x(1),-100]));
    fprintf(tr,'972 px  %f\n',max([TPlan.plan.phantom(3,2)   *TPlan.image.Resolution(3)/10-0.001+(TPlan.image.ImagePositionPatient(3))/10+1+S_x(2), 100]));
    fprintf(tr,'973 py  %f\n',min([(TPlan.plan.phantom(1,1)-1)*TPlan.image.Resolution(1)/10+0.001+(TPlan.image.ImagePositionPatient(1))/10-1+S_y(1),-100]));
    fprintf(tr,'974 py  %f\n',max([TPlan.plan.phantom(1,2)   *TPlan.image.Resolution(1)/10-0.001+(TPlan.image.ImagePositionPatient(1))/10+1+S_y(2), 100]));
    fprintf(tr,'975 pz  %f\n',min([-TPlan.plan.phantom(2,2)   *TPlan.image.Resolution(1)/10-0.001-(TPlan.image.ImagePositionPatient(2))/10-1+S_z(1), -100]));
    fprintf(tr,'976 pz  %f\n\n',max([-(TPlan.plan.phantom(2,1)-1)*TPlan.image.Resolution(1)/10+0.001-(TPlan.image.ImagePositionPatient(2))/10+1+S_z(2),100]));
     
end


%
function [] = write_phantom(tr,simulation,TPlan)
%
% check material map
if isfield(TPlan.plan,'Mat_HU')==0
    errordlg('Create a material map first!');
    return
end
l=TPlan.plan.phantom;
fprintf(tr,'600 0       -606 605 -602 601 -604 603      lat=1 u=999 imp:p=1 imp:e=1\n');
fprintf(tr,'                                               fill 0:%d 0:%d 0:%d &\n',diff(l(2,:)),diff(l(3,:)),diff(l(1,:)));
if strcmp(simulation.Density,'Lib')==1
   M = flipud(TPlan.plan.Mat_HU(l(2,1):l(2,2),l(1,1):l(1,2),l(3,1):l(3,2)));
else
   M=flipud(single(TPlan.plan.Mat_HU(l(2,1):l(2,2),l(1,1):l(1,2),l(3,1):l(3,2)))*290 ...
       +round(TPlan.plan.Density(l(2,1):l(2,2),l(1,1):l(1,2),l(3,1):l(3,2))*100));
   %
   for i=1:100  
     if strcmp(TPlan.plan.materials.(sprintf('mat%.0f',i)).tissue,'no')==1 ...   
          && isempty(find(TPlan.plan.Mat_HU==i,1))==0  
          %
          M(TPlan.plan.Mat_HU(l(2,1):l(2,2),l(1,1):l(1,2),l(3,1):l(3,2))==i)=i+30000;   
          %
     end
   end
   %
end
M=permute(M,[1 3 2]);
x=reshape(M,1,[]);
rep=find(diff([-1 x -1]) ~= 0);
for i=1:size(rep,2)-1
  if rep(i+1)-rep(i)>1
     fprintf(tr,'%d %dr ', x(rep(i)),rep(i+1)-rep(i)-1);  
  else
     fprintf(tr,'%d ', x(rep(i)));  
  end
  if mod(i,8)==0 && i<size(rep,2)-1; fprintf(tr,'&\n '); end
end
%
fprintf(tr,'\nc\nc Universes ------------------\n');
%
% universes
M = TPlan.plan.Mat_HU;
%
if strcmp(simulation.Density,'Lib')==1 
   %
   for i=1:100 
     if isempty(find(TPlan.plan.Mat_HU==i,1))==0    
        d=TPlan.plan.materials.(sprintf('mat%.0f',i)).density;
        fprintf(tr,'    %d %d  %.6f -606 605 -604 603 -602 601      u=%d imp:p=1 imp:e=1\n', i*2+999,i,-d, i); 
     end
   end
   %
else
   for i=1:100  
       if strcmp(TPlan.plan.materials.(sprintf('mat%.0f',i)).tissue,'no')==1  ...
               && isempty(find(TPlan.plan.Mat_HU==i,1))==0 
         %
         d=TPlan.plan.materials.(sprintf('mat%.0f',i)).density;
         fprintf(tr,'    %d %d  %.6f -606 605 -604 603 -602 601         u=%d imp:p=1\n', i*290,i,-d, i+30000);  
         %
       elseif isempty(find(TPlan.plan.Mat_HU==i,1))==0 
         for j=1:290  
             d=j*0.01;   
             fprintf(tr,'    %d %d  %.6f -606 605 -604 603 -602 601      u=%d imp:p=1\n', i*290+j+2000,i,-d,i*290+j);   
         end
       end
    end
end
%
end
%
%

function [] = write_tally(tr,simulation,TPlan)
%
%
if size(simulation.Score,2)>=4 && strcmp(simulation.Score{4},'Dose')==1
    % Construct a questdlg with three options
    choice = questdlg('Pick one of the options?', ...
	    '*F8', ...
	    'Region','Contour','Material','Region');
        % Handle response
        switch choice
            case 'Region'
                % check if tally is smaller than phantom 
                ch =  TPlan.plan.dosegrid - TPlan.plan.phantom;
                %
                Limits(:,1) = ch(:,1);
                Limits(:,2) = diff(TPlan.plan.dosegrid')'-1;
                fprintf(tr,'c F8 TALLY - \n');     
                fprintf(tr,'*F8:p (500<600[%d:%d %d:%d %d:%d]) \nc\n',Limits(2,1),Limits(2,2)+Limits(2,1), ...
                                                                      Limits(1,1),Limits(1,2)+Limits(1,1), ...
                                                                      Limits(3,1),Limits(3,2)+Limits(3,1));   
            %
            case 'Contour'
                % check contour names
                c_name=fieldnames(TPlan.struct.contours);
                idx=0;
                for i=1:length(c_name)
                  str{i} = TPlan.struct.contours.(sprintf('Item_%d',i)).Name;
                end
                %
                % select a contour
                [s,~] = listdlg('PromptString','Select a contour:',...
                                'SelectionMode','single',...
                                'ListString',str);
                %
                if isempty(s)==1
                    return;
                else
                    [I(:,1),I(:,2),I(:,3)]             = ind2sub(size(TPlan.struct.contours.(sprintf('Item_%d',i)).Mask),find(TPlan.struct.contours.(sprintf('Item_%d',i)).Mask>0)); 
                end
                %
                if exist('I','var')==1 && isempty(I)==0
                  fprintf(tr,'c F8 CONTOUR TALLY - \n'); 
                  % MCNP coordinates are not the same as matlab 
                  % reference voxel idx is also related to the phantom
                  I(:,1) = I(:,1)-TPlan.plan.phantom(1,1)-1; % MCNP start with 0 matlab with 1
                  I(:,2) = I(:,2)-TPlan.plan.phantom(2,1)-1;
                  I(:,3) = I(:,3)-TPlan.plan.phantom(3,1)-1;
                  %
                  for i=1:size(I,1)
                      if i==1
                          fprintf(tr,'*F8:p  (500<600[%d %d %d]) \n',I(i,1),I(i,2),I(i,3));
                      else
                          fprintf(tr,'       (500<600[%d %d %d]) \n',I(i,1),I(i,2),I(i,3));
                      end
                  end
                end
            %    
            %    
            case 'Material'
                warndlg('Option not available in this version !');
        end                                              
end
end


% 
function write_source(tr)
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c Source \n');
    fprintf(tr,'mode p e\n'); 
    fprintf(tr,'ssr old = 9999 tr=7\n');
    fprintf(tr,'c \n');
end

%
function write_material(tr,TPlan)
fprintf(tr,'c MATERIALS ---------------------------------------\n');
    fprintf(tr,'c composition of the collimators d=17.85\n');
    fprintf(tr,'m1001  26000. -0.925\n');
    fprintf(tr,'       28000. -0.025\n');
    fprintf(tr,'       29000. -0.025\n');
    fprintf(tr,'       74000. -0.025\n');
    %
    for i=1:100
       if isempty(find(TPlan.plan.Mat_HU==i,1))==0
           fprintf(tr,'%s\n',TPlan.plan.materials.(sprintf('mat%.0f',i)).composition{1});
           fprintf(tr,'m%d',i);
           fprintf(tr,'%s\n',TPlan.plan.materials.(sprintf('mat%.0f',i)).composition{2:end});
       end
    end
end

function [] = write_linac_tr(tr,field)
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c LINAC HEAD - GANTRY\n');
    fprintf(tr,'tr1*    %3.2f  %3.2f  %3.2f  &\n',field.TR_1(1), field.TR_1(2), field.TR_1(3));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_1(4), field.TR_1(5), field.TR_1(6));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_1(7), field.TR_1(8), field.TR_1(9));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_1(10),field.TR_1(11),field.TR_1(12));
    %
    fprintf(tr,'c LINAC HEAD - COLLIMATOR\n');
    fprintf(tr,'tr2*    %3.2f  %3.2f  %3.2f  &\n',field.TR_2(1), field.TR_2(2), field.TR_2(3));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_2(4), field.TR_2(5), field.TR_2(6));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_2(7), field.TR_2(8), field.TR_2(9));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_2(10),field.TR_2(11),field.TR_2(12));
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c JAW 1 - X\n');
    fprintf(tr,'tr3*    %3.2f  %3.2f  %3.2f  &\n',field.TR_3(1), field.TR_3(2), field.TR_3(3));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_3(4), field.TR_3(5), field.TR_3(6));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_3(7), field.TR_3(8), field.TR_3(9));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_3(10),field.TR_3(11),field.TR_3(12));
    fprintf(tr,'c\n');
    fprintf(tr,'c JAW 2 - X\n');
    fprintf(tr,'tr4*    %3.2f  %3.2f  %3.2f  &\n',field.TR_4(1), field.TR_4(2), field.TR_4(3));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_4(4), field.TR_4(5), field.TR_4(6));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_4(7), field.TR_4(8), field.TR_4(9));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_4(10),field.TR_4(11),field.TR_4(12));
    fprintf(tr,'c JAW 3 - Y\n');
    fprintf(tr,'tr5*    %3.2f  %3.2f  %3.2f  &\n',field.TR_5(1), field.TR_5(2), field.TR_5(3));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_5(4), field.TR_5(5), field.TR_5(6));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_5(7), field.TR_5(8), field.TR_5(9));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_5(10),field.TR_5(11),field.TR_5(12));
    fprintf(tr,'c JAW 4 - Y\n');
    fprintf(tr,'tr6*    %3.2f  %3.2f  %3.2f  &\n',field.TR_6(1), field.TR_6(2), field.TR_6(3));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_6(4), field.TR_6(5), field.TR_6(6));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_6(7), field.TR_6(8), field.TR_6(9));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_6(10),field.TR_6(11),field.TR_6(12));
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c SOURCE TR\n');
    fprintf(tr,'tr7*    %3.2f  %3.2f  %3.2f  &\n',field.TR_7(1), field.TR_7(2), field.TR_7(3));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_7(4), field.TR_7(5), field.TR_7(6));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_7(7), field.TR_7(8), field.TR_7(9));
    fprintf(tr,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_7(10),field.TR_7(11),field.TR_7(12));
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    fprintf(tr,'c Phantom TR\n');
    fprintf(tr,'tr8*    %3.2f  %3.2f  %3.2f   \n',field.TR_8(1)/10,field.TR_8(2)/10,field.TR_8(3)/10);
    fprintf(tr,'c ---------------------------------------------------------------------------80\n');
    %
    if   isfield(field,'MLC')==0
           MLC(1:60)  =  10;
           MLC(61:120)= -10;
    else   % need to sort it to follow MCNP order -- easier to create TR but does not match tps
           MLC(61:90) = field.MLC(31:60)/10;
           MLC(91:120)= flip(field.MLC(1:30))/10;
           MLC(1:30)  = field.MLC(91:120)/10;
           MLC(31:60) = flip(field.MLC(61:90))/10;
    end
    %
    fprintf(tr,'tr20*    %.4f 0.0000    -46.9700\n',MLC(1));  
    fprintf(tr,'tr21*    %.4f 0.0000    -46.9700\n',MLC(2));  
    fprintf(tr,'tr22*    %.4f 0.5000    -46.9700\n',MLC(3));  
    fprintf(tr,'tr23*    %.4f 0.5000    -46.9700\n',MLC(4));  
    fprintf(tr,'tr24*    %.4f 1.0000    -46.9700\n',MLC(5));  
    fprintf(tr,'tr25*    %.4f 1.0000    -46.9700\n',MLC(6));  
    fprintf(tr,'tr26*    %.4f 1.5000    -46.9700\n',MLC(7));  
    fprintf(tr,'tr27*    %.4f 1.5000    -46.9700\n',MLC(8));  
    fprintf(tr,'tr28*    %.4f 2.0000    -46.9700\n',MLC(9));  
    fprintf(tr,'tr29*    %.4f 2.0000    -46.9700\n',MLC(10));  
    fprintf(tr,'tr30*    %.4f 2.5000    -46.9700\n',MLC(11));  
    fprintf(tr,'tr31*    %.4f 2.5000    -46.9700\n',MLC(12));  
    fprintf(tr,'tr32*    %.4f 3.0000    -46.9700\n',MLC(13));  
    fprintf(tr,'tr33*    %.4f 3.0000    -46.9700\n',MLC(14));  
    fprintf(tr,'tr34*    %.4f 3.5000    -46.9700\n',MLC(15));  
    fprintf(tr,'tr35*    %.4f 3.5000    -46.9700\n',MLC(16));  
    fprintf(tr,'tr36*    %.4f 4.0000    -46.9700\n',MLC(17));  
    fprintf(tr,'tr37*    %.4f 4.0000    -46.9700\n',MLC(18));  
    fprintf(tr,'tr38*    %.4f 4.5000    -46.9700\n',MLC(19));  
    fprintf(tr,'tr39*    %.4f 4.5000    -46.9700\n',MLC(20));  
    fprintf(tr,'tr40*    %.4f 0.0000    -46.9700\n',MLC(21));  
    fprintf(tr,'tr41*    %.4f 0.0000    -46.9700\n',MLC(22));  
    fprintf(tr,'tr42*    %.4f 1.0000    -46.9700\n',MLC(23));  
    fprintf(tr,'tr43*    %.4f 1.0000    -46.9700\n',MLC(24));  
    fprintf(tr,'tr44*    %.4f 2.0000    -46.9700\n',MLC(25));  
    fprintf(tr,'tr45*    %.4f 2.0000    -46.9700\n',MLC(26));  
    fprintf(tr,'tr46*    %.4f 3.0000    -46.9700\n',MLC(27));  
    fprintf(tr,'tr47*    %.4f 3.0000    -46.9700\n',MLC(28));  
    fprintf(tr,'tr48*    %.4f 4.0000    -46.9700\n',MLC(29));  
    fprintf(tr,'tr49*    %.4f 4.0000    -46.9700\n',MLC(30));  
    fprintf(tr,'tr50*    %.4f -0.0000   -46.9700\n',MLC(31));  
    fprintf(tr,'tr51*    %.4f -0.0000   -46.9700\n',MLC(32));  
    fprintf(tr,'tr52*    %.4f -0.5000   -46.9700\n',MLC(33));  
    fprintf(tr,'tr53*    %.4f -0.5000   -46.9700\n',MLC(34));  
    fprintf(tr,'tr54*    %.4f -1.0000   -46.9700\n',MLC(35));  
    fprintf(tr,'tr55*    %.4f -1.0000   -46.9700\n',MLC(36));  
    fprintf(tr,'tr56*    %.4f -1.5000   -46.9700\n',MLC(37));  
    fprintf(tr,'tr57*    %.4f -1.5000   -46.9700\n',MLC(38));  
    fprintf(tr,'tr58*    %.4f -2.0000   -46.9700\n',MLC(39));  
    fprintf(tr,'tr59*    %.4f -2.0000   -46.9700\n',MLC(40));  
    fprintf(tr,'tr60*    %.4f -2.5000   -46.9700\n',MLC(41));  
    fprintf(tr,'tr61*    %.4f -2.5000   -46.9700\n',MLC(42));  
    fprintf(tr,'tr62*    %.4f -3.0000   -46.9700\n',MLC(43));  
    fprintf(tr,'tr63*    %.4f -3.0000   -46.9700\n',MLC(44));  
    fprintf(tr,'tr64*    %.4f -3.5000   -46.9700\n',MLC(45));  
    fprintf(tr,'tr65*    %.4f -3.5000   -46.9700\n',MLC(46));  
    fprintf(tr,'tr66*    %.4f -4.0000   -46.9700\n',MLC(47));  
    fprintf(tr,'tr67*    %.4f -4.0000   -46.9700\n',MLC(48));  
    fprintf(tr,'tr68*    %.4f -4.5000   -46.9700\n',MLC(49));  
    fprintf(tr,'tr69*    %.4f -4.5000   -46.9700\n',MLC(50));  
    fprintf(tr,'tr70*    %.4f -0.0000   -46.9700\n',MLC(51));  
    fprintf(tr,'tr71*    %.4f -0.0000   -46.9700\n',MLC(52));  
    fprintf(tr,'tr72*    %.4f -1.0000   -46.9700\n',MLC(53));  
    fprintf(tr,'tr73*    %.4f -1.0000   -46.9700\n',MLC(54));  
    fprintf(tr,'tr74*    %.4f -2.0000   -46.9700\n',MLC(55));  
    fprintf(tr,'tr75*    %.4f -2.0000   -46.9700\n',MLC(56));  
    fprintf(tr,'tr76*    %.4f -3.0000   -46.9700\n',MLC(57));  
    fprintf(tr,'tr77*    %.4f -3.0000   -46.9700\n',MLC(58));  
    fprintf(tr,'tr78*    %.4f -4.0000   -46.9700\n',MLC(59));  
    fprintf(tr,'tr79*    %.4f -4.0000   -46.9700\n',MLC(60)); 
    fprintf(tr,'tr80*    %.4f  0.0000   -46.9700\n',MLC(61));
    fprintf(tr,'tr81*    %.4f  0.0000   -46.9700\n',MLC(62));  
    fprintf(tr,'tr82*    %.4f  0.5000   -46.9700\n',MLC(63));
    fprintf(tr,'tr83*    %.4f  0.5000   -46.9700\n',MLC(64));  
    fprintf(tr,'tr84*    %.4f  1.0000   -46.9700\n',MLC(65));  
    fprintf(tr,'tr85*    %.4f  1.0000   -46.9700\n',MLC(66));  
    fprintf(tr,'tr86*    %.4f  1.5000   -46.9700\n',MLC(67));  
    fprintf(tr,'tr87*    %.4f  1.5000   -46.9700\n',MLC(68));  
    fprintf(tr,'tr88*    %.4f  2.0000   -46.9700\n',MLC(69));  
    fprintf(tr,'tr89*    %.4f  2.0000   -46.9700\n',MLC(70));  
    fprintf(tr,'tr90*    %.4f  2.5000   -46.9700\n',MLC(71));  
    fprintf(tr,'tr91*    %.4f  2.5000   -46.9700\n',MLC(72));  
    fprintf(tr,'tr92*    %.4f  3.0000   -46.9700\n',MLC(73));  
    fprintf(tr,'tr93*    %.4f  3.0000   -46.9700\n',MLC(74));  
    fprintf(tr,'tr94*    %.4f  3.5000   -46.9700\n',MLC(75));  
    fprintf(tr,'tr95*    %.4f  3.5000   -46.9700\n',MLC(76));  
    fprintf(tr,'tr96*    %.4f  4.0000   -46.9700\n',MLC(77));  
    fprintf(tr,'tr97*    %.4f  4.0000   -46.9700\n',MLC(78));  
    fprintf(tr,'tr98*    %.4f  4.5000   -46.9700\n',MLC(79));  
    fprintf(tr,'tr99*    %.4f  4.5000   -46.9700\n',MLC(80));  
    fprintf(tr,'tr100*   %.4f  0.0000   -46.9700\n',MLC(81));  
    fprintf(tr,'tr101*   %.4f  0.0000   -46.9700\n',MLC(82));  
    fprintf(tr,'tr102*   %.4f  1.0000   -46.9700\n',MLC(83));  
    fprintf(tr,'tr103*   %.4f  1.0000   -46.9700\n',MLC(84));  
    fprintf(tr,'tr104*   %.4f  2.0000   -46.9700\n',MLC(85));  
    fprintf(tr,'tr105*   %.4f  2.0000   -46.9700\n',MLC(86));  
    fprintf(tr,'tr106*   %.4f  3.0000   -46.9700\n',MLC(87));  
    fprintf(tr,'tr107*   %.4f  3.0000   -46.9700\n',MLC(88));  
    fprintf(tr,'tr108*   %.4f  4.0000   -46.9700\n',MLC(89));  
    fprintf(tr,'tr109*   %.4f  4.0000   -46.9700\n',MLC(90));  
    fprintf(tr,'tr110*   %.4f  -0.0000  -46.9700\n',MLC(91));  
    fprintf(tr,'tr111*   %.4f  -0.0000  -46.9700\n',MLC(92));  
    fprintf(tr,'tr112*   %.4f  -0.5000  -46.9700\n',MLC(93));  
    fprintf(tr,'tr113*   %.4f  -0.5000  -46.9700\n',MLC(94));  
    fprintf(tr,'tr114*   %.4f  -1.0000  -46.9700\n',MLC(95));  
    fprintf(tr,'tr115*   %.4f  -1.0000  -46.9700\n',MLC(96));  
    fprintf(tr,'tr116*   %.4f  -1.5000  -46.9700\n',MLC(97));  
    fprintf(tr,'tr117*   %.4f  -1.5000  -46.9700\n',MLC(98));  
    fprintf(tr,'tr118*   %.4f  -2.0000  -46.9700\n',MLC(99));  
    fprintf(tr,'tr119*   %.4f  -2.0000  -46.9700\n',MLC(100));  
    fprintf(tr,'tr120*   %.4f  -2.5000  -46.9700\n',MLC(101));  
    fprintf(tr,'tr121*   %.4f  -2.5000  -46.9700\n',MLC(102));  
    fprintf(tr,'tr122*   %.4f  -3.0000  -46.9700\n',MLC(103));  
    fprintf(tr,'tr123*   %.4f  -3.0000  -46.9700\n',MLC(104));  
    fprintf(tr,'tr124*   %.4f  -3.5000  -46.9700\n',MLC(105));  
    fprintf(tr,'tr125*   %.4f  -3.5000  -46.9700\n',MLC(106));  
    fprintf(tr,'tr126*   %.4f  -4.0000  -46.9700\n',MLC(107));  
    fprintf(tr,'tr127*   %.4f  -4.0000  -46.9700\n',MLC(108));  
    fprintf(tr,'tr128*   %.4f  -4.5000  -46.9700\n',MLC(109));  
    fprintf(tr,'tr129*   %.4f  -4.5000  -46.9700\n',MLC(110));  
    fprintf(tr,'tr130*   %.4f  -0.0000  -46.9700\n',MLC(111));  
    fprintf(tr,'tr131*   %.4f  -0.0000  -46.9700\n',MLC(112));  
    fprintf(tr,'tr132*   %.4f  -1.0000  -46.9700\n',MLC(113));  
    fprintf(tr,'tr133*   %.4f  -1.0000  -46.9700\n',MLC(114));  
    fprintf(tr,'tr134*   %.4f  -2.0000  -46.9700\n',MLC(115));  
    fprintf(tr,'tr135*   %.4f  -2.0000  -46.9700\n',MLC(116));  
    fprintf(tr,'tr136*   %.4f  -3.0000  -46.9700\n',MLC(117));  
    fprintf(tr,'tr137*   %.4f  -3.0000  -46.9700\n',MLC(118));  
    fprintf(tr,'tr138*   %.4f  -4.0000  -46.9700\n',MLC(119));  
    fprintf(tr,'tr139*   %.4f  -4.0000  -46.9700\n',MLC(120));
    %
end

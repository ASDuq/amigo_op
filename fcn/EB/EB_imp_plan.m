function [] = EB_imp_plan(varargin)
global EB_p
if nargin>0
  % select treatment plan to open
  [File,Folder]  =  uigetfile('*.dcm');
else
end
%
if isnumeric(Folder)==1
    return;
end
%
EB_plan        =  dicominfo([Folder File]);
%
% delete previous fields
F_n = fieldnames(EB_p);
B   = contains(F_n,'Beam_');
if sum(B)>0
    EB_p=rmfield(EB_p,F_n(B));
end
%
% Number of beam
%
EB_p.NBeams  =  size(fieldnames(EB_plan.BeamSequence),1);
cont=1;
for i=1:EB_p.NBeams
 %
 str{cont}=['Beam ' num2str(i)]; 
 cont=cont+1;
 %
 EB_p.(sprintf('Beam_%d',i)).BeamNumber                    =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamNumber;
 EB_p.(sprintf('Beam_%d',i)).BeamName                      =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamName;
 %
 EB_p.(sprintf('Beam_%d',i)).BeamType                      =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamType;
 EB_p.(sprintf('Beam_%d',i)).NumberOfControlPoints         =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).NumberOfControlPoints;
 %
 EB_p.(sprintf('Beam_%d',i)).ControlPointSequence          =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence;
 %
 EB_p.(sprintf('Beam_%d',i)).SourceAxisDistance            =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).SourceAxisDistance;
 %
 if isfield(EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamLimitingDeviceSequence,'Item_3')==1
    EB_p.(sprintf('Beam_%d',i)).LeafPositionBoundaries     =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamLimitingDeviceSequence.Item_3.LeafPositionBoundaries;
 end
 %
 for j=1:EB_p.(sprintf('Beam_%d',i)).NumberOfControlPoints
  %
  str{cont}=['  Control Point   ' num2str(j)]; cont=cont+1;
  EB_p.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).Jaw01   = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDevicePositionSequence.Item_1.LeafJawPositions;
  EB_p.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).Jaw03   = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDevicePositionSequence.Item_2.LeafJawPositions;
  %
  if isfield(EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDevicePositionSequence,'Item_3')==1
    EB_p.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).MLC   = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDevicePositionSequence.Item_3.LeafJawPositions;
  end
  %
  EB_p.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).NominalBeamEnergy          = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.NominalBeamEnergy;
  EB_p.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).DoseRateSet                = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.DoseRateSet;
  EB_p.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).GantryAngle                = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.GantryAngle;
  EB_p.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).GantryRotationDirection    = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.GantryRotationDirection;
  EB_p.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).CollimatorAngle            = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDeviceAngle;
  EB_p.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).Isocenter                  = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDeviceAngle;
  %
  if isfield(EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1,'SourceToSurfaceDistance' )==1
    EB_p.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).SourceToSurfaceDistance  = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.IsocenterPosition;
  end
  %
 end
end
EB_p.gui.info.String = str;
EB_p.gui.info.Value  = 1;
%
% caculate MCNP TR cards
EB_p=mcnp_tr_cards(EB_p);
%
end


function [EB_p] = mcnp_tr_cards(EB_p)
% check all the beams and define the TR cards of each jaw and mlc
for k=1:EB_p.NBeams
   % check each control point
   for j=1:EB_p.(sprintf('Beam_%d',k)).NumberOfControlPoints
     JAW_X      = EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).Jaw01;
     JAW_Y      = EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).Jaw03;
     Gantry     = EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).GantryAngle;
     Collimator = EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).CollimatorAngle;
     %
     % TRCL = o1 o2 o3 xx' yx' zx' xy' yy' zy' xz' yz' zz'
     %
     % LINAC HEAD TR1 - Accounts for gantry 
     TR_1(1:3)   = [0,                 0,             -100]; % reference corrdinate for rotation - isocenter  
     TR_1(4:6)   = [Gantry,           90,        90+Gantry];
     TR_1(7:9)   = [90,                0,               90];
     TR_1(10:12) = [90-Gantry,        90,           Gantry];
     TR_1(13)    = -1;
     %
     % Collimator
     TR_2(1:3)   = [0,                         0,    0];    % reference corrdinate for rotation - isocenter  
     TR_2(4:6)   = [Collimator,    90-Collimator,   90];
     TR_2(7:9)   = [90+Collimator,    Collimator,   90];
     TR_2(10:12) = [90,                       90,    0];
     TR_2(13)    = -1;
     % JAW X  - TR2 and TR3
     JAW_X =atand(JAW_X/1000);                              % mm  - converting to deg to find rotation
     % Positive side
     TR_3(1:3)   = [0,                   0,         36.88]; % reference corrdinate for rotation - isocenter  
     TR_3(4:6)   = [JAW_X(2),           90,   90-JAW_X(2)];
     TR_3(7:9)   = [90,                  0,            90];
     TR_3(10:12) = [90+JAW_X(2),        90,      JAW_X(2)];
     TR_3(13)    = -1;
     % Negative side
     TR_4(1:3)   = [0,                   0,         36.88]; % reference corrdinate for rotation - isocenter  
     TR_4(4:6)   = [JAW_X(1),           90,   90-JAW_X(1)];
     TR_4(7:9)   = [90,                  0,            90];
     TR_4(10:12) = [90+JAW_X(1),        90,      JAW_X(1)];
     TR_4(13)    = -1;
     % JAW Y  - TR4 and TR5
     JAW_Y       = atand(JAW_Y/1000);                       % mm  - converting to deg to find rotation
     % Positive side
     TR_5(1:3)   = [0,             0,         28.16]; % reference corrdinate for rotation - isocenter  
     TR_5(4:6)   = [0,            90,         90.00];
     TR_5(7:9)   = [90,     JAW_Y(2),   90-JAW_Y(2)];
     TR_5(10:12) = [90,  90+JAW_Y(2),      JAW_Y(2)];
     TR_5(13)    = -1;
     % Negative side
     TR_6(1:3)   = [0,             0,         28.16]; % reference corrdinate for rotation - isocenter  
     TR_6(4:6)   = [0,            90,         90.00];
     TR_6(7:9)   = [90,     JAW_Y(1),   90-JAW_Y(1)];
     TR_6(10:12) = [90,  90+JAW_Y(1),      JAW_Y(1)];
     TR_6(13)    = -1;
     %
     EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_1=TR_1;
     EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_2=TR_2;
     EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_3=TR_3;
     EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_4=TR_4;
     EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_5=TR_5;
     EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_6=TR_6;
     %
     % check if there is a MLC field ...
     if isfield(EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)),'MLC')==1
         MLC = EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).MLC;
         for i=1:size(MLC,1)
         end
     end   
     %
%      % TR source - granty and collimator in one transformation
%      % x' pos 
%      x(1) =   cosd(Gantry)*cosd(Collimator);
%      x(2) =   cosd(Gantry)*sind(Collimator);
%      x(3) =  -sind(Gantry);
%      % y' pos
%      y(1) =  -sind(Collimator)*cosd(Gantry);
%      y(2) =   cosd(Collimator)*cosd(Gantry);
%      y(3) =  -sind(Gantry);
%      % z' pos
%      z(1) = sind(Gantry)*cosd(Collimator);
%      z(2) = sind(Gantry)*sind(Collimator);
%      z(3) = cosd(Gantry);
%      %
%      TR_source(4)=acosd(dot(x,[1,0,0]));
%      TR_source(5)=acosd(dot(x,[0,1,0]));
%      TR_source(6)=acosd(dot(x,[0,0,1]));
%      %
%      TR_source(7)=acosd(dot(y,[1,0,0]));
%      TR_source(8)=acosd(dot(y,[0,1,0]));
%      TR_source(9)=acosd(dot(y,[0,0,1]));
%      %
%      TR_source(10)=acosd(dot(z,[1,0,0]));
%      TR_source(11)=acosd(dot(z,[0,1,0]));
%      TR_source(12)=acosd(dot(z,[0,0,1]));
%
     % SOURCE TR 7
     TR_7(1:3)   = [0,                 0,            -46.3]; % reference corrdinate for rotation - isocenter  
     TR_7(4:6)   = [Gantry,           90,        90+Gantry];
     TR_7(7:9)   = [90,                0,               90];
     TR_7(10:12) = [90-Gantry,        90,           Gantry];
     TR_7(13)    = -1;
     %
     EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_7=TR_7;
   end
end
%

end
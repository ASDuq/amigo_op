%%% Read results condor
%
% Mctal 
%
if ispc==1
    fol='\\172.18.8.29\gabriel_condor\';
else
    fol='/mnt/condor_work/MCNP6/';
end
%
folds=dir([fol 'pr*']);
%
for i=1:size(folds,1)
   r_mctal([fol folds(i).name filesep],struct); 
   disp([fol folds(i).name filesep]);
end
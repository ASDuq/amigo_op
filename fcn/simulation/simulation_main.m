function [simulation] = simulation_main()
% get simulation info
%
% parameters from user
simulation   = get_sim_info;
%
if isnumeric(simulation)==1
    return;
end
% convert dwell positions 
TPlan=getappdata(findobj('Tag','amb_interface'),'TPlan');
%
if isfield(TPlan.plan,'dosegrid')==1
  TPlan.plan.dosegrid     = double(TPlan.plan.dosegrid);
end
TPlan.plan.phantom        = double(TPlan.plan.phantom);
%
if strcmp(get(findobj('Tag','no_source'),'Checked'),'off')==1
  simulation   = convert_dwellpos(TPlan,simulation);
end
%
[File,Folder]= uiputfile('*.inp');
if size(Folder,2)==1
    return;
end
%
tr           = fopen([Folder File],'Wb+');
%
w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
%
write_header(tr,simulation,TPlan);           set(w_bar.hand,'Visible',1,'Value',40);
write_surfaces(tr,simulation,TPlan);         set(w_bar.hand,'Visible',1,'Value',70);
write_source(tr,simulation,TPlan);           set(w_bar.hand,'Visible',1,'Value',80);
write_tally(tr,simulation,TPlan);            set(w_bar.hand,'Visible',1,'Value',90);
write_material(tr,simulation,TPlan);         set(w_bar.hand,'Visible',1,'Value',95);
%
fprintf(tr,'PHYS:P\n');
fprintf(tr,'cut:p j 0.001\n');
fprintf(tr,'prdmp j 1e+9 j j\n');
fprintf(tr,'dbcn 17j 1 30j 2\n');
%
fprintf(tr,'nps %d\n',simulation.Nps);
fclose(tr);
set(w_bar.hand,'Visible',0,'Value',0);
fclose('all');
end
%
function [] = write_tally(tr,simulation,TPlan)
%
%
if strcmp(simulation.Score{1},'Dwm')==1
     fprintf(tr,'c ------------ FMESH ----------\n');
     fprintf(tr,'*FMESH4:p GEOM=xyz ORIGIN= %.16f %.16f &\n', (TPlan.plan.dosegrid(3,1)-1)*(TPlan.image.Resolution(3)/10), ...
                                                              (TPlan.plan.dosegrid(1,1)-1)*(TPlan.image.Resolution(1)/10));
     fprintf(tr,'     %.16f IMESH=%3.16f\n',                  (TPlan.plan.dosegrid(2,1)-1)*(TPlan.image.Resolution(2)/10), ...
                                                               TPlan.plan.dosegrid(3,2)   *(TPlan.image.Resolution(3)/10));
     fprintf(tr,'     IINTS=%1.0i JMESH=%3.16f\n',             diff(TPlan.plan.dosegrid(3,1:2))+1, ...
                                                               TPlan.plan.dosegrid(1,2)   *(TPlan.image.Resolution(1)/10));
     fprintf(tr,'     JINTS=%1.0i KMESH=%3.16f KINTS=%1.0i OUT=jk \nc\n', diff(TPlan.plan.dosegrid(1,1:2))+1, ...
                                                                          TPlan.plan.dosegrid(2,2)*(TPlan.image.Resolution(2)/10), ...
                                                                          diff(TPlan.plan.dosegrid(2,1:2))+1);
     %
     write_water_coef(tr);
end
%
if size(simulation.Score,2)>=2 && strcmp(simulation.Score{2},'Dmm')==1
  %
  fprintf(tr,'c ------------ FMESH ----------\n');
  fprintf(tr,'FMESH14:p GEOM=xyz ORIGIN= %.16f %.16f &\n', (TPlan.plan.dosegrid(3,1)-1)*(TPlan.image.Resolution(3)/10), ...
                                                           (TPlan.plan.dosegrid(1,1)-1)*(TPlan.image.Resolution(1)/10));
  fprintf(tr,'     %.16f IMESH=%3.16f\n',                  (TPlan.plan.dosegrid(2,1)-1)*(TPlan.image.Resolution(2)/10), ...
                                                            TPlan.plan.dosegrid(3,2)   *(TPlan.image.Resolution(3)/10));
  fprintf(tr,'     IINTS=%1.0i JMESH=%3.16f\n',             diff(TPlan.plan.dosegrid(3,1:2))+1, ...
                                                            TPlan.plan.dosegrid(1,2)   *(TPlan.image.Resolution(1)/10));
  fprintf(tr,'     JINTS=%1.0i KMESH=%3.16f KINTS=%1.0i OUT=jk \nc\n', diff(TPlan.plan.dosegrid(1,1:2))+1, ...
                                                                       TPlan.plan.dosegrid(2,2) *(TPlan.image.Resolution(2)/10), ...
                                                                       diff(TPlan.plan.dosegrid(2,1:2))+1); 
  %
  fprintf(tr,'FM14 -1 -0 -5 -6\n');
  %
end
%
%
if size(simulation.Score,2)>=3 && strcmp(simulation.Score{3},'Mean')==1
   %
     fprintf(tr,'c ------------ FMESH ----------\n');
     fprintf(tr,'*FMESH24:p GEOM=xyz ORIGIN= %.16f %.16f &\n', (TPlan.plan.dosegrid(3,1)-1)*(TPlan.image.Resolution(3)/10), ...
                                                              (TPlan.plan.dosegrid(1,1)-1)*(TPlan.image.Resolution(1)/10));
     fprintf(tr,'     %.16f IMESH=%3.16f\n',                  (TPlan.plan.dosegrid(2,1)-1)*(TPlan.image.Resolution(2)/10), ...
                                                               TPlan.plan.dosegrid(3,2)   *(TPlan.image.Resolution(3)/10));
     fprintf(tr,'     IINTS=%1.0i JMESH=%3.16f\n',             diff(TPlan.plan.dosegrid(3,1:2))+1, ...
                                                               TPlan.plan.dosegrid(1,2)   *(TPlan.image.Resolution(1)/10));
     fprintf(tr,'     JINTS=%1.0i KMESH=%3.16f KINTS=%1.0i OUT=jk \nc\n', diff(TPlan.plan.dosegrid(1,1:2))+1, ...
                                                                          TPlan.plan.dosegrid(2,2) *(TPlan.image.Resolution(2)/10), ...
                                                                          diff(TPlan.plan.dosegrid(2,1:2))+1);
     %
     fprintf(tr,'FMESH34:p GEOM=xyz ORIGIN= %.16f %.16f &\n', (TPlan.plan.dosegrid(3,1)-1)*(TPlan.image.Resolution(3)/10), ...
                                                              (TPlan.plan.dosegrid(1,1)-1)*(TPlan.image.Resolution(1)/10));
     fprintf(tr,'     %.16f IMESH=%3.16f\n',                  (TPlan.plan.dosegrid(2,1)-1)*(TPlan.image.Resolution(2)/10), ...
                                                               TPlan.plan.dosegrid(3,2)   *(TPlan.image.Resolution(3)/10));
     fprintf(tr,'     IINTS=%1.0i JMESH=%3.16f\n',             diff(TPlan.plan.dosegrid(3,1:2))+1, ...
                                                               TPlan.plan.dosegrid(1,2)   *(TPlan.image.Resolution(1)/10));
     fprintf(tr,'     JINTS=%1.0i KMESH=%3.16f KINTS=%1.0i OUT=jk \nc\n', diff(TPlan.plan.dosegrid(1,1:2))+1, ...
                                                                          TPlan.plan.dosegrid(2,2) *(TPlan.image.Resolution(2)/10), ...
                                                                          diff(TPlan.plan.dosegrid(2,1:2))+1);
    %
end
%
%
if size(simulation.Score,2)>=4 && strcmp(simulation.Score{4},'Dose')==1
    % Construct a questdlg with three options
    choice = questdlg('Pick one of the options?', ...
	    '*F8', ...
	    'Region','Contour','Material','Region');
        % Handle response
        switch choice
            case 'Region'
                % check if tally is smaller than phantom 
                ch =  TPlan.plan.dosegrid - TPlan.plan.phantom;
                %
                if strcmp(simulation.Voxel,'Off')==1
                    errordlg({'Dose to voxels cannot be calculated without a voxel phantom','Check the simulation parameters'});
                end
                %
                if sum(ch(:,1)<0)>0 || sum(ch(:,2)>0)>0
                    errordlg({'Adjust the dose grid !','It MUST be smaller than the voxel phantom'});
                    fclose(tr);
                    return;
                end
                Limits(:,1) = ch(:,1);
                Limits(:,2) = diff(TPlan.plan.dosegrid')'-1;
                fprintf(tr,'c F8 TALLY - \n');     
                fprintf(tr,'*F8:p (500<600[%d:%d %d:%d %d:%d]) \nc\n',Limits(2,1),Limits(2,2)+Limits(2,1), ...
                                                                      Limits(1,1),Limits(1,2)+Limits(1,1), ...
                                                                      Limits(3,1),Limits(3,2)+Limits(3,1));   
            %
            %  
            %
            %
            %
            case 'Contour'
                % check contour names
                c_name=fieldnames(TPlan.struct.contours);
                idx=0;
                for i=1:length(c_name)
                  str{i} = TPlan.struct.contours.(sprintf('Item_%d',i)).Name;
                end
                %
                % select a contour
                [s,~] = listdlg('PromptString','Select a contour:',...
                                'SelectionMode','single',...
                                'ListString',str);
                %
                if isempty(s)==1
                    return;
                else
                    [I(:,1),I(:,2),I(:,3)]             = ind2sub(size(TPlan.struct.contours.(sprintf('Item_%d',i)).Mask),find(TPlan.struct.contours.(sprintf('Item_%d',i)).Mask>0)); 
                end
                %
                if exist('I','var')==1 && isempty(I)==0
                  fprintf(tr,'c F8 CONTOUR TALLY - \n'); 
                  % MCNP coordinates are not the same as matlab 
                  % reference voxel idx is also related to the phantom
                  I(:,1) = I(:,1)-TPlan.plan.phantom(1,1)-1; % MCNP start with 0 matlab with 1
                  I(:,2) = I(:,2)-TPlan.plan.phantom(2,1)-1;
                  I(:,3) = I(:,3)-TPlan.plan.phantom(3,1)-1;
                  %
                  for i=1:size(I,1)
                      if i==1
                          fprintf(tr,'*F8:p  (500<600[%d %d %d]) \n',I(i,1),I(i,2),I(i,3));
                      else
                          fprintf(tr,'       (500<600[%d %d %d]) \n',I(i,1),I(i,2),I(i,3));
                      end
                  end
                end
            %    
            %    
            case 'Material'
                warndlg('Option not available in this version !');
        end

                                               
end
end

function [] = write_water_coef(tr)
        fprintf(tr,'#  de4     df4\n');            % mass energy coeff. for water (NIST)
        fprintf(tr,'   0.0010   4065.0000\n');
        fprintf(tr,'   0.0015   1372.0000\n');
        fprintf(tr,'   0.0020    615.2000\n');
        fprintf(tr,'   0.0030    191.7000\n');
        fprintf(tr,'   0.0040    81.9100\n');
        fprintf(tr,'   0.005    41.88000\n   0.006    24.05000\n');
        fprintf(tr,'   0.008     9.91500\n   0.010     4.94400\n');
        fprintf(tr,'   0.015     1.37400\n   0.020     0.55030\n');
        fprintf(tr,'   0.030     0.15570\n   0.040     0.06947\n');
        fprintf(tr,'   0.050     0.04223\n   0.060     0.03190\n');
        fprintf(tr,'   0.080     0.02597\n   0.100     0.02546\n');
        fprintf(tr,'   0.150     0.02764\n   0.200     0.02967\n');
        fprintf(tr,'   0.300     0.03192\n   0.400     0.03279\n');
        fprintf(tr,'   0.500     0.03299\n   0.600     0.03284\n');
        fprintf(tr,'   0.800     0.03206\n   1.000     0.03103\n');
        fprintf(tr,'   1.250     0.02965\n   1.500     0.02833\nc\n');
end


%
function [] = write_phantom(tr,simulation,TPlan)
%
% check material map
if isfield(TPlan.plan,'Mat_HU')==0
    errodlg('Create a material map first!');
    return
end
l=TPlan.plan.phantom;
fprintf(tr,'600 0       -606 605 -604 603 -602 601     lat=1 u=999 imp:p=1\n');
fprintf(tr,'                                               fill 0:%d 0:%d 0:%d &\n',diff(l(2,:)),diff(l(1,:)),diff(l(3,:)));
if strcmp(simulation.Density,'Lib')==1
   M = TPlan.plan.Mat_HU(l(2,1):l(2,2),l(1,1):l(1,2),l(3,1):l(3,2));
else
   M=single(TPlan.plan.Mat_HU(l(2,1):l(2,2),l(1,1):l(1,2),l(3,1):l(3,2)))*290 ...
       +round(TPlan.plan.Density(l(2,1):l(2,2),l(1,1):l(1,2),l(3,1):l(3,2))*100);
   %
   for i=1:100  
     if strcmp(TPlan.plan.materials.(sprintf('mat%.0f',i)).tissue,'no')==1 ...   
          && isempty(find(TPlan.plan.Mat_HU==i,1))==0  
          %
          M(TPlan.plan.Mat_HU(l(2,1):l(2,2),l(1,1):l(1,2),l(3,1):l(3,2))==i)=i+30000;   
          %
     end
   end
   %
end
x=reshape(M,1,[]);
rep=find(diff([-1 x -1]) ~= 0);
for i=1:size(rep,2)-1
  if rep(i+1)-rep(i)>1
     fprintf(tr,'%d %dr ', x(rep(i)),rep(i+1)-rep(i)-1);  
  else
     fprintf(tr,'%d ', x(rep(i)));  
  end
  if mod(i,8)==0 && i<size(rep,2)-1; fprintf(tr,'&\n '); end
end
%
fprintf(tr,'\nc\nc Universes ------------------\n');
%
% universes
M = TPlan.plan.Mat_HU;
%
if strcmp(simulation.Density,'Lib')==1 
   %
   for i=1:100 
     if isempty(find(TPlan.plan.Mat_HU==i,1))==0    
        d=TPlan.plan.materials.(sprintf('mat%.0f',i)).density;
        fprintf(tr,'    %d %d  %.6f -606 605 -604 603 -602 601         u=%d imp:p=1\n', i*2+999,i,-d, i); 
     end
   end
   %
else
   for i=1:100  
       if strcmp(TPlan.plan.materials.(sprintf('mat%.0f',i)).tissue,'no')==1  ...
               && isempty(find(TPlan.plan.Mat_HU==i,1))==0 
         %
         d=TPlan.plan.materials.(sprintf('mat%.0f',i)).density;
         fprintf(tr,'    %d %d  %.6f -606 605 -604 603 -602 601         u=%d imp:p=1\n', i*290,i,-d, i+30000);  
         %
       elseif isempty(find(TPlan.plan.Mat_HU==i,1))==0 
         for j=1:290  
             d=j*0.01;   
             fprintf(tr,'    %d %d  %.6f -606 605 -604 603 -602 601      u=%d imp:p=1\n', i*290+j+2000,i,-d,i*290+j);   
         end
       end
    end
end
%
end
%
%



function write_material(tr,simulation,TPlan)
fprintf(tr,'c MATERIALS ---------------------------------------\n');
if strcmp(simulation.Voxel,'Off')==1
    fprintf(tr,'c material composition of Air - (rho=0.001205)\n');
    fprintf(tr,'  m1     6000. -0.000125   7000. -0.755267    8000. -0.231781\n');
    fprintf(tr,'        18000. -0.012827\nc\n');
    fprintf(tr,'c material composition of water (rho=-1.0)\n');
    fprintf(tr,'  m5     1000.  2     8000.  1\n');
elseif strcmp(simulation.Voxel,'On')==1
    for i=1:100
       if isempty(find(TPlan.plan.Mat_HU==i,1))==0
           fprintf(tr,'%s\n',TPlan.plan.materials.(sprintf('mat%.0f',i)).composition{1});
           fprintf(tr,'m%d',i);
           fprintf(tr,'%s\n',TPlan.plan.materials.(sprintf('mat%.0f',i)).composition{2:end});
       end
    end
end
end

%
function [] = write_surfaces(tr,simulation,TPlan)
TPlan.plan.phantom=double(TPlan.plan.phantom);
if strcmp(simulation.Voxel,'Off')==1 % water phantom    
   fprintf(tr,'c  Phant.\n');
   fprintf(tr,'997  5 -1.0   994 -995 996 -997 998 -999         imp:p=1\n');
   fprintf(tr,'c  Region of interest\n');
   fprintf(tr,'998  5 -1.0   971 -972 973 -974 975 -976 #997    imp:p=1\n');
   fprintf(tr,'c  Region of no interest\n');
   fprintf(tr,'999  0    -971:972:-973:974:-975:976             imp:p=0\n');
   % ---------------------------------------------------------------------------------- 
   % Surfaces
   % Source surfaces
   load source.mat;
   % source surfaces 
   fprintf(tr,'\nc  Source\n');
   if     strcmp(simulation.Source,'MicroSelectronv2') == 1
        fprintf(tr,'%s\n',source.MicroSelectron.surfaces{1:end});
   elseif strcmp(simulation.Source,'GammaMedPlus')     == 1
        fprintf(tr,'%s\n',source.Gammamed_Plus.surfaces{1:end});
   end
   % 
   fprintf(tr,'c  Edge\n'); 
   fprintf(tr,'   971 px  %.6f\n',(TPlan.plan.phantom(3,1)-1)*TPlan.image.Resolution(3)/10-20);
   fprintf(tr,'   972 px  %.6f\n', TPlan.plan.phantom(3,2)   *TPlan.image.Resolution(3)/10+20);
   fprintf(tr,'   973 py  %.6f\n',(TPlan.plan.phantom(2,1)-1)*TPlan.image.Resolution(2)/10-20);
   fprintf(tr,'   974 py  %.6f\n', TPlan.plan.phantom(2,2)   *TPlan.image.Resolution(2)/10+20);
   fprintf(tr,'   975 pz  %.6f\n',(TPlan.plan.phantom(1,1)-1)*TPlan.image.Resolution(1)/10-20);
   fprintf(tr,'   976 pz  %.6f\n', TPlan.plan.phantom(1,2)   *TPlan.image.Resolution(1)/10+20); 
   fprintf(tr,'c  Phanton\n');
   fprintf(tr,'   994 px  %.6f\n',  (TPlan.plan.phantom(3,1)-1)*TPlan.image.Resolution(3)/10);
   fprintf(tr,'   995 px  %.6f\n',   TPlan.plan.phantom(3,2)   *TPlan.image.Resolution(3)/10);
   fprintf(tr,'   996 py  %.6f\n',  (TPlan.plan.phantom(2,1)-1)*TPlan.image.Resolution(2)/10);
   fprintf(tr,'   997 py  %.6f\n',   TPlan.plan.phantom(2,2)   *TPlan.image.Resolution(2)/10);
   fprintf(tr,'   998 pz  %.6f\n',  (TPlan.plan.phantom(1,1)-1)*TPlan.image.Resolution(1)/10);
   fprintf(tr,'   999 pz  %.6f\n\n', TPlan.plan.phantom(1,2)   *TPlan.image.Resolution(1)/10);           
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
elseif strcmp(simulation.Voxel,'On')==1 % Voxel phantom 
    fprintf(tr,'c  Phant.\n');
    fprintf(tr,'    500 0        501 -502 503 -504 505 -506      fill=999  imp:p=1\n');
    % -------------------------------------------------------------------------------------------------    
    % Voxel Phantom
    write_phantom(tr, simulation, TPlan);
    % region of interet 
    fprintf(tr,'c  Region of interest\n');
    fprintf(tr,'    998  1 -0.0012  971 -972 973 -974 975 -976 #500   imp:p=1\n');
    % ------------------------------------------------------------------------------------------------------------------------
    % Region of no interest
    fprintf(tr,'c  Region of no interest\n');
    fprintf(tr,'    999  0    -971:972:-973:974:-975:976               imp:p=0\n');
    % Surfaces
    % ------------------------------------------------------------------------------------------------------------------------
    % seed surfaces
    % Source surfaces
    load source.mat;
    % source surfaces 
    fprintf(tr,'\nc  Source\n');
    if     strcmp(simulation.Source,'MicroSelectronv2') == 1
        fprintf(tr,'%s\n',source.MicroSelectron.surfaces{1:end});
    elseif strcmp(simulation.Source,'GammaMedPlus')     == 1
        fprintf(tr,'%s\n',source.Gammamed_Plus.surfaces{1:end});
    elseif strcmp(simulation.Source,'TG-186')     == 1
        fprintf(tr,'%s\n',source.TG186.surfaces{1:end});
    end
    % write the phantom adding 2cm of air around it.
    fprintf(tr,'c  Phanton\n');
    fprintf(tr,'   501 px  %.6f\n',(TPlan.plan.phantom(3,1)-1)*TPlan.image.Resolution(3)/10);
    fprintf(tr,'   502 px  %.6f\n', TPlan.plan.phantom(3,2)   *TPlan.image.Resolution(3)/10);
    fprintf(tr,'   503 py  %.6f\n',(TPlan.plan.phantom(1,1)-1)*TPlan.image.Resolution(1)/10);
    fprintf(tr,'   504 py  %.6f\n', TPlan.plan.phantom(1,2)   *TPlan.image.Resolution(1)/10);
    fprintf(tr,'   505 pz  %.6f\n',(TPlan.plan.phantom(2,1)-1)*TPlan.image.Resolution(2)/10);
    fprintf(tr,'   506 pz  %.6f\n', TPlan.plan.phantom(2,2)   *TPlan.image.Resolution(2)/10);
    fprintf(tr,'c  Lattice\n');
    fprintf(tr,'   601 px   %.6f\n',(TPlan.plan.phantom(3,1)-1)*TPlan.image.Resolution(3)/10);
    fprintf(tr,'   602 px   %.6f\n', TPlan.plan.phantom(3,1)   *TPlan.image.Resolution(3)/10);
    fprintf(tr,'   603 py   %.6f\n',(TPlan.plan.phantom(1,1)-1)*TPlan.image.Resolution(1)/10);
    fprintf(tr,'   604 py   %.6f\n', TPlan.plan.phantom(1,1)   *TPlan.image.Resolution(1)/10);
    fprintf(tr,'   605 pz   %.6f\n',(TPlan.plan.phantom(2,1)-1)*TPlan.image.Resolution(2)/10);
    fprintf(tr,'   606 pz   %.6f\n', TPlan.plan.phantom(2,1)   *TPlan.image.Resolution(2)/10);
    fprintf(tr,'c  Edge\n'); 
    fprintf(tr,'   971 px  %.6f\n',  (TPlan.plan.phantom(3,1)-1)*TPlan.image.Resolution(3)/10-20);
    fprintf(tr,'   972 px  %.6f\n',   TPlan.plan.phantom(3,2)   *TPlan.image.Resolution(3)/10+20);
    fprintf(tr,'   973 py  %.6f\n',  (TPlan.plan.phantom(1,1)-1)*TPlan.image.Resolution(1)/10-20);
    fprintf(tr,'   974 py  %.6f\n',   TPlan.plan.phantom(1,2)   *TPlan.image.Resolution(1)/10+20);
    fprintf(tr,'   975 pz  %.6f\n',  (TPlan.plan.phantom(2,1)-1)*TPlan.image.Resolution(2)/10-20);
    fprintf(tr,'   976 pz  %.6f\n\n', TPlan.plan.phantom(2,2)   *TPlan.image.Resolution(2)/10+20);
end
end



function [] = write_source(tr, simulation, TPlan)
%
if strcmp(simulation.Source,'none')==1 % External beam
    fprintf(tr,'c ... write your source here ..  \n'); 
    fprintf(tr,'c\nc\n');   
    return
end
% Source ... Tr ... Parameters 
fprintf(tr,'c Source ... Tr ... Parameters  \n');
fprintf(tr,'mode p \n');
%
Total_time=sum(TPlan.plan.dwells(:,4));
if strcmp(simulation.Source,'MicroSelectronv2') == 1
    if size(TPlan.plan.dwells(:,4),1)==1
        fprintf(tr,'SSR  OLD 987 989 990 992 993 tr=1 WGT=%4.3f\n',Total_time);
    else
        fprintf(tr,'SSR  OLD 987 989 990 992 993 tr=D1 WGT=%4.3f\n',Total_time);
    end
elseif strcmp(simulation.Source,'GammaMedPlus')     == 1
    if size(TPlan.plan.dwells(:,4),1)==1            
        fprintf(tr,'SSR  OLD 9 12 13 16 tr=1 WGT=%4.3f\n',Total_time);
    else
        fprintf(tr,'SSR  OLD 9 12 13 16 tr=D1 WGT=%4.3f\n',Total_time);
    end
elseif strcmp(simulation.Source,'TG-186')     == 1
    if size(TPlan.plan.dwells(:,4),1)==1            
        fprintf(tr,'SSR  OLD 5 7 8 tr=1 WGT=%4.3f\n',Total_time);
    else
        fprintf(tr,'SSR  OLD 5 7 8 tr=D1 WGT=%4.3f\n',Total_time);
    end    
end
%
% Print dwell positions ID
cont=1;
fprintf(tr,'SI1  L');
for i=1:size(TPlan.plan.dwells(:,4),1)
   fprintf(tr,' %1.i',i); cont= cont+1;
   if (cont==13 && i~=size(TPlan.plan.dwells,1)); fprintf(tr,' \n     '); cont=1; end   
   if (i==size(TPlan.plan.dwells,1));             fprintf(tr,' \n');  end   
end
fprintf(tr,'c\n');
% Print dwell positions sampling probability
cont=1;
fprintf(tr,'SP1  ');
for i=1:size(TPlan.plan.dwells,1)
    fprintf(tr,' %4.1f',TPlan.plan.dwells(i,4));    cont= cont+1;
    if (cont==13 && i~=size(TPlan.plan.dwells,1));  fprintf(tr,' \n     '); cont=1; end;
    if (i==size(TPlan.plan.dwells,1));              fprintf(tr,' \n');              end;   
end
%
fprintf(tr,'c\n');
for i=1:size(TPlan.plan.dwells,1)
   fprintf(tr,'tr%1.0f %6.5f %6.5f %6.5f ',i,simulation.dwells(i,[3 1 2])); 
   if isempty(find(simulation.dwells(i,6:8)~=0,1))==0
      fprintf(tr,'%6.5f %6.5f %6.5f\n',simulation.dwells(i,6:8));
   else
      fprintf(tr,'\n'); 
   end
end
fprintf(tr,'c\n');
%
end


function []    =  write_header(tr,simulation,TPlan)
%
if isfield(simulation,'Score')==0
    errordlg('Select one of the scoring options');
    fclose(tr);
    return;
end
%
if isfield(TPlan.plan,'TotalAirKerma')==1
  fprintf(tr,'AMIGOBrachy - ');     
  fprintf(tr,'%s ',           simulation.Score{:});
  fprintf(tr,'%s ',           simulation.Source);  
  fprintf(tr,'Kerma %7.2f\n', TPlan.plan.TotalAirKerma);
%
  for i=1:size(simulation.dwells,1)
    fprintf(tr,'c %6.2f %6.2f %6.2f %6.2f\n', simulation.dwells(i,1), ...
                                              simulation.dwells(i,2), ...
                                              simulation.dwells(i,3), ...
                                              simulation.dwells(i,4));                                     
  end
else
  fprintf(tr,'AMIGOBrachy - \n');  
%  
end
%
end






function [simulation]    =  convert_dwellpos(TPlan,simulation)
simulation.dwells(:,1:3) =  bsxfun(@minus,TPlan.plan.dwells(:,1:3),TPlan.image.ImagePositionPatient);
simulation.dwells(:,1:3) =  simulation.dwells(:,1:3)/10; %cm
simulation.dwells(:,4:5) =  TPlan.plan.dwells(:,4:5);
simulation=calculate_source_inclination(TPlan,simulation);
end


function [simulation]    =  calculate_source_inclination(TPlan,simulation)
%
global angles
%
for i=1:size(TPlan.plan.dwells,1)
    cat          =  TPlan.plan.dwells(i,5);
    %
    % find two points of reference            
    %
    if isfield(TPlan.plan,'catheter')==1 && isfield(TPlan.plan.catheter,(sprintf('Cat_%.0f',cat)))==1 && size(TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).Points,1)>1
        %
        cat_cont     =  TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).Points;
        % convert to absulte coordinates
        idx = dsearchn(cat_cont,TPlan.plan.dwells(i,1:3));
        if idx==size(cat_cont,1) || (idx>1 && sqrt(sum(bsxfun(@minus, cat_cont(idx-1,1:3), TPlan.plan.dwells(i,1:3)).^2,2)) < sqrt(sum(bsxfun(@minus, cat_cont(idx-1,1:3), cat_cont(idx,1:3)).^2,2)))
            idx=idx-1; % check if it is after or before the closest point
        end
        %
        C1(1,1:3)   =  cat_cont(idx,1:3);
        C1(2,1:3)   =  cat_cont(idx+1,1:3);
    elseif (isfield(TPlan.plan,'catheter')==0 || isfield(TPlan.plan.catheter,'Points')==0 || size(TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).Points,1)==1)  && i < size(TPlan.plan.dwells,1) % use dwell as reference
        C1(1,1:3)   =  TPlan.plan.dwells(i,1:3);
        C1(2,1:3)   =  TPlan.plan.dwells(i+1,1:3);
    elseif i == size(TPlan.plan.dwells,1) % use dwell as reference
        C1(1,1:3)   =  TPlan.plan.dwells(i-1,1:3);
        C1(2,1:3)   =  TPlan.plan.dwells(i,1:3);        
    end
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% X
    d=((C1(2,3)-C1(1,3))^2+(C1(2,1)-C1(1,1))^2+(C1(2,2)-C1(1,2))^2)^0.5;
    d1=((C1(2,3)-C1(1,3))^2+(C1(2,1)-C1(1,1))^2)^0.5;
    Vx=1*(d1/d);
    simulation.dwells(i,6)=Vx*((C1(2,3)-C1(1,3))/d1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Y
    d1=((C1(2,3)-C1(1,3))^2+(C1(2,1)-C1(1,1))^2)^0.5;
    Vy=1*(d1/d);
    simulation.dwells(i,7)=Vy*((C1(2,1)-C1(1,1))/d1);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Y  
    d1=((C1(2,2)-C1(1,2))^2+(C1(2,1)-C1(1,1))^2)^0.5;
    Vz=1*(d1/d);
    simulation.dwells(i,8)=Vz*((C1(2,2)-C1(1,2))/d1);
    %
    angles(i,1)=cosd(asind(Vx));
    angles(i,2)=cosd(atand((C1(2,1)-C1(1,1))/(C1(2,3)-C1(1,3))));
    angles(i,3)=cosd(asind(simulation.dwells(i,8)));
end
%
simulation.dwells(isnan(simulation.dwells(:,6)),6)=0;
simulation.dwells(isnan(simulation.dwells(:,7)),7)=0;
simulation.dwells(isnan(simulation.dwells(:,8)),8)=0;
%
end



%
function [simulation] =  get_sim_info()
%
handles      = guihandles;
%
simulation=[];
%
% Phantom 
if        strcmp(handles.phantom_user.Checked,'on')==1
             simulation.Phantom='User';
elseif    strcmp(handles.phantom_all.Checked,'on')==1
             simulation.Phantom='All';
else 
           warndlg('Select the phantom size'); 
           simulation=[]; simulation=1; return; %#ok<*NASGU>
end
%
% dose grid 
if        strcmp(handles.dosegrid_user.Checked,'on')==1
             simulation.Dosegrid='User';
elseif    strcmp(handles.dosegrid_all.Checked,'on')==1
             simulation.Dosegrid='All';
else 
          warndlg('Select the dose grid'); return;             
end
%
% score 
if    strcmp(handles.score_dwm.Checked,'on')==1
             simulation.Score{1}='Dwm';
end
if    strcmp(handles.score_dmm.Checked,'on')==1
             simulation.Score{2}='Dmm';
end
if    strcmp(handles.score_energy.Checked,'on')==1
             simulation.Score{3}='Mean';             
end
if    strcmp(handles.score_dose.Checked,'on')==1
             simulation.Score{4}='Dose';             
end
%
% voxels
if        strcmp(handles.vox_on.Checked,'on')==1
             simulation.Voxel='On';
elseif    strcmp(handles.vox_off.Checked,'on')==1
             simulation.Voxel='Off';           
end
%
% density
if        strcmp(handles.dens_lib.Checked,'on')==1
             simulation.Density='Lib';
elseif    strcmp(handles.dens_ct.Checked,'on')==1
             simulation.Density='CT';           
end
%
% source
if        strcmp(handles.source_gmp.Checked,'on')==1
             simulation.Source='GammaMedPlus';
elseif    strcmp(handles.source_mselectronv2.Checked,'on')==1
             simulation.Source='MicroSelectronv2';   
elseif    strcmp(handles.no_source.Checked,'on')==1
             simulation.Source='none'; 
end
% nps
simulation.Nps=str2double(handles.sim_nps.Label);
%
end





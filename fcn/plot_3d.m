
handles = guihandles; TPlan=getappdata(handles.amb_interface,'TPlan');
%axes(findobj('Tag','amb_vis_04'));
%colormap(map)
figure;
D=20*int16(TPlan.struct.contours.Item_1.Mask);
dw=TPlan.plan.dwellVox;
%
Ds = smooth3(D);
hiso = patch(isosurface(Ds,1),...
   'FaceColor',[1,.0,.0],...
   'EdgeColor','none');
   isonormals(Ds,hiso)
   
%hiso.FaceColor='b'
%hiso.FaceAlpha=0.25
view(35,30) 
axis tight 
daspect([3,3,0.68])   
hiso.FaceAlpha=0.45;
lightangle(45,30);
lighting gouraud
hiso.AmbientStrength = 0.95;
hiso.SpecularColorReflectance = 0.05;
hiso.SpecularExponent = 90;
%
hold on;
h=plot3(dw(:,1),dw(:,2),dw(:,3),'b.');
